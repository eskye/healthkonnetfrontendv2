
import { environment } from '../../environments/environment';
export const BASEURL = environment.BASEURL;
export const BASE = environment.BASE;

export const routes = {
  jsoncode: 'https://jsonplaceholder.typicode.com/posts',
  LOGIN: BASEURL + '/Account/token',
  LOGOUT: BASEURL + '/Account/logout',
  LOGGEDIN: BASEURL + '/Account/GetCurrentLoggedInUser',
  GETACCOUNT: BASEURL + '/Account/getAccount',
  REGISTER: BASEURL + '/Account/Register',
  INVITE: BASEURL + '/Account/invite',
  OPENACCOUNT: BASEURL + '/Account/openAccountAfterActivation',
  UPDATEPROFILE: BASEURL + '/Account/profile',
  SETUPACCOUNT: BASEURL + '/Account/installation',
  CHANGEPASSWORD: BASEURL + '/Account/changepassword',
  FORGOTPASSWORD: BASEURL + '/Account/ForgotPassword',
  RESETPASSWORD: BASEURL + '/Account/ResetPassword',
  MAKEACCOUNTACTIVE: BASEURL + '/Account/makeActive',
  ACTIVATEACCOUNT: BASEURL + '/Account/activateAccount',
  ACTIVATE_ENROLLEE: BASEURL + '/Account/activate',
  CREATEANONYMOUS: BASEURL + '/Account/createanonymous',
  GET_ENROLLEE: BASEURL + '/Account/checkemployeetemp',
  GETTEMPENROLLEE: BASEURL + '/Account/getPatientTemp',
  RESENDCONFIRMATIONLINK: BASEURL + '/Account/ResendConfirmationLink',
  ROLES: BASEURL + '/roles/getrole',
  GETSTATUS: BASEURL + '/Account/GetStatuses',
  GETSCHEMES: BASEURL + '/Account/GetSchemes',
  SETUPORGANACCOUNTHMO: BASEURL + '/Account/createorganization',
  SETUPPROVIDER: BASEURL + '/provider/createProvider',
  PROVIDERPROFILE: BASEURL + '/Account/UpdateProviderProfile',
  HMOPROFILE: BASEURL + '/Account/UpdateHmoProfile',
  // USERS LIST
  USERLIST: BASEURL + '/userprofile/users',
  USERDETAIL: BASEURL + '/userprofile/userDetails',


  CREATEHMO: BASEURL + '/hmo/create',
  SETUPHMO: BASEURL + '/hmo/createhmo',
  HMOCLIENTS: BASEURL + '/hmo/hmoClient',
  CLIENTCOUNTER: BASEURL + '/hmo/counter',
  SETCHARGES: BASEURL + '/hmo/setPrice',
  GETCHARGES: BASEURL + '/hmo/getPrice',
  CREATEHMOINFO: BASEURL + '/hmo/createhmoinfo',
  HMOS: BASEURL + '/hmo',
  VERIFYHMOACCOUNT: BASEURL + '/hmo/VerifyHmoAccount',
  GETORGANIZATIONNAMEANDIDS : BASEURL + '/hmo/organizationLookUp/',
  APPROVEREQUEST : BASEURL + '/hmo/approveChangeProviderRequest',
  GETCLAIMS : BASEURL + '/hmo/getClaims',
  APPROVECLAIMS : BASEURL + '/hmo/approveClaims',
  GETPROVIDERNAMEANDID: BASEURL + '/hmo/HmoEnrolleeProviderList',



  PACKAGES: BASEURL + '/packages',
  PACKAGENAMEANDID: BASEURL + '/packages/GetNameAndIds',
  SEARCHPACKAGE: BASEURL + '/packages/search',

  ORGGETHMO: BASEURL + '/organization/gethclient',
  UPLOADENROLLEE: BASEURL + '/organization/upload',

  GETEMPLOYEES: BASEURL + '/organization/getenrolles',

  SUBLIST: BASEURL + '/organization/getsubscription',

  OTHERS: {
  GETSTATES: BASEURL + '/other/GetStates',
  LISTOFBANKS: BASEURL + '/other/ListOfBanks',
  RESOLVEACCOUNT: BASEURL + '/other/VerifyAccountNumber',
  DETAIL: BASEURL + '/other/GetBankSetting',
  SAVE: BASEURL + '/other/saveBankSetting',
  SAVEPAYMENTCONFIG: BASEURL + '/other/AddPaymentGatewayConfiguration',
  PAYMENTCONFIGDETAIL: BASEURL + '/other/GetPaymentGatewayConfig',
  },
  TREATMENTREQUEST: BASEURL + '/treatmentRequest',

  TREATMENTREQUESTNOTIFICATION:  BASEURL + '/treatmentRequest/RequestNotification',
  MARKREQUESTASREAD: BASEURL + '/treatmentRequest/MarkAsRead',

  DASHBOARD:{
    HMODASHBOARD: BASEURL + '/Dashboard/hmoDashboard',
    PROVIDERDASHBOARD: BASEURL + '/Dashboard/providerDashboard',
    SUPERADMINDASHBOARD: BASEURL + '/Dashboard/superadminDashboard',
     ORGANIZATIONDASHBOARD: BASEURL + '/Dashboard/organizationDashboard',
  },
  GETCHARTDATA: {
    ORGDASHBOARD: BASEURL + '/chart/organizationDashboardChart',
    HMOENROLLEECHART: BASEURL + '/chart/hmoEnrolleeChart',
    HMOENROLLEEVISIT: BASEURL + '/chart/hmoEnrolleeVisitChart',
    PROVIDERENROLLEEVISIT: BASEURL + '/chart/providerEnrolleeVisitChart'
  },
  FILEMANAGER: {
    UPLOADDIAGNOSIS: BASEURL + '/filemanager/uploadDiagnosis',
    UPLOADBLOGIMAGE: BASEURL + '/filemanager/uploadBlog',
    UPLOADEVENTIMAGE: BASEURL + '/filemanager/uploadEvent',
    UPLOADPROFILEIMAGE: BASEURL + '/filemanager/uploadProfileImage'
  },
  HEALTHCARE: {
    UPLOAD: BASEURL + '/healthcare/UploadHealthCareList',
    COUNT: BASEURL + '/healthcare/count',
    CREATE: BASEURL + '/healthcare/Create',
    SEARCH: BASEURL + '/healthcare/searchHealthCare'
  },
  ORGANIZATION: {
    COUNT: BASEURL + '/organization/count',
    CREATEORG: BASEURL + '/organization/create',
    ADDSETTING : BASEURL + '/hmo/AddOrganizationSetting',
    GETDETAILBYKEY: BASEURL + '/organization/Detail',
    GETORGANIZATIONLOOKUP: BASEURL + '/organization/GetHmoOrganizationLookup'
  },
  HMO: {
    LIST: BASEURL + '/hmo/list',
    APPROVEREQUEST : BASEURL + '/hmo/approveChangeProviderRequest',
    GETHMODETAILS : BASEURL + '/hmo/Detail'
    
  },
  PROVIDER: {
   COUNT: BASEURL + '/provider/count',
   NAMEANDID: BASEURL + '/provider/GetNameAndIds',
   SEARCH: BASEURL + '/provider/searchProvider',
   PROVIDER: BASEURL + '/provider',
   VALIDATEPRICE: BASEURL + '/provider/validateDrug',
   VALIDATESERVICE: BASEURL + '/provider/validateService',
  },
  TARIFF: {
    CATEGORY: BASEURL + '/TariffCategory',
    TARIFFUPLOAD: BASEURL + '/Tariff/UploadTariffList',
    TARIFFCOUNT: BASEURL + '/Tariff/count',
    TARIFFUPDATE: BASEURL + '/Tariff',
    GETSERVICES: BASEURL + '/Tariff/GetServiceLookUp',
    DRUGUPLOAD: BASEURL + '/DrugTariff/UploadDrugTariffList',
    DRUGCOUNT: BASEURL + '/DrugTariff/count',
    DRUGUPDATE: BASEURL + '/DrugTariff',
    DOWNLOADTARIFFTEMPLATE: BASEURL + '/Tariff/DownloadTemplate',
    DOWNLOADDRUGTARIFFTEMPLATE: BASEURL + '/DrugTariff/DownloadTemplate',
  },
  SELECTHOSPITAL: {
    CREATE : BASEURL + '/HospitalSelection/create',
    COUNT : BASEURL + '/HospitalSelection/count',
    MAIN : BASEURL + '/HospitalSelection',
    SELECTEDHOSPITAL : BASEURL + '/HospitalSelection/GetSelectedHmoProviderLookup',
    ASSIGNTARIFF: BASEURL + '/HospitalSelection/AssignTariff',
    GETCURRENTTARIFF: BASEURL + '/HospitalSelection/GetCurrentTariff',
    GETBYSTATE: BASEURL + '/HospitalSelection/GetSelectedHmoProviderLookupByState'
  },
  AUDITTRAIL: {
    MAIN: BASEURL + '/audittrail',
    COUNT: BASEURL + '/audittrail/count',
    GETAUDITACTIONS: BASEURL + '/audittrail/AuditActions',
    GETAUDITSECTIONS: BASEURL + '/audittrail/AuditSections'
  },
  STAFF: {
    DOWNLOADTEMPLATE: BASEURL + '/Staff/Staff-upload-Template',
    UPLOAD: BASEURL + '/Staff/upload',
    COUNT: BASEURL + '/Staff/count',
    DETAIL: BASEURL + '/Staff/detail'
  },
  PAYMENT: {
    FUND: BASEURL + '/Payment/FundWallet',
    PAYMENTLIST: BASEURL + '/Payment/payments',
    DETAIL: BASEURL + '/Payment'
  },
  REPORT: {
    INVOICE: BASE + '/report/invoice'
  },
  ACCOUNT: {
    ONBOARD: {
      PIN: BASEURL + '/Account/ConfirmPin'
    }
  },
  USERPROFILE: {
    DETAIL: BASEURL + '/userprofile/Detail',
    UPDATE: BASEURL + '/userprofile'
  },
  ENROLLEE: {
    DETAIL: BASEURL + '/enrollee/Detail',
    COUNT: BASEURL + '/enrollee/count',
    UPDATE: BASEURL + '/enrollee',
    ADDPLAN: BASEURL + '/enrollee/AddPackage',
    VERIFYENROLLEE: BASEURL + '/enrollee/verifyEnrollee',
    SEARCH: BASEURL + '/enrollee/searchEnrollee',
    CHANGEPROVIDER: BASEURL + '/enrollee/ChangeProvider',
    GETCHANGEPROVIDERREQUESTS: BASEURL + '/enrollee/GetChangeProviderList',
    GETCURRENTPACKAGE: BASEURL + '/enrollee/getCurrentPackage',
  },
CLAIM: {
  MAIN: BASEURL + '/Batch',
  LOOKUP: BASEURL + '/Batch/GetClaimBatchLookup',
  BATCHEDCLAIMCOUNT: BASEURL + '/Batch/count',
   CLAIMFORMCOUNT: BASEURL + '/ClaimForm/count',
   GETREQUESTAUTHORIZATION: BASEURL + '/ClaimForm/GetAuthorizationCodesRequest',
  CLAIMFORM: BASEURL + '/ClaimForm',
  CLAIMFORMDETAILBYVISITCODE: BASEURL + '/ClaimForm/GetDetailByVisitCode',
  CLAIMSTATUS: BASEURL + '/ClaimForm/GetStatuses',
  ADDCLAIMFORM: BASEURL + '/ClaimForm/AddClaims',
  REQUESTAUTHORIZATIONCODE: BASEURL + '/ClaimForm/GenerateAuthorizationCode',
  APPROVEAUTHORIZATIONCODE: BASEURL + '/ClaimForm/ApproveAuthorizationCode',
  POSTAPPROVECLAIM: BASEURL + '/ClaimForm/ClaimApprovalWorkFlow',
  GETCLAIMAPPROVALCOMMENTS: BASEURL + '/ClaimForm/GetClaimApprovalWorkFlows'
},
  CREATEPRO: BASEURL + '/provider/create',
  CREATEPROINFO: BASEURL + '/provider/createproviderInfo',
  GETPROVIDERBYSTATE: BASEURL + '/provider/providerlocation',
  GETPROVIDERBYLOCATION: BASEURL + '/provider/providerSearchMap',
  GETPROVIDERAROUND: BASEURL + '/provider/providerAround',
  PROVIDERENROLLEECOUNT: BASEURL + '/Counter/countProviderEnrollee',
  SEARCHENROLLEE: BASEURL + '/provider/providerSearch',
  GETCLAIMTYPES: BASEURL + '/provider/getClaimType',
  CREATECLAIM: BASEURL + '/provider/addClaim',
  UPDATECLAIM: BASEURL + '/provider/updateClaim',
  COMPLAINT: BASEURL + '/provider/Complaint',
  MEDICATION: BASEURL + '/provider/Medication',
  SAVEMEDICALRECORD: BASEURL + '/provider/SaveMedicalRecord',
  GETENROLLEEHMOINPROVIDER: BASEURL + '/provider/getEnrolleeHmoProvider',
  GETENROLLEEORGINPROVIDER: BASEURL + '/provider/getEnrolleeOrganizationProvider',

  INITIALIZETREATMENT: BASEURL + '/visit/initializeTreatment',
  VISITLIST: BASEURL + '/visit/visits',
  INVESTIGATIONS: BASEURL + '/visit/complaints',
  MEDICATIONS: BASEURL + '/visit/medications',
  DIAGNOSES: BASEURL + '/visit/diagnosis',
  VISIT: BASEURL + '/visit',

  ADDENROLLEEPROVIDER: BASEURL + '/enrollee/addEnrolleeProvider',
  ADDENROLLEEPLAN: BASEURL + '/enrollee/addEnrolleePlan',
  ADDENROLLEEPACKAGE: BASEURL + '/enrollee/addPackage',
  GETSUBACCOUNT: BASEURL + '/enrollee/getSubAccounts',
  CHECKBARCODE: BASEURL + '/enrollee/verifyBarcode',
  ENROLLEEDETAIL: BASEURL + '/enrollee',

  GENERATEBARCODE: BASEURL + '/enrollee/generateBarcode',
  FETCHBARCODE: BASEURL + '/enrollee/fetchBarcode',
  GETPROVIDERINFO: BASEURL + '/enrollee/getCurrentProvider',
  POSTEMERGENCY: BASEURL + '/enrollee/emergency',
  OTHERSETTING: BASEURL + '/enrollee',
  CHANGEPROVIDER: BASEURL + '/enrollee/changeProvider',

  GETEMERGENCYLIST: BASEURL + '/enrollee/GetEmergency',
  CHECKWALLET: BASEURL + '/enrollee/CheckWallet',
  GETENROLLEES: BASEURL + '/enrollee',
  EXPORTEMPLOYEE: BASEURL + '/enrollee/exportEmployee',
  CHECKBALANCE: BASEURL + '/Payment/checkBalance',
  GETEMPLOYEESTEMP: BASEURL + '/employee/employees',


  GETALLACCOUNTS: BASEURL + '/Account/getAllAccounts',
  UNACTIVEACCOUNTS: BASEURL + '/Account/unactiveAccount',

  USERLOGGER: BASEURL + '/userlogger',

  PLAN:   BASEURL + '/Plans',
  POST: {
   CATEGORY: BASEURL + '/BlogCategory',
   BLOG: BASEURL + '/Blog',
   COUNT: BASEURL + '/Blog/Count'
  },
  EVENT: {
    MAIN: BASEURL + '/Event',
    COUNT: BASEURL + '/Event/Count'
   },
  FAQ: {
    SECTION: BASEURL + '/faqSection',
    FAQS: BASEURL + '/faq'
   },
   MEDICKIT: {
    MEDICATION: BASEURL + '/Medication',
    DISEASE: BASEURL + '/Disease'
   },
   TRANSFERWITHDRAW: {
     CREDITPROVIDER: BASEURL + '/TransferWithdraw/creditProvider',
     PROVIDERSETTLEMENTHISTORY: BASEURL + '/TransferWithdraw/settlementTransactionHistory'
   },
   EMAILTEMPLATE: {
     MAIN: BASEURL + '/EmailTemplate',
     COUNT: BASEURL + '/EmailTemplate/count',
     GETEMAILTEMPLATETYPES : BASEURL + '/EmailTemplate/GetEmailTemplateTypes',
     DETAIL: BASEURL + '/EmailTemplate/Detail',
     PLACEHOLDERS: BASEURL + '/EmailTemplate/Placeholders'
   }

};

export const SystemConstant = {
  ENCRYPTIONKEY: '</@$!!Hmosass&!!)/>',
  RSA: `MIICXQIBAAKBgQDlOJu6TyygqxfWT7eLtGDwajtNFOb9I5XRb6khyfD1Yt3YiCgQ
  WMNW649887VGJiGr/L5i2osbl8C9+WJTeucF+S76xFxdU6jEPF31xk7YOBfkGI8qjLoq06V+FyBfDSwL8KbLyeH
  m7KUZnLNQbk8yGLzB3iYKkRHlmUanQGaNMIJziWOkN+N9dECQQD0ONYRNZeuM8zd
  8XJTSdcIX4a3gy3GGCJxOzv16XHxD03GW6UNLmfPwxaBJbVRfApFrE0/mPwmP5
  rN7QwjrMY+0+AbXcm8mRQyQ1VGJiGr/L5i2osbl8C9+WJTeucF+S76
  xFxdU6jE0NQ+Z+zEdhUTooNRaY5nZiu5PgDB0ED/ZKBUSLKL7eibMxZtMlUDHjm4
  gwQco1KRMDSmXSMkDwIDAQAB`,
  SESSIONKEY: '_hhaomssrhy234_',
  IMAGE: BASE + '/upload/profile/',
  BLOGIMAGE: BASE + '/upload/blog/',
  LOGO: BASE + '/upload/logo/',
  DOWNLOAD: BASEURL + '/Enrollee/DownloadCsv/',
  LEVEL_ACCESS: {
    'o': 'Organization',
    'e': 'Enrollee',
    'a': 'Admin',
    'h': 'HMO',
    'p': 'Provider',
    's': 'Superadmin'
  },
  PREFIX: 'hmo',
  CONTACT: {
    email: 'support@healthkonnet.com'
  }
};
export enum roles {
  Superadmin = 1,
  Admin = 2,
  Enrollee = 3,
  HMO = 4,
  Organization = 5,
  Provider = 6
}
export enum notifications {
   success = 'Success Notification',
   error = 'Error Notification',
   warn = 'Warning Notification',
   info = 'Info Notification'
}

export enum sroles {
  Superadmin = 'Superadmin',
  Admin = 'Admin',
  Enrollee = 'Enrollee',
  HMO = 'HMO',
  Organization = 'Organization',
  Provider = 'Provider'
}

