import { StoreService } from "./services/store.service";
import * as moment from 'moment';
 
export class Utils {
    private static fileExtension: any;
    private static maxSize = 1; // 5MB
   /**
    *
    */
   /**
    *
    */
   constructor(private store: StoreService) {
     
     
   }
    
    static isMobile() {
        return window && window.matchMedia('(max-width: 767px)').matches;
    }
    static ngbDateToDate(ngbDate: { month, day, year }) {
        if (!ngbDate) {
            return null;
        }
        return new Date(`${ngbDate.month}/${ngbDate.day}/${ngbDate.year}`);
    }
    static dateToNgbDate(date: Date) {
        if (!date) {
            return null;
        }
        date = new Date(date);
        return { month: date.getMonth() + 1, day: date.getDate(), year: date.getFullYear() };
    }
    static scrollToTop(selector: string) {
        if (document) {
            const element = <HTMLElement>document.querySelector(selector);
            element.scrollTop = 0;
        }
    }
    static genId() {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

   static isInArray(array, word) {
     return array.indexOf(word.toLowerCase()) > -1;
   }

   static fileValidator(file: any, isImage: boolean = false, isDocument: boolean = false) {
     let allowedExtensions: any;
     const allowedExtensionForDocument =
       ['csv', 'CSV', 'pdf', 'PDF', 'doc', 'DOC', 'DOCX', 'docx'];
     const allowedExtensionForImage = ['jpg', 'JPG', 'png', 'PNG', 'gif', 'GIF', 'jpeg', 'JPEG'];
     this.fileExtension = file.name.split('.').pop();
     if (isImage) {
       allowedExtensions = allowedExtensionForImage;
     } else if (isDocument) {
       allowedExtensions = allowedExtensionForDocument;
     } else {
       return false;
     }
     return this.isInArray(allowedExtensions, this.fileExtension);
   }

   static isValidFileSize(file) {
     const fileSizeinMB = file.size / (1024 * 1000);
     const  size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place
     if (size > this.maxSize) { return false; }
     return true;
 }

 static getMonth(monthNumber: number): string {
    const monthNames = ['January', 'Feburary', 'March', 'April', 'May', 'June',
                       'July', 'August', 'September', 'October', 'November', 'December'];
    return monthNames[monthNumber];
 }

 static greet() {
   const currentDate = new Date();
   const hours = currentDate.getHours();
   if (hours < 12) {
      return 'Good morning';
    } else if (hours < 18) {
      return 'Good afternoon';
    } else {
      return 'Good evening';
    }
 }
 static clock() {
  return moment().format('hh : mm : ss a');
   /* const now = new Date();
    const TwentyFourHour = now.getHours();
    let hour = now.getHours();
    const min = now.getMinutes();
    const sec = now.getSeconds();
    let mid = 'pm';
    let newmin = '', newhour = '';
    if (min < 10) {
      newmin = '0' + min;
    }

    if (min === 10 || min > 10) {
      newmin = min.toString();
    }
    if (hour > 12) {
      hour = hour - 12;
      newhour = hour.toString();
    }
    if (hour === 0) {
      hour = 12;
      newhour = hour.toString();
    }
    if (hour < 10) {
      newhour = '0' + hour;
    }

    if (TwentyFourHour < 12) {
       mid = 'am';
    }
   return `${newhour} : ${newmin} : ${sec < 10 ? '0' + sec : sec}  ${mid}`; */
    }

   get UserInfo(){
     return this.store.user;
    }
}
