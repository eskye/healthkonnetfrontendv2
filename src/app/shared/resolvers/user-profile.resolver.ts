import { AuthService } from '../services/auth.service';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { IProfile } from '../common/model/IProfile';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
    providedIn: 'root'
})
export class UserProfileResolver implements Resolve<IProfile> {
    constructor(private auth: AuthService) { }
    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.auth.loggedInUser().pipe(map(res => {
            return {...res};
        }));
    }
}

