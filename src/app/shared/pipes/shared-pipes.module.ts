import { SortPipe } from './sort.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExcerptPipe } from './excerpt.pipe';
import { GetValueByKeyPipe } from './get-value-by-key.pipe';
import { RelativeTimePipe } from './relative-time.pipe';
import { SearchFilterPipe } from './search-filter.pipe';
import { GrdFilterPipe } from './grd-filter.pipe';
import { SafeHtmlPipe } from './safe-html.pipe';

const pipes = [
  ExcerptPipe,
  GetValueByKeyPipe,
  RelativeTimePipe,
  SearchFilterPipe,
  SortPipe,
  GrdFilterPipe,
  SafeHtmlPipe
];


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: pipes,
  exports: pipes
})
export class SharedPipesModule { }
