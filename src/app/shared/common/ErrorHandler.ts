import {HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import { throwError } from 'rxjs';
export class ErrorHandler {

  static handleError(error: HttpErrorResponse) {
    // console.error('server error:', error.error);
    const errors: any = error.error.error.message;
    if (errors) {
      let modelStateError = '';
      for (const key in errors) {
        if (errors[key]) {
          modelStateError += errors[key] + '\n';
          console.log(modelStateError);
        }
      }
      if (modelStateError) {
        return throwError(modelStateError || 'Unexpected error occurred');
      }
    }

    if (error.error instanceof Error) {
      const errMessage = error.error.message;
      return throwError(errMessage);
      // Use the following instead if using lite-server
      // return Observable.throw(err.text() || 'backend server error');
    }
    return throwError(error || 'Server error');
  }

  static ErrorConnection(err: HttpErrorResponse) {
    let errorMessage = '';
    let isCustomError = false;
    if (err.error instanceof Error) {
       isCustomError = (<any>err.error).isCustomError;
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage =  err.error.message;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }

  static ErrorServerConnection(error: HttpErrorResponse) {
    console.error('server error:', error.error);
    const isCustomError = error.error.isCustomError;
     if (isCustomError) {
       return throwError(error.error.message || 'Unexpected error occurred');
     } else if (error.status === 0) {
      return throwError('You are not connected to the internet');
     }else if(error.status === 401){
       return throwError('You session has timed out');
     }
    return throwError('Unexpected Server error');
  }

  private Errorhandler(error: any) {
    // const applicationError = http.get('Application-Error');
    const applicationError = '';
    if (applicationError) {
      return throwError(applicationError);
    }
    const serverError = error.json();
    let modelStateErrors = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
    }

    return throwError(modelStateErrors || 'server Error' );
  }
}
