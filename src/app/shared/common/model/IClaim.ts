export interface IDrug {
  name: string;
  dosageDescription: string;
  quantityPrescribed: number;
  cost: any;
  unq: any;
}

export interface IServiceClaim {
  name: string;
  description: string;
  unq: any;
  cost: number;
}
