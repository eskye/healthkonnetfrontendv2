import { IRootObject } from '../interface/IRootObject';
export interface  IPayment extends IRootObject {
    amount: number;
    datePayed: string;
    mode: string;
    paymentCode: string;
    paymentStatus: boolean;
  }

  export interface IAccountVerificationModel{
    accountNumber: string;
    bankCode: string;
  }
