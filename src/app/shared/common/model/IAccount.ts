import { IRootObject } from '../interface/IRootObject';
export interface IAccount extends IRootObject {
  email: string;
  password?: string;
  confirmpassword?: string;
  Role?: string;
  role: number|string;
  scheme?: number;
  Id?: string;
  isSubAccount?: boolean;
  isAnonymous?: boolean;
}

export interface IPassword extends IAccount {
  currentpassword: string;

}

export interface ITokenparams extends IAccount {
  accessToken: string;
  token_type: string;
  expires_in: string;
  key: string;
  role: string;

}

export interface IUserLogin extends IRootObject {
  email: string;
  password: string;
}

export interface IResetPassword extends IRootObject, IAccount {
  code: string;
}
