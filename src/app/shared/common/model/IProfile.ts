import { IRootObject } from '../interface/IRootObject';
import { IData } from '../interface';



/**
 * Created by Sunkee on 26-Aug-18.
 */
export interface IOthersetting extends IRootObject {
  employeeid: string;
  nationality: string;
  state: string;
  lga: string;
  maritalstatus: string;
  surname?: string;
  email: string;
  dateofbirth: string;
}
export interface IProfile extends IRootObject {
  [x: string]: any;
  isPersonal: any;
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  gender: string;
  email: string;
  profilePic: string|any;
  isActive: boolean;
  isSub: boolean;
  role: string;
  roleId?: number|string;
  planName: string;
  picture: string|any;
  isAnonymous: boolean;

}

export interface IProfileHmo extends  IRootObject {
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  gender: string;
  email: string;
  roleId: number;
  nationality: string;
  state: string;
  lga: string;
  maritalstatus: string;
  dateofbirth: string;

}

export interface ILoggedInUser extends IRootObject {
  IsActive: boolean;
  Role: string;
  Email: string;
}
