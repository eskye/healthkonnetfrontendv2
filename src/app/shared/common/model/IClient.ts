import { IRootObject } from '../interface/IRootObject';

export interface IClient extends IRootObject {
    email: string;
    isActive: boolean;
    role: string;
    username: string;
    firstname: string;
    lastname: string;
}
