import { IRootObject } from '../interface/IRootObject';

export interface ISetting extends IRootObject {
    userId: string;
    charge: number;
    action: string;
}

export interface IFacility extends IRootObject {
    userId: string;
    planId?: number;
    providerId?: string;
    packageCode?: string;
  }

  export interface IPackage extends IRootObject {
    packageId: number;
    packageName: string;
    packageCode: string;
    packageValue: number;
    subTitle: string;
    packageDescription: string;
    formattedValue: string;
  }

  export interface IMedications {
    code: string;
    description: string;
    id: number;
  }

  export interface IDisease {
    code: string;
    description: string;
    id: number;
  }