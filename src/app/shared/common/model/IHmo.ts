
import { IRootObject } from '../interface/IRootObject';

export interface IHmo extends IRootObject {
    hmod?: number;
    hmoName: string;
    hmoAddress: string;
    hmoLocation: string;
    hmoEmail: string;
    hmoPhone: string;
    hmoLogo: string|any;
    businessDescription: string;
    nameOfHr: string;
    hmcanHmoNo: string;
    md: string;
    nhisHmoNo: string;
    registrationNumber: string;
    Datecreated: string;
  }
