import {IRootObject} from '../interface/IRootObject';
export interface ICounter extends IRootObject {
organization: number;
 enrollees: number;
 provider: number;
 hmo: number;
 activeEmployees: number;
  unactiveEmployees: number;
 subscription: string;
 totalPaymentMade: string;
 walletValue: string;
 claims: number;
 pendingClaimRequests: number;
 pendingAuthorizationRequests: number;
 changeProvidersCount: number;
}
