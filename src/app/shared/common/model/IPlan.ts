import { IRootObject } from '../interface';

export interface IPlan extends IRootObject {
  planName: string;
  planValue: number;
  description: string;
}
