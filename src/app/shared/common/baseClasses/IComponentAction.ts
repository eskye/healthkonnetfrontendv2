import { IQueryOptions, IFilter } from 'src/app/shared/common/interface/IQueryOptions';
import { IError } from '../model/IError';


export interface IComponentAction {
    editorOptions: any;
    // settings: IConfirmSettings | any;
    url: any;
    query: IQueryOptions;
    items: any;
    states: any;
    statuses: any;
    nameAndIds: any;
    pager: any;
    item: any;
    size: number;
    sizes: Array<number>;
    genders: Array<string>;
    months: Array<string>;
    hideFilter: boolean;
    filter: any;
    error: IError;
    filterarray: IFilter[];
    notificationData: Array<any>;
    isUploading: boolean;
    paginationConfig?: { count: number; page: number; total: number };
    selectedIds: any;
    selectedAll: any;
    greet: () => string;
    clock: () => string;
    getParamValue: (key: string) => string;
    pageChanged: (event) => void;
    changePage: () => void;
    init: () => void;
    goBack();
    goTo(url);
    go(route, id);
    gotoRoute(route, id, other);
    goToNav(route);
    reloadComponent();
    setupPagination();
    setupPaginations();
    genPagination();
    getCountry();
    getStatuses(url);
    getNameAndId(url);
    getPageInfoDescription(): string;
    toggleActive();
    toggleFilter();
    load(url?);
    titleRoute(title: string);
    getSum(field: string, items: Array<any>): number;
    openInNewTab(url: string, behavior: string, options: string)
    secureData(value: any): string;
    unsecureData(value: any): string;
    checkUncheckAll();
    isAllSelected();
    getCheckedItemList();
}
