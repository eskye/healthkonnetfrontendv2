import { IComponentAction } from './IComponentAction';
import { Location } from '@angular/common';
import {ActivatedRoute, NavigationEnd, Router, ParamMap} from '@angular/router';
// import { IConfirmSettings } from '../confirm-settings';
import { IRootService, IQueryOptions } from '../interface';
import { IFilter } from '../interface/IQueryOptions';
import {OnInit} from '@angular/core';
import {map, catchError} from 'rxjs/internal/operators';
import {Title} from '@angular/platform-browser';
import { isNullOrUndefined } from 'util';
import { IError } from '../model/IError';
import * as CryptoJS from 'crypto-js';
import { routes } from '../../constant';
import { Utils } from '../../utils';
import { HttpErrorResponse } from '@angular/common/http';
import { GetCurrentUser } from 'src/app/views/sessions/state/user.actions';
import { StoreService } from '../../services/store.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/views/sessions/state/user.reducers';
 
export abstract class BaseComponent implements IComponentAction, OnInit {
private locker = '123_text_ec';
  selectedIds: any;
  selectedAll: any;
  checkedList: any[];
  masterSelected: any;
  constructor(
    public location?: Location,
    public router?: Router,
    public activatedRoute?: ActivatedRoute,
    public titleService?: Title,
    public resource?: IRootService<any>
  ) {
  }
  editorOptions: any = {
    charCounterCount: true,
    // tslint:disable-next-line:max-line-length
    toolbarButtons: [
      'fontFamily',
      '|',
      'fontSize',
      '|',
      'paragraphFormat',
      '|',
      'bold',
      'italic',
      'underline',
      'underline',
      'strikeThrough',
      'outdent',
      'indent',
      'clearFormatting',
      'undo',
      'redo',
      'codeView',
      '|',
      'quote',
      'insertHR'
    ],
    fontFamilySelection: true,
    fontSizeSelection: true,
    paragraphFormatSelection: true,
    heightMin: 100,
    heightMax: 500
  };
  /*settings: IConfirmSettings | any = {
    overlay: true,
    overlayClickToClose: true,
    showCloseButton: true,
    confirmText: 'Yes',
    declineText: 'No'
  };*/
  query: IQueryOptions;
  filterarray: IFilter[];
  waiting: boolean;
  url: any;
  items: any;
  item: any = {};
  exitem: any;
  notificationData: Array<any> = [];
  states: any;
  statuses: any;
  nameAndIds: any;
  isUploading: boolean;
  error: IError = {
    Message: 'Error: No internet connection',
    isError: false
   };
  hideFilter = false;
  size = 20;
  providerId: any;
  filter: any = {};
  changePage: () => void;
  paginationConfig = {
    count: this.size || 50,
    page: 1,
    total: 0
};
sizes: Array<number> = [50, 100, 150, 200, 250];
genders: Array<string> = ['Man', 'Woman'];
months: Array<string> = ['January', 'Feburary', 'March', 'April', 'May', 'June',
'July', 'August', 'September', 'October', 'November', 'December'];
checkItems: any;
pager: any = { page: 1, size: 50 };
  init(id?: number) {
   this.setupPagination(id);
   this.filterarray = [
     {key: 'Active', value: 1},
     {key: 'Inactive', value: 0}
   ];

    }
    getParamValue(key: string): string {
      let value = '';
      this.activatedRoute.paramMap.subscribe((param: ParamMap) => {
        value = param.get(key);
      });
      return value;
    }
  goBack() {
    this.location.back();
  }
  go(route, id, isSecure = false) {
    const eid = isSecure ? this.secureData(id) : id;
    this.router.navigate([route, id]);
  }
  gotoRoute(route, id, other, isSecure = false) {
  const eid = isSecure ? this.secureData(id) : id;
    this.router.navigate([route, eid, other]);
  }
  goToNav(route) {
    this.router.navigate([route]);
  }
  // tslint:disable-next-line:no-shadowed-variable
  goTo(url: string) {
    this.router.navigateByUrl(url);
  }
  ngOnInit() {}
  reloadComponent() {
    this.filter = {};
     this.ngOnInit();
  }

  secureData(value: any): string {
   // const dataStore: DataSaverService = this.resource.getService(DataSaverService);
    return CryptoJS.AES.encrypt(value, this.locker).toString();
  }
  unsecureData(value: any): string {
    // const dataStore: DataSaverService = this.resource.getService(DataSaverService);
    return CryptoJS.AES.decrypt(value, this.locker).toString();
  }
  titleRoute(title: string) {
    this.titleService.setTitle(title);
    }
  setupPagination(id?: number) {
    this.waiting = true;
    if (this.filter.isActive === 'Active' || id !== 0) {
      this.filter = {
        isActive: 1,
        providerId: id
      };
    } else {
      this.filter = {
        isActive: 0
      };
    }
   return this.resource
      .query(
        Object.assign({
          count: this.paginationConfig.count,
          page: this.paginationConfig.page,
          orderByExpression: this.query.orderByExpression,
          whereCondition: JSON.stringify(this.filter)
        }),
        this.url
      ).subscribe(res => {
        this.waiting = false;
        this.paginationConfig.total = res.data.total;
        this.items = res.data.items;
        this.hideFilter = false;
    // tslint:disable-next-line:no-shadowed-variable
    }, error => {
        this.waiting = false;
        this.error.isError = true;
        this.error.Message = error.error.message;
       this.resource.errorAlert(error.error.message, 'Error');

     });
}
  setupPaginations() {
    this.waiting = true;
    return this.resource
      .query(
        Object.assign({
          count: this.paginationConfig.count,
          page: this.paginationConfig.page,
          orderByExpression: this.query.orderByExpression,
          whereCondition: JSON.stringify(this.filter)
        }),
        this.url
      ).subscribe(res => {
        this.waiting = false;
        this.paginationConfig.total = res.data.total;
        // this.resource.getService(EmitService).checkTotalCount(this.paginationConfig.total);
        this.items = res.data.items;
        this.hideFilter = false;
      }, error => {
        this.waiting = false;
        this.resource.errorAlert('An error occurred while loading resource', 'Error');
      });
  }

 
  genPagination() {

    return this.resource
      .query(
        Object.assign({
          count: this.paginationConfig.count,
          page: this.paginationConfig.page,
          orderByExpression: this.query.orderByExpression,
          whereCondition: JSON.stringify(this.filter)
        }),
        this.url
      ).pipe(map(res => {
        return res;
      }));
  }
  getPageInfoDescription(): string {
    if (this.items) {
        return 'Showing ' + (this.paginationConfig.count * (this.pager.page - 1) + 1) + ' to ' +
            (this.paginationConfig.count * (this.pager.page - 1)
                + this.items.length) + ' of ' + this.paginationConfig.total;
    }
    return '';
}

pageChanged(event) {
  this.masterSelected = false;
  this.paginationConfig.page = event;
    this.waiting = true;
    return this.resource.query(Object.assign({
        count: this.paginationConfig.count,
        page: this.paginationConfig.page,
        orderByExpression: this.query.orderByExpression,
        whereCondition: JSON.stringify(this.filter)
    }),  this.url)
        .subscribe((data) => {
        this.items = data.data.items;
        this.pager.page = this.paginationConfig.page;
        this.waiting = false;
    }, (error) => {
           this.error.isError = true;
          this.error.Message = error.error.message;
          this.resource.errorAlert(error.error.message, 'Error');
          this.waiting = false;
    });
}
toggleActive() {
  throw new Error('Method not implemented.');
}

toggleFilter() {
  this.hideFilter = !this.hideFilter;
}
getCountry() {
  // const apiUrl = '../../../assets/countries-data.json';
  return this.resource.getlist(routes.OTHERS.GETSTATES).subscribe((res) => {
    const response: Array<string> = res;
    this.states = response.sort((a, b) => a < b ? -1 : 1);
    return this.states;
  });
}

load(url?) {
  const audio = new Audio();
  audio.src =  url ? url : '../../../assets/talkingdrum.wav';
  audio.load();
  audio.play();
}

getStatuses(url) {
  return this.resource.getlist(url).subscribe((res) => {
    this.statuses = res;
  });
}

getNameAndId(url) {
  return this.resource.getlist(url).subscribe((res) => {
    this.nameAndIds = res;
  });
}

getSum(field: string, items: Array<any>): number {
  let sum = 0;
  if (items.length !== 0 || !isNullOrUndefined(items.length)) {
    for (let i = 0; i < items.length; i++) {
      // console.log(i);
      sum += items[i][field];
    }
  }
  return sum;
}

openInNewTab(url: string, behavior: string, options: string) {
  window.open(url, behavior, options);
}
getCheckedItems() {
  let checked = 0;
  this.selectedIds = [];
  if (this.checkItems) {
      for (let i = 0; i < this.checkItems.length; i++) {
          if (this.checkItems[i].selected) {
              checked++;
              this.selectedIds.push(this.checkItems[i].id);
          }
      }
  }
  return checked;
}

checkAll() {
  this.checkItems.forEach(item =>  item.selected = !this.selectedAll);
}

getCheckedItemList() {
  this.checkedList = [];
  for (let i = 0; i < this.checkItems.length; i++) {
    if (this.checkItems[i].selected) {
      this.checkedList.push(this.checkItems[i]);
    }
  }
  // console.log(this.checkedList);
  return this.checkedList;
}

checkUncheckAll() {
  for (let i = 0; i < this.checkItems.length; i++) {
    this.checkItems[i].selected = this.masterSelected;
  }
  this.getCheckedItemList();
}

isAllSelected() {
  this.masterSelected = this.checkItems.every(function(item: any) {
      return item.selected === true;
    });
  this.getCheckedItemList();
}
greet(): string {
 return Utils.greet();
}
 clock(): string {
   let result = '';
   setTimeout(() => {
     result = Utils.clock();
   }, 1000);
   return result;
}
 
}

