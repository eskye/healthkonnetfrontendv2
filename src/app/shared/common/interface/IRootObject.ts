
import { IData } from './IData';

export class IRootObject {
    id?: string;
    selected?: boolean;
    UserId?: string;
    hmoId?: string;
    providerId?: string;
    OrgId?: string;
    data?: IData;
    // update(config?): IRootObject;
    // count(config?): INameAndValue[];
}

