import { Observable } from 'rxjs/internal/Observable';
import { IRootObject } from './IRootObject';
import { IQueryOptions, IQueryVerify } from './IQueryOptions';
import { ICountModel } from './ICountModel';
// import { ConfirmSettings } from '@jaspero/ng-confirmations';


export interface IRootService<T extends IRootObject> {
    count(query: IQueryOptions, url: string): Observable<ICountModel<T>>;
    query(query: IQueryOptions, url: string): Observable<ICountModel<T>>;
    querySearch(queryOptions: IQueryVerify, url: string): Observable<T>;
    create(item: T, url: string): Observable<T>;
    update(item: T, url: string): Observable<T>;
    delete(item: T, url: string);
    get(id: number, url: string): Observable<T>;
    post(url: string, body: any, headers?: any): Observable<any>;
    getlist(url: string): Observable<T>;
    getquery(url: string, queryKey: string, param?: any): Observable<any>;
    details(id: number, url: string): any;
    toggleActive(item: T, url: string);
    successAlert(msg, title?);
    errorAlert(msg, title?);
    warningAlert(msg: any, title?: any);
    InfoAlert(msg: any, title?: any);
    getService(TService): any;
    
    // confirm(title: string, message: string, options: ConfirmSettings): Observable<any>;
}
