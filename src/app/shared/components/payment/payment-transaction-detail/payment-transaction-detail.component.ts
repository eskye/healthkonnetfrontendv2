import { Location } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import { PaymentService } from 'src/app/shared/services/payment.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Router } from '@angular/router';
import { routes } from 'src/app/shared/constant';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-payment-transaction-detail',
  templateUrl: './payment-transaction-detail.component.html',
  styleUrls: ['./payment-transaction-detail.component.scss']
})
export class PaymentTransactionDetailComponent extends BaseComponent implements OnInit {
  @Input() id: any;
  waiting = false;
    response: any = {};
  constructor(location: Location, router: Router, private title: Title, private paymentService: PaymentService) {
    super(location,  router, null, title, paymentService);
  }

  ngOnInit() {
    this.getDetail();
    this.titleRoute('HealthKonnet | Payment Transaction Detail');
  }
  getDetail() {
    this.waiting = true;
    this.paymentService.PaymentDetail(this.id).subscribe(res => {
      this.waiting = false;
      this.response = res.data;
    }, error => {
      this.waiting = false;
      this.paymentService.errorAlert(error);
    });
  }

  openTab(id) {
    this.openInNewTab(`${routes.REPORT.INVOICE}/${id}`, 'popUpWindow',
    'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
  }
}
