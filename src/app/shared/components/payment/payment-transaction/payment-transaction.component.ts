import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmitService } from '../../../services/emit.service';
import { BaseComponent } from '../../../common/baseClasses/BaseComponent';
import { routes } from '../../../constant';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-payment-transaction',
  templateUrl: './payment-transaction.component.html',
  styleUrls: ['./payment-transaction.component.scss']
})
export class PaymentTransactionComponent extends BaseComponent implements OnInit {

  @Input() items: any;
  @Output() filterAction = new EventEmitter<any>();
  @Output() reload = new EventEmitter<any>();
  @Input() waiting = false;
  @Input() totalPaid: number;
  @Input() role: string;
  constructor(location: Location, router: Router, private title: Title, private emitService: EmitService) {
    super(location, router, null, title);
  }

  ngOnInit() {
    this.emitService.countSource$.subscribe((res) => {
      this.paginationConfig.total = res;
    });
    this.titleRoute('Payment Transaction Page');
  }

  refresh() {
    this.filter = {};
    this.reload.emit(true);
  }

  filterData() {
   this.filterAction.emit(this.filter);
  }

  openTab(id) {
    this.openInNewTab(`${routes.REPORT.INVOICE}/${id}`, 'popUpWindow',
    'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
  }
}
