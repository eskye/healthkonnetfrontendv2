import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, Subject, concat, of } from 'rxjs';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { EmitService } from 'src/app/shared/services/emit.service';
import { routes } from 'src/app/shared/constant';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';

@Component({
  selector: 'app-shared-change-provider',
  templateUrl: './shared-change-provider.component.html',
  styleUrls: ['./shared-change-provider.component.scss']
})
export class SharedChangeProviderComponent extends BaseComponent implements OnInit {
  enrollees$: Observable<any[]>;
  people3Loading = false;
  textinput$ = new Subject<string>();
  private modalRef: NgbModalRef;
  @Input() items: any;
  @Output() filterAction = new EventEmitter<any>();
  @Output() reload = new EventEmitter<any>();
  @Output() sendData = new EventEmitter<any>();
  @Output() approve = new EventEmitter<any>();
  @Input() waiting = false;
  @Input() totalPaid: number;
  @Input() role: string;
  @Input() isClose = false;
  constructor(router: Router, private enrolleeService: EnrolleeService, private modalService: NgbModal, 
    private emitService: EmitService) {
    super(null, router, null, null, enrolleeService);
  }
  ngOnInit() {
    this.emitService.countSource$.subscribe((res) => {
      this.paginationConfig.total = res;
    });
    this.role = this.enrolleeService.auth.role;
    this.getCountry();
    this.getChangeProviderRequests();
    // this.getNameAndId(routes.SELECTHOSPITAL.SELECTEDHOSPITAL);
    this.loadEnrollee();
  }
  open(content) {
    this.modalRef  = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'});
    this.modalRef.result.then((result) => {
     // console.log(result);
    }, (reason) => {
     // console.log('Err!', reason);
    });
  }

  refresh() {
    this.reload.emit(true);
  }

  closeModal(){
    if(this.isClose){
      this.modalRef.close();
    }
   
  }

  getChangeProviderRequests() {
    this.waiting = true;
    this.enrolleeService.getChangeProviderRequests().subscribe((res) => {
      this.items = res.data;
      this.waiting = false;
    }, error => {
      this.waiting = false;
      this.enrolleeService.errorAlert(error.error.message, 'error');
    });
  }

   

  submitData() {
    if (confirm('The action you want to perform cannot be reverted, Do you want to proceed?')) {
    this.sendData.emit(this.item);
      this.item = {};
    }
  }
  approveAction(data) {
    this.approve.emit(data);
    this.getChangeProviderRequests();
  }

  getProviderByState(state){
    this.getNameAndId(`${routes.SELECTHOSPITAL.GETBYSTATE}?name=${state}`);
  }
  
  private loadEnrollee() {
    this.enrollees$ = concat(
      of([]), // default items
      this.textinput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.people3Loading = true)),
        switchMap(term => term.length < 3 ? of([]) :
          this.enrolleeService.searchEnrollee(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.people3Loading = false))
          )
        )
      )
    );
  }
}
