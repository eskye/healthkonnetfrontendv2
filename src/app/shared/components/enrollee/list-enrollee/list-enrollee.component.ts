import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmitService } from 'src/app/shared/services/emit.service';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { roles, routes, sroles } from 'src/app/shared/constant';

@Component({
  selector: 'app-list-enrollee',
  templateUrl: './list-enrollee.component.html',
  styleUrls: ['./list-enrollee.component.scss']
})
export class ListEnrolleeComponent extends BaseComponent implements OnInit {
  @Input() items: any;
  @Output() filterAction = new EventEmitter<any>();
  @Output() reload = new EventEmitter<any>();
  @Output() sendData = new EventEmitter<any>();
  @Output() entryCount = new EventEmitter<any>();
  @Output() submitEnrolleeData = new EventEmitter<any>();
  @Input() waiting = false;
  @Input() totalPaid: number;
  @Input() role: string;
  hmos: any;
  orgs: any;
  searchText: any;
  constructor(router: Router,   private modalService: NgbModal,
    private emitService: EmitService, private providerService: ProviderService) {
    super(null, router, null, null, providerService);
  }

  ngOnInit() {
    this.emitService.countSource$.subscribe((res) => {
      this.paginationConfig.total = res;
      this.paginationConfig.count = 20;
    });
    this.fetchInit();
  }
  fetchInit() {
    if (this.role === sroles.Provider) {
      this.providerService.fetchProviderDropDown().subscribe(res => {
        this.hmos = res[0];
        this.orgs = res[1];
      });
    } else {
      this.getNameAndId(routes.ORGANIZATION.GETORGANIZATIONLOOKUP);
    }
  }
  refresh() {
    this.filter = {};
    this.reload.emit(true);
  }
  changeCount() {
   this.entryCount.emit(this.paginationConfig.count);
  }
  filterData() {
   this.filterAction.emit(this.filter);
  }

  open(content) {
    this.modalService.open(content, { centered: true });
  }

  createEnrollee(f: NgForm) {
    if (f.valid) {
      this.submitEnrolleeData.emit(this.item);
    }
  }
}
