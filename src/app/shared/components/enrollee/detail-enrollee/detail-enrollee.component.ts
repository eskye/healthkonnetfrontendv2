import { Location } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';

@Component({
  selector: 'app-detail-enrollee',
  templateUrl: './detail-enrollee.component.html',
  styleUrls: ['./detail-enrollee.component.scss']
})
export class DetailEnrolleeComponent extends BaseComponent implements OnInit {
  @Input() id: any;
  waiting = false;
    response: any = {};
    constructor(location: Location, router: Router, private enrolleeService: EnrolleeService) {
    super(location, router, null, null);
  }
    ngOnInit() {
      this.getDetail();
    }
    getDetail() {
      if (this.id > 0) {
        this.waiting = true;
        this.enrolleeService.getDetails(this.id).subscribe(res => {
          this.waiting = false;
          this.response = res;
        }, error => {
          this.waiting = false;
          this.enrolleeService.errorAlert(error);
        });
      }
     
    }
    /* goBack(){
      this.router.navigateByUrl('/hmo/user/enrollee');
    } */
}
