import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BtnLoadingComponent } from './btn-loading/btn-loading.component';
import { FeatherIconComponent } from './feather-icon/feather-icon.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { SharedPipesModule } from '../pipes/shared-pipes.module';
import { SearchModule } from './search/search.module';
import { SharedDirectivesModule } from '../directives/shared-directives.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { LayoutsModule } from './layouts/layouts.module';
import { ConfirmModalComponent } from '../services/confirm-modal.service';
import { LoaderComponent } from './loader/loader.component';
import { BankAccountSetupComponent } from './bank-account-setup/bank-account-setup.component';
import { FormsModule } from '@angular/forms';
import { UploadStaffComponent } from './staff/upload-staff/upload-staff.component';
import { StaffListComponent } from './staff/staff-list/staff-list.component';
import { PaymentTransactionComponent } from './payment/payment-transaction/payment-transaction.component';
import { DetailClaimComponent } from './claims/detail-claim/detail-claim.component';
import { AuthorizationCodeComponent } from './claims/authorization-code/authorization-code.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ListEnrolleeComponent } from './enrollee/list-enrollee/list-enrollee.component';
import { VerifyEnrolleeComponent } from './enrollee/verify-enrollee/verify-enrollee.component';
import { DetailEnrolleeComponent } from './enrollee/detail-enrollee/detail-enrollee.component';
import { AvatarModule } from 'ngx-avatar';
import { PaymentTransactionDetailComponent } from './payment/payment-transaction-detail/payment-transaction-detail.component';
import { SharedChangeProviderComponent } from './enrollee/shared-change-provider/shared-change-provider.component';
import { HealthTipsListComponent } from './event-miscellaneous/health-tips-list/health-tips-list.component';
import { TipsDetailComponent } from './event-miscellaneous/tips-detail/tips-detail.component';
import { EventListComponent } from './event-miscellaneous/event-list/event-list.component';
import { EventDetailComponent } from './event-miscellaneous/event-detail/event-detail.component';
import { PaymentGatewayConfigComponent } from './payment-gateway-config/payment-gateway-config.component';

const components = [
  BtnLoadingComponent,
  FeatherIconComponent,
  ConfirmModalComponent,
  LoaderComponent,
  BankAccountSetupComponent,
  UploadStaffComponent,
  StaffListComponent,
  PaymentTransactionComponent,
  DetailClaimComponent,
  AuthorizationCodeComponent,
  ListEnrolleeComponent,
  VerifyEnrolleeComponent,
  DetailEnrolleeComponent,
  PaymentTransactionDetailComponent,
  SharedChangeProviderComponent,
  HealthTipsListComponent,
  TipsDetailComponent,
  EventListComponent,
  EventDetailComponent,
  PaymentGatewayConfigComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    LayoutsModule,
    SharedPipesModule,
    SharedDirectivesModule,
    SearchModule,
    NgSelectModule,
    AvatarModule,
    PerfectScrollbarModule,
    NgbModule
  ],
  declarations: components,
  exports: components
})
export class SharedComponentsModule { }
