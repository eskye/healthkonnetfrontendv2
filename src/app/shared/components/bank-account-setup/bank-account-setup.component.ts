import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { BaseComponent } from '../../common/baseClasses/BaseComponent';
import { IAccountVerificationModel } from '../../common/model/IPayment';
import { PaymentService } from '../../services/payment.service';
import { isNullOrUndefined } from 'util';


@Component({
  selector: 'app-bank-account-setup',
  templateUrl: './bank-account-setup.component.html',
  styleUrls: ['./bank-account-setup.component.css']
})
export class BankAccountSetupComponent extends BaseComponent implements OnInit {
account: IAccountVerificationModel;
 item: any = {};
 banks: Array<any>;
 isVerified = false;
  loading: boolean;
  message: any;
  @Output() bankAction = new EventEmitter<any>();
  @Input() isSaving: boolean;
  constructor(private paymentService: PaymentService) {
    super(null, null, null, null, paymentService);
  }

  ngOnInit() {
   this.init();
  }

  init() {
    this.listOfBanks();
    this.getBankDetail();
  }

  getBankDetail() {
    this.paymentService.GetBankSettings().subscribe(res =>{
      this.item = res.data;
    }, error => {
      console.log(error);
    });
  }

  listOfBanks() {
    this.paymentService.ListOfBanks().subscribe(res =>{
       this.banks = res.data.data;
    }, error => {
      this.paymentService.errorAlert('Could not load resource due to your internet connection, reload the page');
    });
  }
  bankChange() {
    this.isVerified = false;
    this.message = '';
  }

  verifyAccount() {
    this.loading = true;
    this.paymentService.VerifyAccountNumber(this.item).subscribe(res =>{
      this.loading = false;
      this.isVerified = res.data.status;
       this.message = !this.isVerified ? res.data.message : '';
       this.item.accountName = isNullOrUndefined(res.data.data) ? '' : res.data.data.account_name;
       this.item.type = isNullOrUndefined(res.data.data) ? '' : res.data.data.type;
          }, error => {
      this.loading = false;
      console.log(error);
    });
  }

  bankActionHandler() {
    this.bankAction.emit(this.item);
  }



}
