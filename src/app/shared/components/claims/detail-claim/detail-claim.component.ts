import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail-claim',
  templateUrl: './detail-claim.component.html',
  styleUrls: ['./detail-claim.component.scss']
})
export class DetailClaimComponent implements OnInit {
@Input() id: any;
@Output() approve = new EventEmitter<any>();
@Output() aClaim = new EventEmitter<any>();
@Output() reClaim = new EventEmitter<any>();
response: any = {};
role: any;
claimApprovalCommentModel: any = {};
waiting = false;
private modalRef: NgbModalRef;
private claimFormId:number ;
  Loading: boolean;
  comments: Array<any>;
  constructor(private claimService: ClaimService, private authService: AuthService,  private modalService: NgbModal) {
  }
  ngOnInit() {
    this.role = this.authService.role;
    this.getDetail();
  }

  getDetail() {
    this.waiting = true;
    this.claimService.getDetailsByVisitCode(this.id).subscribe(res => {
      this.waiting = false;
      this.response = res;
      this.claimFormId = this.response.claimId;
      this.response.investigation = res.investigationModels[0];
    }, error => {
      this.waiting = false;
      this.claimService.errorAlert(error);
    });
  }

  approveAction() {
    if (confirm('Are you sure you want to proceed with this action?')) {
      this.approve.emit(this.id);
    } else {
      return false;
    }
  }

  getClaimApprovalComment() {
    if (this.claimFormId > 0) {
      this.Loading = true;
      this.claimService.getClaimApprovalWorkFlow(this.claimFormId, 1, 50).subscribe(res => {
        this.comments = res.data;
        this.Loading = false;
      }, error => {
        this.Loading = false; 
        this.claimService.errorAlert(error.error.message);
      });
    }
  }

  open(content) {
    this.getClaimApprovalComment();
    this.modalRef  = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'});
    this.modalRef.result.then((result) => {
     console.log(result);
    }, (reason) => {
      console.log('Err!', reason);
    });
  }
 
  approveClaim() {
    if (confirm('Are you sure you want to proceed with this action?')) {
    this.claimApprovalCommentModel.isApprove = true;
     this.claimApprovalCommentModel.claimFormId = this.claimFormId;
     console.log(this.claimApprovalCommentModel);
     this.comments.push(this.claimApprovalCommentModel);
     this.claimService.approveClaim(this.claimApprovalCommentModel).subscribe(
      res => {
        this.claimService.successAlert(
          'Claim has been approved successfully',
          'Claim Approval Alert'
        );
        this.getClaimApprovalComment();
      },error => {  }); 
    }else{
      return false;
    }
  }
 rejectClaim() {
  if (confirm('Are you sure you want to proceed with this action?')) {
  this.claimApprovalCommentModel.isApprove = false;
  this.claimApprovalCommentModel.claimFormId = this.claimFormId;
  this.comments.push(this.claimApprovalCommentModel);
  this.claimService.approveClaim(this.claimApprovalCommentModel).subscribe(
    res => {
      this.claimService.InfoAlert('Claim returned for correction successfully', 'Claim Return Alert');
      this.getClaimApprovalComment();
    },error => {  });
  }else{
    return false;
  }
  }

}
