import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, map } from 'rxjs/operators';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EmitService } from 'src/app/shared/services/emit.service';
import { Router } from '@angular/router';
import { routes } from 'src/app/shared/constant';
 
@Component({
  selector: 'app-authorization-code',
  templateUrl: './authorization-code.component.html',
  styleUrls: ['./authorization-code.component.scss']
})
export class AuthorizationCodeComponent extends BaseComponent implements OnInit {
  enrollees$: Observable<any[]>;
  people3Loading = false;
  textinput$ = new Subject<string>();
  private modalRef: NgbModalRef;
  @Input() items: any;
  @Output() filterAction = new EventEmitter<any>();
  @Output() reload = new EventEmitter<any>();
  @Output() sendData = new EventEmitter<any>();
  @Output() approve = new EventEmitter<any>();
  @Input() waiting = false;
  @Input() totalPaid: number;
  @Input() role: string;
  constructor(router: Router, private enrolleeService: EnrolleeService, private modalService: NgbModal, 
    private emitService: EmitService) {
    super(null, router, null, null, enrolleeService);
  }

  ngOnInit() {
    this.emitService.countSource$.subscribe((res) => {
      this.paginationConfig.total = res;
    });
    this.loadEnrollee();
    this.getNameAndId(routes.SELECTHOSPITAL.SELECTEDHOSPITAL);
  }
  open(content) {
    this.modalRef  = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'});
    this.modalRef.result.then((result) => {
     // console.log(result);
    }, (reason) => {
     // console.log('Err!', reason);
    });
  }

  gotoClaim(item) {
   // this.router.navigate(['/products'], { queryParams: { order: 'popular', 'price-range': 'not-cheap' } });
    this.router.navigate(['/provider/enrollee/add-claim'], { queryParams: { token: `${item.code}`, 'ecode': `${item.enrolleeCode}` } });
  }
  refresh() {
    this.filter = {};
    this.reload.emit(true);
  }

  filterData() {
   this.filterAction.emit(this.filter);
  }

  submitData() {
    if (confirm('The action you want to perform cannot be revert, Do you want to proceed?')) {
    this.sendData.emit(this.item);
    }
  }
  approveAction(data){
    this.approve.emit(data);
  }
  private loadEnrollee() {
    this.enrollees$ = concat(
      of([]), // default items
      this.textinput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.people3Loading = true)),
        switchMap(term => term.length < 3 ? of([]) :
          this.enrolleeService.searchEnrollee(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.people3Loading = false))
          )
        )
      )
    );
  }
}
