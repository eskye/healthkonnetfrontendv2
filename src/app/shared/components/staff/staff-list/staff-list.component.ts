import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss']
})
export class StaffListComponent extends BaseComponent implements OnInit {
  @Input() items: any;
  @Output() filterAction = new EventEmitter<any>();
  @Output() reloadit = new EventEmitter<any>();
  @Input() waiting = false;
  @Input() role: string;
  constructor(private emitService: EmitService) {
    super();
  }

  ngOnInit() {
    this.emitService.countSource$.subscribe((res) => {
      this.paginationConfig.total = res;
    });
  }

  refreshit() {
    this.filter = {};
    this.reloadit.emit(true);
  }

  filterData() {
   this.filterAction.emit(this.filter);
  }
}
