import { Utils } from '../../../utils';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HmoService } from '../../../services/hmo.service';
import { INameAndId } from '../../../common/model/INameAndId';
import { BaseComponent } from '../../../common/baseClasses/BaseComponent';
import { BASEURL } from '../../../constant';

@Component({
  selector: 'app-shared-upload-staff',
  templateUrl: './upload-staff.component.html',
  styleUrls: ['./upload-staff.component.scss']
})
export class UploadStaffComponent extends BaseComponent implements OnInit {
item: any = {};
  packages: INameAndId[];
  filename: string;
  fileExtensionError: boolean;
  formData: FormData = new FormData();
  fileExtensionMessage: string;
  tariffService: any;
  constructor(private hmoService: HmoService ) {
    super();
  }
 @Output() submitBtn = new EventEmitter<FormData>();
 @Input() waiting = false;
  ngOnInit() {
    this.getPackages();
  }

  getPackages() {
    this.hmoService.getPackagesNameAndId().subscribe(res => {
      this.packages = res;
    }, error => {

    });
  }

  async onFileChange(event) {
    const file = <File>event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, false, true)) {
      this.fileExtensionError = true;
      if (event.target.files.length > 0) {
         this.formData.append('file', file);
         this.formData.append('id', this.item.id);
      }
    } else {
      this.fileExtensionMessage = 'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.tariffService.warningAlert(this.fileExtensionMessage);
    }
  }

  send() {
    this.submitBtn.emit(this.formData);
  }

  downloadTemplate() {
    this.openInNewTab(`${BASEURL}/Staff/Staff-upload-Template`, 'popUpWindow',
    'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');

  }

}
