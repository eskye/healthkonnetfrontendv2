import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/shared/services/navigation.service';
import { SearchService } from 'src/app/shared/services/search.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { IProfile } from 'src/app/shared/common/model/IProfile';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/views/sessions/state/user.reducers';
import * as userActions from 'src/app/views/sessions/state/user.actions';

@Component({
  selector: 'app-header-sidebar-compact',
  templateUrl: './header-sidebar-compact.component.html',
  styleUrls: ['./header-sidebar-compact.component.scss']
})
export class HeaderSidebarCompactComponent implements OnInit {
  notifications: any[];
  data: any;

  constructor(
    private navService: NavigationService,
    public searchService: SearchService,
    private ds: DataStoreService,
    private router: Router,
    private store: Store<AppState>,
    private toastrService: ToastrService,
    private auth: AuthService
  ) {
    this.notifications = [
      {
        icon: 'i-Speach-Bubble-6',
        title: 'New message',
        badge: '3',
        text: 'James: Hey! are you busy?',
        time: new Date(),
        status: 'primary',
        link: '/chat'
      },
      {
        icon: 'i-Receipt-3',
        title: 'New order received',
        badge: '$4036',
        text: '1 Headphone, 3 iPhone x',
        time: new Date('11/11/2018'),
        status: 'success',
        link: '/tables/full'
      },
      {
        icon: 'i-Empty-Box',
        title: 'Product out of stock',
        text: 'Headphone E67, R98, XL90, Q77',
        time: new Date('11/10/2018'),
        status: 'danger',
        link: '/tables/list'
      },
      {
        icon: 'i-Data-Power',
        title: 'Server up!',
        text: 'Server rebooted successfully',
        time: new Date('11/08/2018'),
        status: 'success',
        link: '/dashboard/v2'
      },
      {
        icon: 'i-Data-Block',
        title: 'Server down!',
        badge: 'Resolved',
        text: 'Region 1: Server crashed!',
        time: new Date('11/06/2018'),
        status: 'danger',
        link: '/dashboard/v3'
      }
    ];
  }

  ngOnInit() {
    this.getLoggedInUser();
    this.data = this.ds.getData('udata');
    if (!this.data) { 
      this.auth.getUserDetail().then(data => {
        this.data = data; 
        this.TriggerPasswordChange(<boolean>this.data.passwordChanged);
      }, error => {
        this.toastrService.error('An error occurred', 'Authentication Error');
      });
    } else {
       this.TriggerPasswordChange(<boolean>this.data.passwordChanged);
    }
  }
  private getLoggedInUser() {
    this.store.dispatch(new userActions.GetCurrentUser());
  }
  
  private TriggerPasswordChange(value: boolean) {
    if (!value) {
      this.router.navigateByUrl('/sessions/change-password');
     // console.log(this.data);
    }
    return false;
  }

  toggelSidebar() {
    const state = this.navService.sidebarState;
    state.sidenavOpen = !state.sidenavOpen;
    state.childnavOpen = !state.childnavOpen;
  }

  signout() {
    // this.auth.signout();
    this.store.dispatch(new userActions.RemoveUserData());
    this.ds.removeAllPersistedData();
    localStorage.clear();
    this.router.navigate(['/sessions/signin']);
}

}
