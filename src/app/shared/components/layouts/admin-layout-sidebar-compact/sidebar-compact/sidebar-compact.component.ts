import { Component, OnInit, HostListener } from '@angular/core';
import {
  NavigationService,
  IMenuItem} from '../../../../services/navigation.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Utils } from '../../../../utils';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { isNullOrUndefined } from 'util';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sidebar-compact',
  templateUrl: './sidebar-compact.component.html',
  styleUrls: ['./sidebar-compact.component.scss']
})
export class SidebarCompactComponent  implements OnInit {
  selectedItem: IMenuItem;
  nav: IMenuItem[];
  profile: any;
  role: any;

  constructor(public router: Router, public navService: NavigationService,
     private ds: DataStoreService, private authService: AuthService, private toastrService: ToastrService) {
      this.role = this.ds.getData('role');
      if (!isNullOrUndefined(this.role)) {
          this.authService.getUserDetail().then(data => {
            this.profile = data;
          }, error => {
            if (error.status === 401) {
              this.toastrService.error('Authentication Error', 'Your login session has expired');
              this.ds.removeAllPersistedData();
              localStorage.clear();
             this.router.navigateByUrl('/sessions/signin');
            }
           //  this.toastrService.error('Unknown error occurred', 'Error');
          });
      }
  }

  ngOnInit() {
    // this.navService.publishNavigationChange('sd');
    // this.navService.publishNavigationChange('Organization');
  //  this.navService.publishNavigationChange('HMO');
    // this.navService.publishNavigationChange('Enrollee'); // Uncomment To activate System Admin Sidebar menu if you are working on it
     this.navService.publishNavigationChange(this.role); // Uncomment To activate Hmo Sidebar menu if you are working on it
    this.updateSidebar();
    // CLOSE SIDENAV ON ROUTE CHANGE
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.closeChildNav();
        if (Utils.isMobile()) {
          this.navService.sidebarState.sidenavOpen = false;
        }
      });

    this.navService.menuItems$.subscribe(items => {
      this.nav = items;
      this.setActiveFlag();
    });
  }

  selectItem(item) {
    this.navService.sidebarState.childnavOpen = true;

    this.selectedItem = item;
    this.setActiveMainItem(item);
  }
  closeChildNav() {
    this.navService.sidebarState.childnavOpen = false;
    this.setActiveFlag();
  }

  onClickChangeActiveFlag(item) {
    this.setActiveMainItem(item);
  }
  setActiveMainItem(item) {
// tslint:disable-next-line: no-shadowed-variable
    this.nav.forEach(item => {
      item.active = false;
    });
    item.active = true;
  }

  setActiveFlag() {
    if (window && window.location) {
      const activeRoute = window.location.hash || window.location.pathname;
      this.nav.forEach(item => {
        item.active = false;
        if (activeRoute.indexOf(item.state) !== -1) {
          this.selectedItem = item;
          item.active = true;
        }
        if (item.sub) {
          item.sub.forEach(subItem => {
            subItem.active = false;
            if (activeRoute.indexOf(subItem.state) !== -1) {
              this.selectedItem = item;
              item.active = true;
              // subItem.active = true;
              // debugger;
            }
            if (subItem.sub) {
              subItem.sub.forEach(subChildItem => {
                if (activeRoute.indexOf(subChildItem.state) !== -1) {
                  this.selectedItem = item;
                  item.active = true;
                  subItem.active = true;
                }
              });
            }
          });
        }
      });
    }
  }

  updateSidebar() {
    if (Utils.isMobile()) {
      this.navService.sidebarState.sidenavOpen = false;
      this.navService.sidebarState.childnavOpen = false;
    } else {
      this.navService.sidebarState.sidenavOpen = true;
    }
  }
  toggelSidebar() {
    const state = this.navService.sidebarState;
    state.sidenavOpen = !state.sidenavOpen;
    state.childnavOpen = !state.childnavOpen;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.updateSidebar();
  }
}
