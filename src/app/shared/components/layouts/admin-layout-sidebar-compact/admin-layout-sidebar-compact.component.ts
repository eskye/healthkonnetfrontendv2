import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationService } from '../../../services/navigation.service';
import { SearchService } from 'src/app/shared/services/search.service';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { Router, RouteConfigLoadStart, ResolveStart, RouteConfigLoadEnd, ResolveEnd } from '@angular/router';
 

@Component({
  selector: 'app-admin-layout-sidebar-compact',
  templateUrl: './admin-layout-sidebar-compact.component.html',
  styleUrls: ['./admin-layout-sidebar-compact.component.scss']
})
export class AdminLayoutSidebarCompactComponent implements OnInit {
    moduleLoading: boolean;
  data: any;

    constructor(
      public navService: NavigationService,
      public searchService: SearchService,
      private router: Router
    ) {
      /*this.emitService.currentData$.subscribe(data => {
        this.data = data;
      //  console.log(this.userData);
     });*/
      /*  this.data = this.auth.store.getPersistedData('udata');
      this.emitService.changeData(this.data);
    if (!this.data) {
      this.auth.getUserDetail().then(userdata => {
        this.data = userdata;
        this.emitService.changeData(this.data);
      }, err => {console.error(err); });
    } */
}

    ngOnInit() {
      this.router.events.subscribe(event => {
        if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
          this.moduleLoading = true;
        }
        if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
          this.moduleLoading = false;
        }
      });
    }

}
