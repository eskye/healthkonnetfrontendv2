import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { BaseComponent } from '../../common/baseClasses/BaseComponent';
import { IAccountVerificationModel } from '../../common/model/IPayment';
import { PaymentService } from '../../services/payment.service';
import { isNullOrUndefined } from 'util';


@Component({
  selector: 'app-payment-gateway-config',
  templateUrl: './payment-gateway-config.component.html',
  styleUrls: ['./payment-gateway-config.component.css']
})
export class PaymentGatewayConfigComponent extends BaseComponent implements OnInit {
account: IAccountVerificationModel;
 item: any = {};
 banks: Array<any>;
 isVerified = false;
  loading: boolean;
  message: any;
  @Output() save = new EventEmitter<any>();
  @Input() isSaving: boolean;
  constructor(private paymentService: PaymentService) {
    super(null, null, null, null, paymentService);
  }

  ngOnInit() {
   this.init();
  }

  init() {
    this.getConfigDetail();
  }

  getConfigDetail() {
    this.paymentService.GetPaymentConfig().subscribe(res => {
      this.item = res.data;
    }, error => {
      console.log(error);
    });
  }

  isCheckedToggle() {
    this.isVerified = false;
    this.message = '';
  }

  ActionHandler() {
    this.save.emit(this.item);
  }
 
}
