import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { EmitService } from 'src/app/shared/services/emit.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-shared-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent extends BaseComponent implements OnInit {
  @Input() items: any;
  @Output()reloadit = new EventEmitter<any>();
  role: any;
  @Input() waiting = false;
  constructor(router: Router, activatedRoute: ActivatedRoute, title: Title,
    private emitService: EmitService, private authService: AuthService) {
      super(null, router, activatedRoute, title, null);
    }
    ngOnInit() {
      this.emitService.countSource$.subscribe((res) => {
        this.paginationConfig.total = res;
      });
      this.role = this.authService.role;
    }
    goTo(route) {
      this.router.navigateByUrl(route);
    }
    refreshit() {
      this.reloadit.emit(true);
    }

}
