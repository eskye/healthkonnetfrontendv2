 import { Component, OnInit, Input } from '@angular/core';
import { PostService } from 'src/app/shared/services/post.service';
import { Meta, Title } from '@angular/platform-browser';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Location } from '@angular/common';

@Component({
  selector: 'app-tips-detail',
  templateUrl: './tips-detail.component.html',
  styleUrls: ['./tips-detail.component.scss']
})
export class TipsDetailComponent extends BaseComponent implements OnInit {
  @Input() id: any;
  item: any;
  @Input() waiting = false;
  // coverPhotoUrl: string = SystemConstant.BLOGIMAGE;
  constructor(location: Location, titleService: Title,  private postService: PostService, private meta: Meta) {
    super(location, null, null, titleService, postService);
  }

  ngOnInit() {
    this.getDetails();
  }
  getDetails() {
    if (this.id > 0) {
      this.waiting = true;
      this.postService.details(this.id).subscribe(res => {
        this.item = res.data;
        this.titleRoute(res.data.title);
        this.meta.addTags([
          {property: 'og:url', content: window.location.href},
          {property: 'og:type', content: 'article'},
          {property: 'og:description', content: (<string>res.data.content).substring(0, 800) + '...Read more'},
          {property: 'og:title', content:  res.data.title},
          {property: 'og:image', content:  res.data.thumbnail}
        ]);
        this.waiting = false;
      }, error => {
        this.waiting = false;
        this.postService.errorAlert(error.error.message);
      });
    }
  }
}
