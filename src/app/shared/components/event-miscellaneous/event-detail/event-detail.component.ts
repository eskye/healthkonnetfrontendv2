import { EventService } from 'src/app/shared/services/event.service';
import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Title, Meta } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-shared-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent extends BaseComponent implements OnInit {
  @Input() id: any;
  item: any;
  @Input() waiting = false;
  role: any;
  // coverPhotoUrl: string = SystemConstant.BLOGIMAGE;
  constructor(location: Location, titleService: Title, private eventService: EventService,
     private meta: Meta, private authService: AuthService) {
    super(location, null, null, titleService, eventService);
  }

  ngOnInit() {
    this.role = this.authService.role;
    this.getDetails();
  }
  getDetails() {
    if (this.id > 0) {
      this.waiting = true;
      this.eventService.details(this.id).subscribe(res => {
        this.item = res.data;
        this.titleRoute(res.data.title);
        this.meta.addTags([
          {property: 'og:url', content: window.location.href},
          {property: 'og:type', content: 'event'},
          {property: 'og:description', content: (<string>res.data.description).substring(0, 800) + '...Read more'},
          {property: 'og:title', content:  res.data.title},
          {property: 'og:image', content:  res.data.eventFlyer}
        ]);
        this.waiting = false;
      }, error => {
        this.waiting = false;
        this.eventService.errorAlert(error.error.message);
      });
    }
  }
}
