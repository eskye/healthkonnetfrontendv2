import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { EmitService } from 'src/app/shared/services/emit.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { AuthService } from 'src/app/shared/services/auth.service';


@Component({
  selector: 'app-health-tips-list',
  templateUrl: './health-tips-list.component.html',
  styleUrls: ['./health-tips-list.component.scss']
})
export class HealthTipsListComponent extends BaseComponent implements OnInit {
  @Input() items: any;
  @Output()reloadit = new EventEmitter<any>();
  role: any;
  @Input() waiting = false;
  constructor(router: Router, activatedRoute: ActivatedRoute, title: Title,
    private emitService: EmitService, private authService: AuthService) {
      super(null, router, activatedRoute, title, null);
    }
    ngOnInit() {
      this.emitService.countSource$.subscribe((res) => {
        this.paginationConfig.total = res;
      });
      this.role = this.authService.role;
    }

    refreshit() {
      this.reloadit.emit(true);
    }
}
