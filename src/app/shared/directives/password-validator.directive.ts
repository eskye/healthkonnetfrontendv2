import { Directive, Input } from '@angular/core';
import { ValidationErrors, ValidatorFn, AbstractControl, Validator } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';


export function ValidatePassword(control: string): ValidatorFn {
  return (c: AbstractControl): ValidationErrors | null => {
    if (c.value === null || c.value.length === 0) {
      return null;
    }

    const controltoValidate = c.root.get(control);
    if (controltoValidate) {
      // {6,100}           - Assert password is between 6 and 100 characters
      // (?=.*[0-9])       - Assert a string has at least one number
      // (?!.*\s)          - Spaces are not allowed
      const expression = /^(?=.*\d)(?=.*[a-zA-Z!@#$%^&*])(?!.*\s).{6,100}$/;
      return c.value.match(expression) ? null : { 'invalidPassword': true };
    }
  };
}
  @Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[invalidpassword]'
  })
  export class PasswordValidatorDirective implements Validator {
    // tslint:disable-next-line:no-input-rename
    @Input('invalidpassword') controlToCheck: string;
    constructor() { }
    validate(c: AbstractControl): ValidationErrors | null {
      return ValidatePassword(this.controlToCheck)(c);
    }


  }
