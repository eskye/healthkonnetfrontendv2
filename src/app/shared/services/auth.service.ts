import { DataStoreService } from './data-store.service';
import { Injectable, Injector } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs/internal/Observable';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import {CookieService} from 'ngx-cookie-service';
import { ILoggedInUser, IProfile } from '../common/model/IProfile';
import { routes } from '../constant';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
 
@Injectable({
  providedIn: 'root'
})
export class AuthService extends RootService<any> {
  helper: JwtHelperService = new JwtHelperService();
  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient);
  }

  public get store(): DataStoreService {
    return this.injector.get<DataStoreService>(DataStoreService);
  }

  public get cookieService(): CookieService {
    return this.injector.get<CookieService>(CookieService);
  }


  public isAuthenticated(): boolean {
    const token = this.Token;
      if (!token) { return false; }
    return !this.helper.isTokenExpired(token);
  }

  public isLocalData(): boolean {
    const udata = this.store.getData('udata');
    console.log(udata);
    return udata ? true : false;

  }

  get role() {
    return this.store.getData('role');

  }

  get decodeToken() {
    return this.helper.decodeToken(this.Token);
  }

  get Token() {
    return this.store.getData('token');
  }

  get active() {
    return this.store.getData('active');
  }

  get cookieValue() {
    return this.cookieService.get('token');
  }


  get Key() {
    return this.store.getData('key');
  }

  get Udata() {
    return JSON.parse(this.store.getData('udata'));
  }

  loggedInUser(): Observable<ILoggedInUser> {
    if (this.isAuthenticated()) {
      return this.getlist(routes.LOGGEDIN).pipe(map(loggedUser => {
        return loggedUser.data;
      }), catchError(ErrorHandler.ErrorServerConnection));
    }
  }

   getUserDetail(): Promise<IProfile> {
    return new Promise((resolve, reject) => { 
      this.getlist(routes.LOGGEDIN)
        .subscribe(data => resolve(data.data), err => reject(err));
    });
  }

  passwordChanger(password) {
    // tslint:disable-next-line:no-shadowed-variable
    return this.post(routes.CHANGEPASSWORD, password).pipe(map(password => {
      return password;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

}
