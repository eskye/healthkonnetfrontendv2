import { Injectable, Injector } from '@angular/core';
import {IRootService} from '../common/interface/IRootService';
import {RootService} from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IPlanService extends IRootService<any> {
  create(data): Observable<any>;
  edit(data): Observable<any>;
  get(): Observable<any>;
  remove(data: any): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})
export class PlanService extends RootService<any> implements IPlanService {
  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  create(data): Observable<any> {
    return this.post(`${routes.PLAN}`, data).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  edit(data: any): Observable<any> {
    return this.update(data, `${routes.PLAN}`).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  remove(data: any): Observable<any> {
    return this.delete(data, `${routes.PLAN}`).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  get(): Observable<any> {
    return this.getlist(routes.PLAN).pipe(map(res => {
      return res;
    }));
  }

}
