import { Injectable, Injector } from '@angular/core';
import { IRootService } from '../common';
import { Observable } from 'rxjs/internal/Observable';
import { HttpEvent, HttpClient } from '@angular/common/http';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IFileManagerService extends IRootService<any> {
 uploadImage(data: FormData): Observable<HttpEvent<any>>;
 uploadProfileImage(data: FormData): Observable<HttpEvent<any>>; 
 uploadToCloud(data: FormData, url: any): Observable<HttpEvent<any>>;
}
@Injectable({
  providedIn: 'root'
})
export class FileManagerService extends RootService<any> implements IFileManagerService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  uploadImage(data: FormData): Observable<HttpEvent<any>> {
    return this.httpClient.post<any>(routes.FILEMANAGER.UPLOADBLOGIMAGE, data, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }

  uploadProfileImage(data: FormData): Observable<HttpEvent<any>> {
    return this.httpClient.post<any>(routes.FILEMANAGER.UPLOADPROFILEIMAGE, data, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }


  uploadToCloud(data: FormData, url: any): Observable<HttpEvent<any>> {
    return this.httpClient.post<any>(`${url}`, data, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }


}
