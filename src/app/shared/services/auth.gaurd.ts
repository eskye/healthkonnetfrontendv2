import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthServicew } from './auth.servicew';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurd implements CanActivate {

  constructor(
    private router: Router,
    private auth: AuthServicew
  ) { }

  canActivate() {
    if (this.auth.authenticated) {
      return true;
    } else {
      this.router.navigateByUrl('/sessions/signin');
    }
  }
}
