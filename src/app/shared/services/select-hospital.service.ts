import { Injectable, Injector } from '@angular/core';
import { RootService } from '../common/interface/RootService';
import { IRootService } from '../common/interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { routes } from '../constant';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';

export interface ISelectHospitalService extends IRootService<any> {
  selectHospital(body: any): Observable<any>;
   removeHospital(item): Observable<any>;
   assignTariff(item): Observable<any>;
   getTariffCurrent(id): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class SelectHospitalService extends RootService<any> implements ISelectHospitalService  {

  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  selectHospital(body: any): Observable<any> {
    return this.post(routes.SELECTHOSPITAL.CREATE, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  removeHospital(item): Observable<any>{
    return this.delete(item, `${routes.SELECTHOSPITAL.MAIN}`).pipe(map(res =>{
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  assignTariff(item): Observable<any> {
    return this.post(`${routes.SELECTHOSPITAL.ASSIGNTARIFF}`, item).pipe(map(res =>{
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getTariffCurrent(id): Observable<any>{
    return this.getquery(routes.SELECTHOSPITAL.GETCURRENTTARIFF, 'id', id).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
}
