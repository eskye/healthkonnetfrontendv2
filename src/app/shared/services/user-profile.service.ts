import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { IRootService, IAccount} from '../common';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IUserProfileService extends IRootService<IAccount> {
  getUserDetail(): Observable<any>;
  updateUserDetail(item: any): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class UserProfileService extends RootService<IAccount>
  implements IUserProfileService {
  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  getUserDetail(): Observable<any> {
    return this.getlist(routes.USERPROFILE.DETAIL).pipe(
      map(datas => {
        return datas.data as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }

  updateUserDetail(item: any): Observable<any> {
    return this.update(item, routes.USERPROFILE.UPDATE).pipe(map(res =>{
       return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

}
