import { Injectable, Injector } from '@angular/core';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';

export interface IEmployeeService extends IRootService<any> {
  uploadBulkList(formData: FormData): Observable<any>;
  getEnrolleeTempDetail(): Observable<any>;
 
}
@Injectable({
  providedIn: 'root'
})
export class EmployeeService extends RootService<any> implements IEmployeeService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

   uploadBulkList(formData: FormData): Observable<any> {
    return this.httpClient.post<any>(routes.STAFF.UPLOAD, formData, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }
   getEnrolleeTempDetail(): Observable<any> {
    return this.getlist(routes.STAFF.DETAIL).pipe(
      map(datas => {
        return datas.data as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }
}
