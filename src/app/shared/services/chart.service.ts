import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';

export interface IChartService extends IRootService<any>{
  chartDashboardData(): Promise<any>;
  chartHmoEnrolleeData(): Promise<any>;
  chartHmoEnrolleeVisitData(item): Promise<any>;
  chartProviderEnrolleeVisitData(item): Promise<any>;
}

@Injectable({
  providedIn: 'root'
})
export class ChartService extends RootService<any> implements IChartService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  async chartDashboardData(): Promise<any> {
    return await this.getlist(routes.GETCHARTDATA.ORGDASHBOARD).toPromise();
  }
  async chartHmoEnrolleeData(): Promise<any> {
    return await this.getlist(routes.GETCHARTDATA.HMOENROLLEECHART).toPromise();
  }
  async chartHmoEnrolleeVisitData(item): Promise<any> {
    return await this.getquery(routes.GETCHARTDATA.HMOENROLLEEVISIT, item.q, item.param).toPromise();
  }
  async chartProviderEnrolleeVisitData(item): Promise<any> {
    return await this.getquery(routes.GETCHARTDATA.PROVIDERENROLLEEVISIT, item.q, item.param).toPromise();
  }
}
