import { Injectable, Injector } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { EmitService } from './emit.service';
import { isNullOrUndefined } from 'util';
declare const Pusher: any;
@Injectable({
  providedIn: 'root'
})
export class PusherService  {
  pusher: any;
  public channel: any;
  public socketId: any;
  public channelName: any = '';
  constructor(injector: Injector) {
   const emitService = injector.get(EmitService);
    emitService.currentChannelName$.subscribe((data) =>{
      if(!isNullOrUndefined(data)){
       // alert(data);
        this.pusher = new Pusher(environment.pusher.key, {
          cluster: environment.pusher.cluster,
          encrypted: true
        });
        this.channel = this.pusher.subscribe(data);
        this.pusher.connection.bind('connected', () => {
          this.socketId = this.pusher.connection.socket_id;
          console.log(this.socketId);
        });
      }
    });
   }

   get socket() {
     this.pusher.connection.bind('connected', () => {
       this.socketId = this.pusher.connection.socket_id;
       // alert(this.socketId);
     });
     return this.socketId;
   }


}
