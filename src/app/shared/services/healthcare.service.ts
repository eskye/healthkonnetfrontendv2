import { Injectable, Injector } from '@angular/core';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';

export interface IHealthCareService extends IRootService<any> {
  uploadBulkList(formData: FormData): Observable<any>;
  createSingle(item: any): Observable<any>;
  searchHealthCare(search): Observable<INameAndId[]>;

}
@Injectable({
  providedIn: 'root'
})
export class HealthcareService extends RootService<any> implements IHealthCareService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

   uploadBulkList(formData: FormData): Observable<any> {
    return this.httpClient.post<any>(routes.HEALTHCARE.UPLOAD, formData, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }

   createSingle(item: any): Observable<any> {
    return this.post(routes.HEALTHCARE.CREATE, item).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }

   searchHealthCare(search): Observable<INameAndId[]> {
    return this.getquery(`${routes.HEALTHCARE.SEARCH}`, 'name' , search).pipe(map(res => {
      return res as any;
    }));
   }
}
