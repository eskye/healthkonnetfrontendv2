import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { IRootService, IAccount, ICountModel} from '../common';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';
import { AuthService } from './auth.service';

export interface IEnrolleeService extends IRootService<IAccount> {
  getEnrolleeByUserProfileId(): Observable<any>;
  updateUserDetail(item: any): Observable<any>;
  createEnrollee(item: any): Observable<any>;
  addEnrolleePlan(item:any): Observable<any>;
  verifyEnrollee(item:any): Observable<any>;
  searchEnrollee(search): Observable<INameAndId[]>;
  getDetails(id): Observable<any>;
  changeProvider(item: any): Observable<any>;
  approveChangeProviderRequest(item: any): Observable<any>;
  getCurrentPackage(): Observable<any>;

}
@Injectable({
  providedIn: 'root'
})
export class EnrolleeService extends RootService<IAccount>
  implements IEnrolleeService {
  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  getEnrolleeByUserProfileId(): Observable<any> {
    return this.getlist(routes.ENROLLEE.DETAIL).pipe(
      map(datas => {
        return datas.data as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }

  updateUserDetail(item: any): Observable<any> {
    return this.update(item, routes.ENROLLEE.UPDATE).pipe(map(res =>{
       return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  createEnrollee(item: any): Observable<any> {
    return this.post(routes.ENROLLEE.UPDATE, item).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  addEnrolleePlan(item: any): Observable<any> {
    return this.update(item, routes.ENROLLEE.ADDPLAN).pipe(map(res =>{
       return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
   verifyEnrollee(item: any): Observable<any>{
    return this.getlist(`${routes.ENROLLEE.VERIFYENROLLEE}?name=${item.name}&providerId=${item.providerId}`)
    .pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
   }
   searchEnrollee(search): Observable<INameAndId[]> {
    return this.getquery(`${routes.ENROLLEE.SEARCH}`, 'name' , search).pipe(map(res => {
      return res as any;
    }));
   }

   getDetails(id): Observable<any> {
    return this.get(id, routes.ENROLLEE.UPDATE).pipe(map(res =>{
      return res.data as any;
    }));
  }

  changeProvider(item: any): Observable<any> {
    return this.post(routes.ENROLLEE.CHANGEPROVIDER, item).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  approveChangeProviderRequest(item: any): Observable<any> {
    return this.post(routes.HMO.APPROVEREQUEST, item).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getChangeProviderRequests(): Observable<ICountModel<any>> {
    return this.getlist(routes.ENROLLEE.GETCHANGEPROVIDERREQUESTS).pipe(
      map(data => {
        return data as any;
      }), catchError(ErrorHandler.ErrorServerConnection)
    );
  }

  getCurrentPackage(): Observable<any> {
    return this.getlist(routes.ENROLLEE.GETCURRENTPACKAGE).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection)
    );
  }
  generateEnrolleeBarcode(): Observable<any> {
    return this.post(routes.GENERATEBARCODE, '').pipe(
      map(res => {
        return res.data;
      }), catchError(ErrorHandler.ErrorServerConnection)
    );
  }
  public get auth(): AuthService {
    return this.injector.get(AuthService);
  }
}
