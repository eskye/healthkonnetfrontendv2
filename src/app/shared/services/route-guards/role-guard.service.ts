import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {

  constructor(public auth: AuthService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    const expectedRole = route.data.expectedRole;
    const role = this.auth.role;
    if (!this.auth.isAuthenticated() || role !== expectedRole) {
      // not logged in so redirect to login page with the return url and return false
      this.router.navigate(['/sessions/signin'], { queryParams: { returnUrl: state.url }});
      return false;
    } 
    return true;
  }

}

