import { ErrorHandler } from './../common/ErrorHandler';
import { Injectable, Injector } from '@angular/core';
import { RootService } from '../common/interface/RootService';
import { IRootService } from '../common/interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { routes } from '../constant';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/operators';
 
export interface ICompanyService extends IRootService<any> {
  createCompany(body: any): Observable<any>;
  addSetting(item:any): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class CompanyService extends RootService<any> implements ICompanyService  {

  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  createCompany(body: any): Observable<any> {
    return this.post(routes.ORGANIZATION.CREATEORG, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  addSetting(body: any): Observable<any> {
    return this.post(routes.ORGANIZATION.ADDSETTING, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
}
