/// <reference types="@types/googlemaps" />

import {ElementRef, Injectable, Injector, NgZone} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {EmitService} from './emit.service';


@Injectable({
  providedIn: 'root'
})
export class MapService {
locationResult: any = {};
  constructor( private mapsAPILoader: MapsAPILoader,
               private ngZone: NgZone, private inject: Injector) { }

      get emitService(): EmitService {
        return this.inject.get(EmitService);
      }

  autocompleteLocation(searchElement: ElementRef) {

    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(searchElement.nativeElement, {
        types: ['address']});
      autocomplete.setComponentRestrictions({
        'country': ['ng']
      });

      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          // this.latitude = place.geometry.location.lat();
          // this.longitude = place.geometry.location.lng();
          // this.location = place.formatted_address;

           this.locationResult = {
              latitude : place.geometry.location.lat(),
              longitude: place.geometry.location.lng(),
              location: place.formatted_address
            };
           this.emitService.getMapResponse(this.locationResult);
        });
      });
    });
    return this.locationResult;
  }
}
