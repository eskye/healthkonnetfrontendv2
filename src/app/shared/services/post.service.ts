import { IRootService } from './../common/interface/IRootService';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';
export interface IPostService extends IRootService<any> {
  createPost(body): Observable<any>;
  details(id): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class PostService extends RootService<any> implements IPostService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  createPost(body): Observable<any> {
    return this.post(routes.POST.BLOG, body).pipe(map(res => {
      return res as any;
    }));
  }
  details(id): Observable<any> {
    return this.get(id, routes.POST.BLOG).pipe(map(res => {
      return res as any;
    }));
  }
}
