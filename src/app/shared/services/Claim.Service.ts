import { Injectable, Injector } from '@angular/core';
import { RootService } from '../common/interface/RootService';
import { IRootService } from '../common/interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { routes } from '../constant';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';

export interface IClaimService extends IRootService<any> {
  createClaimBatch(body: any): Observable<any>;
  createClaimForm(body: any): Observable<any>;
  submitClaimFinal(body: any): Observable<any>;
  getClaimNameAndId(): Observable<INameAndId[]>;
  searchProvider(search): Observable<INameAndId[]>;
  getHmoForProviderNameAndId(): Observable<INameAndId[]>;
  getOrgForProviderNameAndId(): Observable<INameAndId[]>;
  getDetails(id): Observable<any>;
  requestAuthorizationCode(body: any): Observable<any>;
  approveAuthorizationCode(item: any): Observable<any>;
  approveClaim(body: any): Observable<any>;
  getDetailsByVisitCode(visitCode): Observable<any>;
  getClaimApprovalWorkFlow(id, page: number, pageSize:number): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class ClaimService extends RootService<any> implements IClaimService  {

  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  createClaimBatch(body: any): Observable<any> {
    return this.post(routes.CLAIM.MAIN, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  submitClaimFinal(body: any): Observable<any> {
    return this.post(routes.CLAIM.ADDCLAIMFORM, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  createClaimForm(body: any): Observable<any> {
    return this.post(routes.CLAIM.CLAIMFORM, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  getClaimNameAndId(): Observable<INameAndId[]> {
    return this.getlist(`${routes.CLAIM.LOOKUP}`).pipe(map(res => {
      return res.data as any;
    }));
   }
   searchProvider(search): Observable<INameAndId[]> {
    return this.getquery(`${routes.PROVIDER.SEARCH}`, 'name' , search).pipe(map(res => {
      return res as any;
    }));
   }
   getDetails(id): Observable<any> {
    return this.get(id, routes.CLAIM.CLAIMFORM).pipe(map(res =>{
      return res.data as any;
    }));
  }
  getDetailsByVisitCode(visitCode): Observable<any> {
    return this.getlist(`${routes.CLAIM.CLAIMFORMDETAILBYVISITCODE}/${visitCode}`).pipe(map(res =>{
      return res.data as any;
    }));
  }
  getHmoForProviderNameAndId(): Observable<INameAndId[]> {
    return this.getlist(routes.GETENROLLEEHMOINPROVIDER).pipe(map(res => {
      return res.data;
    }));
  }
  getOrgForProviderNameAndId(): Observable<INameAndId[]> {
    return this.getlist(routes.GETENROLLEEORGINPROVIDER).pipe(map(res => {
      return res.data;
    }));
  }

  fetchProviderDropDown(): Observable<any> {
    return forkJoin([this.getHmoForProviderNameAndId(), this.getOrgForProviderNameAndId()]);
 }

 getDrugPrice(item: any, hid): Observable<any> {
  return this.getlist(`${routes.PROVIDER.VALIDATEPRICE}?name=${item}&hid=${hid}`)
  .pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
 }
 requestAuthorizationCode(body: any): Observable<any> {
  return this.post(routes.CLAIM.REQUESTAUTHORIZATIONCODE, body).pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
}

approveAuthorizationCode(body: any): Observable<any> {
  return this.update(body, `${routes.CLAIM.APPROVEAUTHORIZATIONCODE}`).pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
}
approveClaim(item:any): Observable<any> {
  return this.post(`${routes.CLAIM.POSTAPPROVECLAIM}`, item).pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
}
 getServicePrice(item: any, hid): Observable<any> {
  return this.getlist(`${routes.PROVIDER.VALIDATESERVICE}?name=${item}&hid=${hid}`)
  .pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
 }
 
 getClaimApprovalWorkFlow(id, page: number, pageSize = 50): Observable<any> {
  return this.getlist(`${routes.CLAIM.GETCLAIMAPPROVALCOMMENTS}?id=${id}&page=${page}&count=${pageSize}`).pipe(map(res => {
    return res;
  }));
}
 
}
