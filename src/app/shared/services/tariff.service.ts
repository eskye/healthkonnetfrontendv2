import { Injectable, Injector } from '@angular/core';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';

export interface ITariffService extends IRootService<any> {
  uploadTarrifList(formData: FormData): Observable<any>;
  uploadDrugTarrifList(formData: FormData): Observable<any>;
  createTariffCategory(item: any): Observable<any>;
  createDrugTariff(item: any): Observable<any>;
  createTariff(item: any): Observable<any>;
  getNameAndId(): Observable<INameAndId[]>;
  updateDrugTariff(item): Observable<any>;
  removeTariff(item): Observable<any>;
  removeDrugTariff(item): Observable<any>;
  getServiceTypes(id): Observable<INameAndId[]>;
}
@Injectable({
  providedIn: 'root'
})
export class TariffService extends RootService<any> implements ITariffService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  createTariffCategory(item: any): Observable<any> {
    return this.post(`${routes.TARIFF.CATEGORY}/create`, item).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }
   createDrugTariff(item: any): Observable<any>{
    return this.post(`${routes.TARIFF.DRUGUPDATE}/create`, item).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
   }
   createTariff(item: any): Observable<any>{
    return this.post(`${routes.TARIFF.TARIFFUPDATE}/create`, item).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
   }
   uploadTarrifList(formData: FormData): Observable<any> {
    return this.httpClient.post<any>(routes.TARIFF.TARIFFUPLOAD, formData, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }

   uploadDrugTarrifList(formData: FormData): Observable<any> {
    return this.httpClient.post<any>(routes.TARIFF.DRUGUPLOAD, formData, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }
   getNameAndId():Observable<INameAndId[]> {
     return this.getlist(`${routes.TARIFF.CATEGORY}/GetNameAndIds`).pipe(map(res => {
       return res.data;
     }), catchError(ErrorHandler.ErrorServerConnection));
   }

   updateDrugTariff(item): Observable<any> {
    return this.update(item, routes.TARIFF.DRUGUPDATE).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  updateTariffList(item): Observable<any> {
    return this.update(item, routes.TARIFF.TARIFFUPDATE).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  removeTariff(item): Observable<any>{
    return this.delete(item, routes.TARIFF.TARIFFUPDATE).pipe(map(res =>{
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
removeDrugTariff(item): Observable<any>{
  return this.delete(item, routes.TARIFF.DRUGUPDATE).pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
 }

 getServiceTypes(id): Observable<INameAndId[]> {
  return this.getlist(`${routes.TARIFF.GETSERVICES}?hid=${id}`).pipe(map(res => {
    return res.data;
  }));
}
}
