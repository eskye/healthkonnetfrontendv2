import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { Observable } from 'rxjs';
import { ICounter } from '../common/model/ICounter';
import { map, catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IDashboardService extends IRootService<any> {
  getDashboard(url): Observable<ICounter>;
}

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends RootService<any> implements IDashboardService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  getDashboard(url): Observable<ICounter> {
    return this.getlist(url).pipe(map(res => {
      return res.data as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
}
