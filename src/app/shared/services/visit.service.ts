import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { IRootService, IAccount} from '../common';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';
import { ErrorHandler } from '../common/ErrorHandler';


export interface IVisitService extends IRootService<IAccount> {
  generateVisitCode(item: any): Observable<any> ;
}
@Injectable({
  providedIn: 'root'
})
export class VisitService extends RootService<IAccount>
  implements IVisitService {
  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }
  generateVisitCode(item: any): Observable<any> {
    return this.post(`${routes.VISIT}?name=${item.name}`, '').pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

}
