import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { IRootService, IAccount, IUserLogin, ITokenparams, IResetPassword } from '../common';
import { IEmployeeData } from '../common/model/IEnrollee';
import { RootService } from '../common/interface/RootService';
import { routes } from '../constant';
import { AuthService } from './auth.service';
import { DataStoreService } from './data-store.service';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IAccountService extends IRootService<IAccount> {
  login(logindata: IUserLogin): Observable<ITokenparams>;
  logout(): Observable<any>;
  sendPasswordMail(email: string);
  resetPassword(body: IResetPassword);
  activate(id: string, pin: string): Observable<IEmployeeData>;
  getEmployeeTemp(id: string): Observable<IEmployeeData>;
  setupAccount(account: IAccount): Observable<IAccount>;
  createAccounts(body: any): Observable<IAccount>;
  createAnonymous(body: IAccount): Observable<IAccount>;
  getCurrentLoggedInUser(userid?: string): Observable<any>;
  //OnBoarding of Enrollee

  activatePin(item: any): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class AccountService extends RootService<IAccount>
  implements IAccountService {
  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  login(logindata: IUserLogin): Observable<ITokenparams> {

    const reqHeader = {
      'Content-Type': 'application/json',
      'No-Auth': 'True'
    };

    return this.post(routes.LOGIN, logindata, reqHeader).pipe(
      map(data => {
        return data as any;
      }));
  }


  sendPasswordMail(email: string) {
    return this.post(routes.FORGOTPASSWORD, { email: email }).pipe(
      map(res => {
        return res;
      }), catchError(ErrorHandler.ErrorServerConnection) );
  }

  resetPassword(body: IResetPassword) {
    return this.post(routes.RESETPASSWORD, body).pipe(
      map(res => {
        return res;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }

  activate(id: string, pin: string): Observable<IEmployeeData> {
    const data = 'key=' + id + '&pin=' + pin;
    const reqHeader = { 'Content-Type': 'application/x-www-form-urlencoded' };
    return this.post(routes.ACTIVATE_ENROLLEE, data, reqHeader).pipe(
      map(activated => {
        return activated as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getEmployeeTemp(id: string): Observable<IEmployeeData> {
    const data = 'key=' + id;
    const reqHeader = { 'Content-Type': 'application/x-www-form-urlencoded' };
    return this.post(routes.GET_ENROLLEE, data, reqHeader).pipe(
      map(datas => {
        return datas as any;
      }), catchError(ErrorHandler.ErrorServerConnection) );
  }

  getEmployeeTempByEmail(email: string): Observable<IEmployeeData> {
    return this.get(email, routes.GETTEMPENROLLEE).pipe(
      map(datas => {
        return datas as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }
  setupAccount(account: IAccount): Observable<IAccount> {
    return this.post(routes.REGISTER, account).pipe(
      map(res => {
        return res;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }
  OpenAccountAfterActivation(account: IAccount): Observable<IAccount> {
    return this.post(routes.OPENACCOUNT, account).pipe(
      map(res => {
        return res;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }

  logout(): Observable<any> {
    if (this.AuthService.isAuthenticated()) {
      const reqHeader = { 'Content-Type': 'application/json' };
      return this.post(
        routes.LOGOUT,
        { userId: this.AuthService.Key },
        reqHeader
      ).pipe(
        map(res => {
          return res;
        }), catchError(ErrorHandler.ErrorServerConnection));
    }
  }
  createAccounts(body: any): Observable<IAccount> {
    return this.post(routes.REGISTER, body)
      .pipe(map(res => {
        return res;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }


  createAnonymous(body: IAccount): Observable<IAccount> {
    return this.post(routes.CREATEANONYMOUS, body).pipe(
      map(res => {
        return res;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }
  getCurrentLoggedInUser(userid?: any): Observable<any> {
    return this.getlist(routes.LOGGEDIN, '').pipe(
      map(datas => {
        return datas as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
  }
  activatePin(item: any): Observable<any>{
    return this.post(routes.ACCOUNT.ONBOARD.PIN, item).pipe(map(res =>{
      return res.data as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  get AuthService(): AuthService {
    return this.getService(AuthService);
  }
  get store(): DataStoreService {
    return this.getService(DataStoreService);
  }
}
