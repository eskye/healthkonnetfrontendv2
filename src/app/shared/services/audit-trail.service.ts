import { Injectable, Injector } from '@angular/core';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';

export interface IAuditTrailService extends IRootService<any> {
  getAuditActions(): Observable<any>;
  getAuditSections(): Observable<any>;
  getActions(): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class AuditTrailService extends RootService<any> implements IAuditTrailService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  getAuditActions(): Observable<any>{
    return this.getlist(routes.AUDITTRAIL.GETAUDITACTIONS).pipe(map(res => {
      return res;
    }));
  }
  getAuditSections(): Observable<any>{
    return this.getlist(routes.AUDITTRAIL.GETAUDITSECTIONS).pipe(map(res => {
      return res;
    }));
  }
  getActions(): Observable<any>{
    return forkJoin([this.getAuditActions()]);
 }
}
