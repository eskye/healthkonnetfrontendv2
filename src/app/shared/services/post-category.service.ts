import { Injectable, Injector } from '@angular/core';
import {IRootService} from '../common/interface/IRootService';
import {RootService} from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IPostCategoryService extends IRootService<any> {
  create(data): Observable<any>;
  edit(data): Observable<any>;
  getNameAndIds(): Observable<any>;
  remove(data: any): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})
export class PostCategoryService extends RootService<any> implements IPostCategoryService {
  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  create(data): Observable<any> {
    return this.post(`${routes.POST.CATEGORY}`, data).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  edit(data: any): Observable<any> {
    return this.update(data, `${routes.POST.CATEGORY}`).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  remove(data: any): Observable<any> {
    return this.delete(data, `${routes.POST.CATEGORY}`).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  getNameAndIds(): Observable<any> {
    return this.getlist(routes.POST.CATEGORY + '/getNameAndIds').pipe(map(res => {
      return res;
    }));
  }

}
