import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import * as firebase from 'firebase';
import { mergeMapTo } from 'rxjs/operators';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FirebaseMessagingService {
messaging = firebase.messaging();
currentMessage = new BehaviorSubject(null);
  constructor(private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging) {
      this.angularFireMessaging.messaging.subscribe((_messaging) => {
       _messaging.onMessage = _messaging.onMessage.bind(_messaging);
       _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      });
    }


    updateToken(userId, token) {
      this.angularFireAuth.authState.pipe(take(1)).subscribe(user => {
        // tslint:disable-next-line: curly
        const data = { [userId]: token };
        this.angularFireDB.object('fcmTokens/').update(data);
      });
    }

   /*requestPermission(){
      // tslint:disable-next-line: deprecation
      this.messaging.requestPermission()
      .then(() => {
        console.log('Notification permission granted.');
        return this.messaging.getToken();
      })
      .then(token => {
        console.log(token)
        this.updateToken(token)
      })
      .catch((err) => {
        console.log('Unable to get permission to notify.', err);
      });
    }*/

    getPermission(userId) {
      this.angularFireMessaging.requestToken.subscribe(
        (token) => {
          console.log(token);
          this.updateToken(userId, token);
        },
        (err) => {
          console.error('Unable to get permission to notify.', err);
        }
      );
    }

    receiveMessage() {
      this.angularFireMessaging.messages.subscribe(
        (payload) => {
          console.log('new message received. ', payload);
          this.currentMessage.next(payload);
        });
    }
}
