import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { IProfile } from '../common/model/IProfile';

@Injectable({
  providedIn: 'root'
})
export class EmitService {

  data: any;
  isactive: boolean;
  isOpen: boolean;
  barcode: boolean;
  statelocals: any;
  totalCountChild: number;
  pageHeader: any;
  breadcrumbName: any;
  __mapResponse: any;
  _isLoggingOut: boolean;
  _pusherChannel: string;
  pnots: Array<any>;
  hnots: Array<any>;
  // Observable variable

  private messageSource = new BehaviorSubject<boolean>(this.isactive);
  currentMessage$ = this.messageSource.asObservable();

  private channelNameSource = new BehaviorSubject<string>(this._pusherChannel);
  currentChannelName$ = this.channelNameSource.asObservable();

  private logoutSource = new BehaviorSubject<boolean>(this._isLoggingOut);
  logoutMessage$ = this.logoutSource.asObservable();

  private sidebarOpenSource = new BehaviorSubject<boolean>(this.isOpen);
  currentSideBarState$ = this.sidebarOpenSource.asObservable();

  private dataBarcodeSource = new BehaviorSubject<boolean>(this.barcode);
  currentCheck$ = this.dataBarcodeSource.asObservable();

  private dataSource = new BehaviorSubject<IProfile>(this.data);
  currentData$ = this.dataSource.asObservable();

  private stateSource = new BehaviorSubject<any>(this.statelocals);
  stateData$ = this.stateSource.asObservable();

  // Page Header Message Hub
  private headerHub = new BehaviorSubject<any>(this.pageHeader);
  headerHubData$ = this.headerHub.asObservable();

  private breadcrumb = new BehaviorSubject<any>(this.breadcrumbName);
  breadcrumbData$ = this.breadcrumb.asObservable();

  private mapResponse = new BehaviorSubject<any>(this.__mapResponse);
  mapResponse$ = this.mapResponse.asObservable();

  private DataCountSource = new BehaviorSubject<number>(this.totalCountChild);
  countSource$ = this.DataCountSource.asObservable();

  private pnotSource  = new BehaviorSubject<Array<any>>(this.pnots);
  pnotSource$ = this.pnotSource.asObservable();

  private hnotSource  = new BehaviorSubject<Array<any>>(this.hnots);
  hnotSource$ = this.hnotSource.asObservable();

  constructor() { }

  changeMessage(message: boolean) {
    this.messageSource.next(message);
  }
  checkTotalCount(total: number) {
    this.DataCountSource.next(total);
  }
  notifyLogout(checker: boolean) {
    this.logoutSource.next(checker);
  }
  getPageHeaderMessage(title: any) {
    this.headerHub.next(title);
  }

  getBreadCrumbMessage(breadcrumb: any) {
    this.breadcrumb.next(breadcrumb);
  }

  changeSideBarState(message: boolean) {
    this.sidebarOpenSource.next(message);
  }

  checkBarcodeStatus(message: boolean) {
    this.dataBarcodeSource.next(message);
  }

  changeData(data: any) {
    this.dataSource.next(data);
  }

  changeLocalState(state: any) {
    this.stateSource.next(state);
  }

  getMapResponse(response: any) {
    this.mapResponse.next(response);
  }

  getChannelName(name: string) {
    this.channelNameSource.next(name);
  }

  pushPnots(items: Array<any>) {
    this.pnotSource.next(items);
  }

  pushHnots(items: Array<any>) {
    this.hnotSource.next(items);
  }
}
