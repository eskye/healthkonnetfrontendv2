import { Injectable, Injector } from '@angular/core';
import { RootService } from '../common/interface/RootService';
import { IRootService } from '../common/interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { routes } from '../constant';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';

export interface IProviderService extends IRootService<any> {
  createProvider(body: any): Observable<any>;
  getProviderNameAndId(): Observable<INameAndId[]>;
  searchProvider(search): Observable<INameAndId[]>;
  getDetails(id): Observable<any>;
  getHmoForProviderNameAndId(): Observable<INameAndId[]>;
  getOrgForProviderNameAndId(): Observable<INameAndId[]>;
  fetchProviderDropDown(): Observable<any>;
  getDrugPrice(item: any, hid): Observable<any>;
  getServicePrice(item: any, hid): Observable<any>;
   
}
@Injectable({
  providedIn: 'root'
})
export class ProviderService extends RootService<any> implements IProviderService  {

  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  createProvider(body: any): Observable<any> {
    return this.post(routes.SETUPPROVIDER, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getProviderNameAndId(): Observable<INameAndId[]> {
    return this.getlist(`${routes.PROVIDER.NAMEANDID}`).pipe(map(res => {
      return res as any;
    }));
   }
   searchProvider(search): Observable<INameAndId[]> {
    return this.getquery(`${routes.PROVIDER.SEARCH}`, 'name' , search).pipe(map(res => {
      return res as any;
    }));
   }
   getDetails(id): Observable<any> {
    return this.get(id, routes.PROVIDER.PROVIDER).pipe(map(res =>{
      return res.data as any;
    }));
  }

  getHmoForProviderNameAndId(): Observable<INameAndId[]> {
    return this.getlist(routes.GETENROLLEEHMOINPROVIDER).pipe(map(res => {
      return res.data;
    }));
  }
  getOrgForProviderNameAndId(): Observable<INameAndId[]> {
    return this.getlist(routes.GETENROLLEEORGINPROVIDER).pipe(map(res => {
      return res.data;
    }));
  }

  fetchProviderDropDown(): Observable<any> {
    return forkJoin([this.getHmoForProviderNameAndId(), this.getOrgForProviderNameAndId()]);
 }

 getDrugPrice(item: any, hid): Observable<any> {
  return this.getlist(`${routes.PROVIDER.VALIDATEPRICE}?name=${item}&hid=${hid}`)
  .pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
 }

 getServicePrice(item: any, hid): Observable<any> {
  return this.getlist(`${routes.PROVIDER.VALIDATESERVICE}?name=${item}&hid=${hid}`)
  .pipe(map(res => {
    return res;
  }), catchError(ErrorHandler.ErrorServerConnection));
 }
}
