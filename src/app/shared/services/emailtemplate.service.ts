import { Injectable, Injector } from '@angular/core';
import {IRootService} from '../common/interface/IRootService';
import {RootService} from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IEmailTemplateService extends IRootService<any> {
  detail(data: any): Observable<any>;
  getEmailTypes(): Observable<any>;
  getPlaceholders(emailType: any): Observable<any>;
  updateTemplate(item: any): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})
export class EmailTemplateService extends RootService<any> implements IEmailTemplateService {
  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }
 
 
  getEmailTypes(): Observable<any> {
    return this.getlist(routes.EMAILTEMPLATE.GETEMAILTEMPLATETYPES).pipe(map(res => {
      return res;
    }));
  }

  detail(id: any): Observable<any> {
    return this.get(id, routes.EMAILTEMPLATE.DETAIL).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getPlaceholders(id: any): Observable<any> {
    return this.get(id, routes.EMAILTEMPLATE.PLACEHOLDERS).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  updateTemplate(item: any): Observable<any> {
    return this.update(item, routes.EMAILTEMPLATE.MAIN).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }


}
