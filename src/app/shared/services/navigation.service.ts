
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { sroles } from '../constant';

export interface IMenuItem {
  id?: string;
  title?: string;
  description?: string;
  type: string; // Possible values: link/dropDown/extLink
  name?: string; // Used as display text for item and title for separator type
  state?: string; // Router state
  icon?: string; // Material icon name
  tooltip?: string; // Tooltip text
  disabled?: boolean; // If true, item will not be appeared in side nav.
  sub?: IChildItem[]; // Drop down items
  badges?: IBadge[];
  active?: boolean;
}
export interface IChildItem {
  id?: string;
  parentId?: string;
  type?: string;
  name: string; // Display text
  state?: string; // Router state
  icon?: string;
  sub?: IChildItem[];
  active?: boolean;
}

interface IBadge {
  color: string; // primary/accent/warn/hex color codes(#fff000)
  value: string; // Display text
}

interface ISidebarState {
  sidenavOpen?: boolean;
  childnavOpen?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  public sidebarState: ISidebarState = {
    sidenavOpen: true,
    childnavOpen: false
  };
  constructor() {}

  defaultMenu: IMenuItem[] = [
    {
      name: 'System Admin',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-Bar-Chart',
      sub: [
        {
          icon: 'i-Clock-3',
          name: 'Version 1',
          state: '/dashboard/v1',
          type: 'link'
        },
        {
          icon: 'i-Clock-4',
          name: 'Version 2',
          state: '/dashboard/v2',
          type: 'link'
        },
        {
          icon: 'i-Over-Time',
          name: 'Version 3',
          state: '/dashboard/v3',
          type: 'link'
        },
        {
          icon: 'i-Clock',
          name: 'Version 4',
          state: '/dashboard/v4',
          type: 'link'
        }
      ]
    },
    {
      name: 'UI kits',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing.',
      type: 'dropDown',
      icon: 'i-Library',
      sub: [
        {
          icon: 'i-Bell',
          name: 'Alerts',
          state: '/uikits/alerts',
          type: 'link'
        },
        {
          icon: 'i-Split-Horizontal-2-Window',
          name: 'Accordions',
          state: '/uikits/accordions',
          type: 'link'
        },
        {
          icon: 'i-Medal-2',
          name: 'Badges',
          state: '/uikits/badges',
          type: 'link'
        },
        {
          icon: 'i-Arrow-Right-in-Circle',
          name: 'Buttons',
          type: 'dropDown',
          sub: [
            {
              name: 'Bootstrap Buttons',
              state: '/uikits/buttons',
              type: 'link'
            },
            {
              name: 'Loding Buttons',
              state: '/uikits/buttons-loading',
              type: 'link'
            }
          ]
        },
        {
          icon: 'i-ID-Card',
          name: 'Cards',
          state: '/uikits/cards',
          type: 'link'
        },
        {
          icon: 'i-Line-Chart-2',
          name: 'Cards metrics',
          state: '/uikits/cards-metrics',
          type: 'link'
        },
        {
          icon: 'i-Credit-Card',
          name: 'Cards widget',
          state: '/uikits/cards-widget',
          type: 'link'
        },
        {
          icon: 'i-Full-Cart',
          name: 'Cards ecommerce',
          state: '/uikits/cards-ecommerce',
          type: 'link'
        },
        {
          icon: 'i-Duplicate-Window',
          name: 'Modals',
          state: '/uikits/modals',
          type: 'link'
        },
        {
          icon: 'i-Speach-Bubble-3',
          name: 'Popover',
          state: '/uikits/popover',
          type: 'link'
        },
        {
          icon: 'i-Like',
          name: 'Rating',
          state: '/uikits/rating',
          type: 'link'
        },
        {
          icon: 'i-Loading-3',
          name: 'Loaders',
          state: '/uikits/loaders',
          type: 'link'
        }
      ]
    },
    {
      name: 'Apps',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-Computer-Secure',
      sub: [
        {
          icon: 'i-Add-File',
          name: 'Invoice Builder',
          state: '/invoice',
          type: 'link'
        },
        { icon: 'i-Email', name: 'Inbox', state: '/inbox', type: 'link' },
        {
          icon: 'i-Speach-Bubble-3',
          name: 'Chat',
          state: '/chat',
          type: 'link'
        },
        {
          icon: 'i-Calendar',
          name: 'Calendar',
          state: '/calendar',
          type: 'link'
        }
      ]
    },
    {
      name: 'Systemadmin',
      type: 'dropdown',
      description: 'System administrator link',
      icon: 'i-home',
      sub: [
        {
          icon: 'i-home',
          name: 'Systemadmin',
          state: '/systemadmin/dashbaord',
          type: 'link'
        }
      ]
    },
    {
      name: 'Forms',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-File-Clipboard-File--Text',
      sub: [
        {
          icon: 'i-File-Clipboard-Text--Image',
          name: 'Basic components',
          state: '/forms/basic',
          type: 'link'
        },
        {
          icon: 'i-Split-Vertical',
          name: 'Form layouts',
          state: '/forms/layouts',
          type: 'link'
        },
        {
          icon: 'i-Receipt-4',
          name: 'Input Group',
          state: '/forms/input-group',
          type: 'link'
        },
        {
          icon: 'i-File-Edit',
          name: 'Input Mask',
          state: '/forms/input-mask',
          type: 'link'
        },
        {
          icon: 'i-Tag-2',
          name: 'Tag Input',
          state: '/forms/tag-input',
          type: 'link'
        },
        {
          icon: 'i-Width-Window',
          name: 'Wizard',
          state: '/forms/wizard',
          type: 'link'
        },
        {
          icon: 'i-Crop-2',
          name: 'Image Cropper',
          state: '/forms/img-cropper',
          type: 'link'
        }
      ]
    },
    {
      name: 'Data Tables',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-File-Horizontal-Text',
      sub: [
        {
          icon: 'i-File-Horizontal-Text',
          name: 'List',
          state: '/tables/list',
          type: 'link'
        },
        {
          icon: 'i-Full-View-Window',
          name: 'Fullscreen',
          state: '/tables/full',
          type: 'link'
        },
        {
          icon: 'i-Code-Window',
          name: 'Paging',
          state: '/tables/paging',
          type: 'link'
        },
        {
          icon: 'i-Filter-2',
          name: 'Filter',
          state: '/tables/filter',
          type: 'link'
        }
      ]
    },
    {
      name: 'Sessions',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-Administrator',
      sub: [
        {
          icon: 'i-Add-User',
          name: 'Sign up',
          state: '/sessions/signup',
          type: 'link'
        },
        {
          icon: 'i-Checked-User',
          name: 'Sign in',
          state: '/sessions/signin',
          type: 'link'
        },
        {
          icon: 'i-Find-User',
          name: 'Forgot',
          state: '/sessions/forgot',
          type: 'link'
        }
      ]
    },
    {
      name: 'Pages',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-Windows-2',
      sub: [
        {
          icon: 'i-Male',
          name: 'User Profile',
          state: '/pages/profile',
          type: 'link'
        }
      ]
    },

    {
      name: 'Icons',
      description: '600+ premium icons',
      type: 'link',
      icon: 'i-Cloud-Sun',
      state: '/icons/iconsmind'
    },
    {
      name: 'Others',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
      type: 'dropDown',
      icon: 'i-Double-Tap',
      sub: [
        {
          icon: 'i-Error-404-Window',
          name: 'Not found',
          state: '/others/404',
          type: 'link'
        }
      ]
    },
    {
      name: 'Doc',
      type: 'extLink',
      tooltip: 'Documentation',
      icon: 'i-Safe-Box1',
      state: 'http://demos.ui-lib.com/gull-doc'
    }
  ];

  adminMenu: IMenuItem[] = [
    {
      name: 'Dashboard',
      description: 'System Administrator, View metrics and details of Users',
      type: 'dropDown',
      icon: 'i-Bar-Chart',
      sub: [
        {
          icon: 'i-Clock-3',
          name: 'Dashboard',
          state: '/systemadmin/dashboard',
          type: 'link'
        }
      ]
    },
    {
      name: 'User Management',
      description: 'All users account management',
      type: 'dropDown',
      icon: 'i-MaleFemale',
      sub: [
        {
          icon: 'i-MaleFemale',
          name: 'Users List',
          state: '/systemadmin/users/lists',
          type: 'link'
        },
        {
          icon: 'i-Add-User',
          name: 'Add new HMO',
          state: '/systemadmin/users/create',
          type: 'link'
        },
        {
          icon: 'i-Add-User',
          name: 'Add new Hospital',
          state: '/systemadmin/users/hospital',
          type: 'link'
        }
      ]
    },
    {
      name: 'Health Care Providers',
      description: 'List of all health care providers list in Nigeria',
      icon: 'i-Building',
      type: 'dropDown',
      sub: [
        {
          icon: 'i-Building',
          name: 'Health Care Providers',
          state: '/systemadmin/health-care-list',
          type: 'link'
        }
      ],
    },
    {
      name: 'Application Settings',
      description: 'Application Settings',
      type: 'dropDown',
      icon: 'i-Gear',
      sub: [
        {
          icon: 'i-Receipt-3',
          name: 'Post Categories',
          state: '/systemadmin/appsetting/post/categories',
          type: 'link'
        },
        {
          icon: 'fa fa-gift',
          name: 'Plan Types',
          state: '/systemadmin/appsetting/plan',
          type: 'link'
        },
        {
          icon: 'i-Receipt-3',
          name: 'Email Settings',
          state: '/systemadmin/appsetting/email',
          type: 'link'
        },
        {
          icon: 'i-Building',
          name: 'Medication / Disease Setting',
          state: '/systemadmin/appsetting/medication',
          type: 'link'
        },
        {
          icon: 'fa fa-gears',
          name: 'Payment Configuration',
          state: '/systemadmin/appsetting/payment-config',
          type: 'link'
        }
      ]
    },
    {
      name: 'Settings',
      description: 'Account Setting',
      type: 'dropDown',
      icon: 'i-Gear',
      sub: [
        {
          icon: 'i-Building',
          name: 'Bank Account Setting',
          state: '/systemadmin/settings/bank',
          type: 'link'
        },
      ]
    },
    {
      name: 'Audit Trail',
      type: 'link',
      tooltip: 'Audit Trail',
      icon: 'i-Box-Full',
      state: '/systemadmin/log'
    },
    {
      name: 'FAQ',
      type: 'extLink',
      tooltip: 'Documentation',
      icon: 'i-Support',
      state: 'https://healthkonnet.com.ng/faq'
    }
  ];

  // I just added this.... This the sidebar menu for HMO
  hmoMenu: IMenuItem[] = [
    {
      name: 'Dashboard',
      description: 'Hmo Dashboard',
      type: 'dropDown',
      icon: 'i-Bar-Chart',
      sub: [
        {
          icon: 'i-Clock-3',
          name: 'Dashboard',
          state: '/hmo/dashboard',
          type: 'link'
        },
        {
          icon: 'i-Full-Screen',
          name: 'Change Provider',
          state: '/hmo/provider-requests',
          type: 'link'
        }
      ]
    },
    {
      name: 'Claims',
      description: 'Claims Management, Authorization Code Request management',
      type: 'dropDown',
      tooltip: 'Claims',
      icon: 'i-Box-Full',
      sub: [
        {
          icon: 'i-Folder',
          name: 'Claims',
          state: '/hmo/claims/list',
          type: 'link'
        },
        {
          icon: 'i-Add-File',
          name: 'Create Claim',
          state: '/hmo/claims/add-claim',
          type: 'link'
        },
        {
          icon: 'i-Financial',
          name: 'Request Authorization Code',
          state: '/hmo/claims/authorization-codes',
          type: 'link'
        },
      ]
    },
    {
      name: 'Users Management',
      description: 'Clients and  Enrollee Management',
      type: 'dropDown',
      icon: 'i-MaleFemale',
      sub: [
        {
          icon: 'i-Conference',
          name: 'Clients',
          state: '/hmo/clients',
          type: 'link'
        },
        {
          icon: 'fa fa-users',
          name: 'Enrollee',
          state: '/hmo/user/enrollee',
          type: 'link'
        }
        // { icon: 'i-MaleFemale', name: 'Enrollees', state: '/hmo/enrollees', type: 'link' },
      ]
    },
   /* {
      name: 'Treatments and Visits',
      description: 'Visits, Claims, Treatment Requests and Replies',
      type: 'dropDown',
      icon: 'i-Medicine',
      sub: [
        {
          icon: 'i-Ambulance',
          name: 'Visits & Claims',
          state: '/hmo/claims',
          type: 'link'
        },
        {
          icon: 'i-Medicine',
          name: 'Requests',
          state: '/hmo/treatment/request',
          type: 'link'
        },
        {
          icon: 'i-MaleFemale',
          name: 'Replies',
          state: '/hmo/treatment/reply/:id',
          type: 'link'
        }
      ]
    },*/
    {
      name: 'Configuration',
      description: 'Business Configuration',
      type: 'dropDown',
      icon: 'i-Gears',
      sub: [
        {
          icon: 'i-Calculator-2',
          name: 'Tariff',
          state: '/hmo/configuration/tariff-category',
          type: 'link'
        },
        {
          icon: 'i-Building',
          name: 'Select Hospitals',
          state: '/hmo/configuration/healthcare-selection',
          type: 'link'
        },
        {
          icon: 'i-Financial',
          name: 'Create Plan',
          state: '/hmo/configuration/create',
          type: 'link'
        },
        {
          icon: 'i-Pound',
          name: 'View Plan Detail',
          state: '/hmo/configuration/view',
          type: 'link'
        },
        {
          icon: 'i-Wallet',
          name: 'Bank Account Setting',
          state: '/hmo/settings/bank',
          type: 'link'

        },
      ]
    },
    {
      name: 'Reports',
      description: 'Report sections',
      type: 'dropDown',
      icon: 'i-Money-2',
      sub: [
        {
          icon: 'i-Coins',
          name: 'Payment Report',
          state: '/hmo/transactions/index',
          type: 'link'
        }
      ]
    },
    {
      name: 'Health Benefit',
      description: 'Create, Management health tips, plans and event/Programs',
      type: 'dropDown',
      icon: 'i-Medicine',
      sub: [
        {
          icon: 'i-Wallet',
          name: 'Creath Health Tips',
          state: '/hmo/benefits/tips/create',
          type: 'link'
        },
        {
          icon: 'i-Pound',
          name: 'View Health Tips',
          state: '/hmo/benefits/tips/list',
          type: 'link'
        },
        {
          icon: 'i-Ticket',
          name: 'Create Event ',
          state: '/hmo/media/event/create',
          type: 'link'
        },
        {
          icon: 'i-Ticket',
          name: 'View Events ',
          state: '/hmo/media/event/list',
          type: 'link'
        },
        {
          icon: 'i-Pound',
          name: 'Send Announcements',
          state: '/hmo/benefits/announcement',
          type: 'link'
        }
      ]
    },
    {
      name: 'Settings',
      description: 'Account Setting',
      type: 'dropDown',
      icon: 'i-Gear',
      sub: [
        {
          icon: 'i-MaleFemale ',
          name: 'Profile',
          state: '/hmo/settings/profile',
          type: 'link'
        }
      ]
    }
  ];
  clientMenu: IMenuItem[] = [
    {
      name: 'Dashboard',
      description: 'Client Dashboard, Manage Staffs and make transactions.',
      type: 'dropDown',
      icon: 'i-Bar-Chart',
      sub: [
        {
          icon: 'i-Clock-3',
          name: 'Dashboard',
          state: '/client/dashboard',
          type: 'link'
        }
      ]
    },
    {
      name: 'Staff',
      description: 'All staff account management',
      type: 'dropDown',
      icon: 'i-MaleFemale',
      sub: [
        {
          icon: 'i-MaleFemale',
          name: 'Staff List',
          state: '/client/staff/list',
          type: 'link'
        },
        {
          icon: 'i-Add-User',
          name: 'Add new Staff',
          state: '/client/staff/create',
          type: 'link'
        }
      ]
    },
    {
      name: 'Transaction',
      description: 'Transaction History',
      type: 'link',
      icon: 'i-Coins',
      state: '/client/transactions'
    },
    {
      name: 'FAQ',
      type: 'extLink',
      tooltip: 'Documentation',
      icon: 'i-Support',
      state: 'https://healthkonnet.com.ng/faq'
    }
  ];

  providerMenu: IMenuItem[] = [
    {
      name: 'Dashboard',
      description: 'Let\'s see all the happenings around',
      type: 'dropDown',
      icon: 'i-Bar-Chart',
      sub: [
        {
          icon: 'i-Clock-3',
          name: 'Dashboard',
          state: '/provider/dashboard',
          type: 'link'
        }
      ]
    },
    {
      name: 'Enrollees',
      description: 'All Enrollees Under My Hospital',
      type: 'dropDown',
      icon: 'i-MaleFemale',
      sub: [
        {
          icon: 'i-MaleFemale',
          name: 'All Enrollees',
          state: '/provider/enrollee/list',
          type: 'link'
        },
        {
          icon: 'i-Add-User',
          name: 'Verify Enrollee',
          state: '/provider/enrollee/verify',
          type: 'link'
        },
        {
          icon: 'i-Add-User',
          name: 'Add Claim',
          state: '/provider/enrollee/add-claim',
          type: 'link'
        }
      ]
    },
    {
      name: 'Claims',
      description: 'Processed and Batched claims',
      type: 'dropDown',
      tooltip: 'Claims',
      icon: 'i-Box-Full',
      sub: [
        {
          icon: 'i-Folder',
          name: 'Batched Claims',
          state: '/provider/claims/batched',
          type: 'link'
        },
        {
          icon: 'i-Repeat-3',
          name: 'Processed Claims',
          state: '/provider/claims/processed',
          type: 'link'
        },
        {
          icon: 'i-Financial',
          name: 'Request Authorization Code',
          state: '/provider/claims/request-authorization',
          type: 'link'
        },
      ]
    },
  ];

  enrolleeMenu: IMenuItem[] = [
    {
      name: 'Dashboard',
      description: 'Home ',
      type: 'dropDown',
      icon: 'i-Dashboard',
      sub: [
        {
          icon: 'i-Clock-3',
          name: 'Dashboard',
          state: '/enrollee/dashboard',
          type: 'link'
        },{
          icon: 'i-Clock-3',
          name: 'Change Provider',
          state: '/enrollee/changeProvider',
          type: 'link'
        }
      ]
    },
    {
      name: 'Health Tips',
      description: 'View Health Tips',
      type: 'dropDown',
      tooltip: 'Claims',
      icon: 'i-Box-Full',
      sub: [
        {
          icon: 'i-Folder',
          name: 'View Health Tips',
          state: '/enrollee/healthtips',
          type: 'link'
        },
      ]
    },
    {
      name: 'Packages',
      description: 'Your Package',
      type: 'dropDown',
      tooltip: 'Packages',
      icon: 'i-Box-Full',
      sub: [
        {
          icon: 'i-Gift-Box',
          name: 'Your Package',
          state: '/enrollee/package',
          type: 'link'
        },
      ]
    },
    {
      name: 'Profile',
      description: 'Your Profile',
      type: 'dropDown',
      icon: 'i-MaleFemale',
      sub: [
        {
          icon: 'i-Gift-Box',
          name: 'Your Profile',
          state: '/enrollee/profile',
          type: 'link'
        },
      ]
    },
    {
      name: 'Events',
      description: 'Events',
      type: 'dropDown',
      icon: 'i-Ticket',
      sub: [
        {
          icon: 'i-Ticket',
          name: 'Events',
          state: '/enrollee/events',
          type: 'link'
        },
      ]
    }
  ];
  // sets iconMenu as default;
  menuItems = new BehaviorSubject<IMenuItem[]>(this.defaultMenu);
  // navigation component has subscribed to this Observable
  menuItems$ = this.menuItems.asObservable();

  // You can customize this method to supply different menu for
  // different user type.

  // Can you see this?  Do you Understand what's happening here??
  //  Yes
  // Define the menu and add it to switch case here according to the role
  // This is where the sidebar menu pick the menu is going to use based on the role of user that login to the sys
  // Oktem
  // Let's move to where i'm using it, I mean where you can be switching it Pending the time the vm will be available
  publishNavigationChange(menuType: string) {
    switch (menuType) {
      case sroles.Superadmin:
        this.menuItems.next(this.adminMenu);
        break;
      case sroles.HMO:
        this.menuItems.next(this.hmoMenu);
        break;
        case sroles.Organization:
          this.menuItems.next(this.clientMenu);
          break;
          case sroles.Provider:
            this.menuItems.next(this.providerMenu);
            break;
            case sroles.Enrollee:
            this.menuItems.next(this.enrolleeMenu);
            break;
      default:
        this.menuItems.next(this.defaultMenu);
    }
  }
}


/* {
  icon: 'i-Wallet',
  name: 'Withdraw',
  state: '/hmo/transfer/withdraw/',
  type: 'link'
},
{
  icon: 'i-Pound',
  name: 'Pay Health Care Provider',
  state: '/hmo/transfer/healthcare-provider',
  type: 'link'
},
{
  icon: 'i-Wallet',
  name: 'Payment',
  state: '/hmo/transfer/payment/',
  type: 'link'
}, */