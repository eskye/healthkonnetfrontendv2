import {Injectable, Injector} from '@angular/core';
import {IRootService} from '../common/interface/IRootService';
import {RootService} from '../common/interface/RootService';
import {HttpClient} from '@angular/common/http';
import {routes} from '../constant';
import {map} from 'rxjs/internal/operators';
import { Observable } from 'rxjs/internal/Observable';
import { IAccountVerificationModel } from '../common/model/IPayment';
export interface  IPaymentService extends IRootService<any> {
  PaymentDetail(id): Observable<any>;
  VerifyAccountNumber(data: IAccountVerificationModel): Observable<any>;
  GetBankSettings(): Observable<any>;
  ListOfBanks(): Observable<any>;
  CreateBankDetails(item): Observable<any>;
  fundWallet(item: any): Observable<any>;
  savePaymentConfig(item): Observable<any>;
  GetPaymentConfig(): Observable<any>;
}


@Injectable({
  providedIn: 'root'
})
export class PaymentService extends RootService<any> implements IPaymentService {

  constructor( httpClient: HttpClient,
               injector: Injector) {
    super(httpClient, injector);
  }
  PaymentDetail(id): Observable<any> {
    return this.get(id, routes.PAYMENT.DETAIL).pipe(map(res => {
      return res as any;
    }));
  }

  VerifyAccountNumber(item: IAccountVerificationModel): Observable<any> {
    return this.httpClient.get(`${routes.OTHERS.RESOLVEACCOUNT}?accountNumber=${item.accountNumber}&code=${item.bankCode}`)
          .pipe(map(res => {
            return res;
      }));
  }

  GetBankSettings( ): Observable<any> {
    return this.getlist(routes.OTHERS.DETAIL).pipe(map(res => {
      return res;
    }));
  }

  ListOfBanks(): Observable<any> {
    return this.getlist(routes.OTHERS.LISTOFBANKS).pipe(map(res => {
      return res;
    }));
  }

  CreateBankDetails(item): Observable<any> {
    return this.post(routes.OTHERS.SAVE, item).pipe(map(res => {
      return res;
    }));
 }
 fundWallet(item: any): Observable<any> {
  return this.post(routes.PAYMENT.FUND, item).pipe(map(res => {
    return res;
  }));
 }

 savePaymentConfig(item): Observable<any> {
  return this.post(routes.OTHERS.SAVEPAYMENTCONFIG, item).pipe(map(res => {
    return res;
  }));
}
GetPaymentConfig(): Observable<any> {
  return this.getlist(routes.OTHERS.PAYMENTCONFIGDETAIL).pipe(map(res => {
    return res;
  }));
}
}
