import { Injectable, Injector } from '@angular/core';
import { IRootService } from '../common';
import { RootService } from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { INameAndId } from '../common/model/INameAndId';

export interface IClientService extends IRootService<any> {
  uploadBulkList(formData: FormData): Observable<any>;
  getDetailByKey(key): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class ClientService extends RootService<any> implements IClientService {

  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

   uploadBulkList(formData: FormData): Observable<any> {
    return this.httpClient.post<any>(routes.STAFF.UPLOAD, formData, {
      reportProgress: true, observe: 'events'
    }).pipe(map(res => {
        return res as any;
      }), catchError(ErrorHandler.ErrorServerConnection));
   }

   getDetailByKey(key): Observable<any>{
    return this.get(key, routes.ORGANIZATION.GETDETAILBYKEY).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
   }
 
}
