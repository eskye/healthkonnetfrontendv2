import { Injectable, Injector } from '@angular/core';
import { RootService } from '../common/interface/RootService';
import { IRootService } from '../common/interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { routes } from '../constant';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/internal/operators/catchError';
import { ErrorHandler } from '../common/ErrorHandler';
import { IPackage } from '../common/model/ISetting';
import { INameAndId } from '../common/model/INameAndId';

export interface IHmoService extends IRootService<any> {
  createHmo(body: any): Observable<any>;
  getPackages(): Observable<IPackage[]>;
  createPackage(packs: IPackage): Observable<IPackage>;
  updatePackage(packs: IPackage): Observable<IPackage>;
  detailPackage(id): Observable<IPackage>;
  getPackagesNameAndId(): Observable<INameAndId[]>;
  getHmo(): Observable<INameAndId[]>;
  getDetails(id): Observable<any>;
}
@Injectable({
  providedIn: 'root'
})
export class HmoService extends RootService<any> implements IHmoService  {
  
  constructor(httpClient: HttpClient, private injector: Injector) {
    super(httpClient, injector);
  }

  createHmo(body: any): Observable<any> {
    return this.post(routes.SETUPHMO, body).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getPackages(): Observable<IPackage[]> {
    return this.getlist(routes.PACKAGES).pipe(map(packages => {
      return packages as any;
    }));
  }
  getPackagesNameAndId(): Observable<INameAndId[]> {
    return this.getlist(routes.PACKAGENAMEANDID).pipe(map(packages => {
      return packages.data as any;
    }));
  }
  createPackage(packs: IPackage): Observable<IPackage> {
    return this.post(routes.PACKAGES, packs).pipe(map(pack => {
      return pack as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  updatePackage(packs): Observable<IPackage> {
    return this.update(packs, routes.PACKAGES).pipe(map(pack => {
      return pack as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  detailPackage(id: any): Observable<IPackage> {
    return this.get(id, routes.PACKAGES).pipe(map(packag => {
      return packag as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  getHmo(): Observable<INameAndId[]> {
    return this.getlist(routes.HMO.LIST).pipe(map(hmos => {
      return hmos.data as any;
    }));
  }
  searchPackages(search): Observable<INameAndId[]> {
    return this.getquery(`${routes.SEARCHPACKAGE}`, 'hmoId' , search).pipe(map(res => {
      return res.data as any;
    }));
   }

   getDetails(id): Observable<any> {
    return this.get(id, routes.HMO.GETHMODETAILS).pipe(
      map(data => {
        return data.data as any;
      })
    );
  }

  
}
