import { Injectable, Injector } from '@angular/core';
import {IRootService} from '../common/interface/IRootService';
import {RootService} from '../common/interface/RootService';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { routes } from '../constant';
import { catchError } from 'rxjs/operators';
import { ErrorHandler } from '../common/ErrorHandler';

export interface IEventService extends IRootService<any> {
  create(data): Observable<any>;
  edit(data): Observable<any>;
  details(id): Observable<any>;
  remove(data: any): Observable<any>;
}

@Injectable({
  providedIn: 'root'
})
export class EventService extends RootService<any> implements IEventService {
  constructor(httpClient: HttpClient, injector: Injector) {
    super(httpClient, injector);
  }

  create(data): Observable<any> {
    return this.post(`${routes.EVENT.MAIN}`, data).pipe(map(res => {
      return res as any;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }

  edit(data: any): Observable<any> {
    return this.update(data, `${routes.EVENT.MAIN}`).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  remove(data: any): Observable<any> {
    return this.delete(data, `${routes.EVENT.MAIN}`).pipe(map(res => {
      return res;
    }), catchError(ErrorHandler.ErrorServerConnection));
  }
  details(id): Observable<any> {
    return this.get(id, routes.EVENT.MAIN).pipe(map(res => {
      return res as any;
    }));
  }

}
