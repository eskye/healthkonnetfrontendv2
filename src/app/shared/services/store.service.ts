import { Injectable, Injector } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/views/sessions/state/user.reducers';
import { isNullOrUndefined } from 'util';
import { IProfile } from '../common/model/IProfile';
import { Utils } from '../utils';
import * as userActions from 'src/app/views/sessions/state/user.actions';

@Injectable({
    providedIn: 'root'
  })

  export class StoreService{
    userData: IProfile; 
      constructor(private store : Store<AppState>) { 
      } 
      public get user(){
        const user$ = this.store.select(state => state.user);
        user$.subscribe(res => { 
          if(!isNullOrUndefined(res)){
            this.userData = (<IProfile>res.payLoad); 
          }
        });
        return this.userData;
      }

      protected get firstName(){
        return this.user.firstName
      }
      public get greet(){
          if(isNullOrUndefined(this.firstName)){
              return '';
          }else{
            return `Hi ${this.firstName},  ${Utils.greet()}`; 
          }
       
      }

      public dispatch(){
        this.store.dispatch(new userActions.RemoveUserData())
      }
  }
