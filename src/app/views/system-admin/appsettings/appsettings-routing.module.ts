import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppsettingsComponent } from './appsettings.component';
import { BlogcategoryComponent } from './blogcategory/blogcategory.component';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { ListComponent } from './email-template/list/list.component';
import { CreateComponent } from './email-template/create/create.component';
import { DetailComponent } from './email-template/detail/detail.component';
import { PlanSetupComponent } from './plan-setup/plan-setup.component';
import { MedicationDiseaseComponent } from './medication-disease/medication-disease.component';
import { PaymentConfigComponent } from './payment-config/payment-config.component';

const routes: Routes = [
  {
    path: '',
    component: AppsettingsComponent,
    children: [
      {
        path: 'post/categories',
        component: BlogcategoryComponent
      },
      {
        path: 'plan',
        component: PlanSetupComponent
      },
      {
        path: 'medication',
        component: MedicationDiseaseComponent
      },
      {
        path: 'email',
        component: EmailTemplateComponent,
        children: [
          {
            path: '',
            component: ListComponent
          },
          {
            path: 'edit/:id',
            component: CreateComponent
          },
          {
            path: 'detail/:id',
            component: DetailComponent
          }
        ]
      },
      {
        path: 'payment-config',
        component: PaymentConfigComponent
      },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppsettingsRoutingModule { }
