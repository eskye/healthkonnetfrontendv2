import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { notifications } from 'src/app/shared/constant';
import { isNullOrUndefined } from 'util';
import { NgForm } from '@angular/forms';
import { PlanService } from 'src/app/shared/services/plan.service';

@Component({
  selector: 'app-plan-setup',
  templateUrl: './plan-setup.component.html',
  styleUrls: ['./plan-setup.component.scss']
})
export class PlanSetupComponent extends BaseComponent implements OnInit {
  plan: any = {};
  edit: boolean;
  private modalRef: NgbModalRef;
  constructor(private modalService: NgbModal, private planService: PlanService) {
    super(null, null, null, null, planService);
  }

  ngOnInit() {
    this.init();
  }
  init() {
    this.planService.get().subscribe(res =>{
      this.items = res;
    }, error => {
      console.log(error);
    });
  }

  open(content, item = null) {
    this.edit = false;
    if (!isNullOrUndefined(item)) { this.edit = true; this.item = item; } else { this.item =  {}; }
  this.modalRef  = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'});
  this.modalRef.result.then((result) => {
   // console.log(result);
  }, (reason) => {
   // console.log('Err!', reason);
  });
}
// addSub(){
//   this.itemSub.push({title: 'new'})
// }
createPlan(f: NgForm) {
  if (f.valid) {
    this.waiting = true;
    const value = f.value;
    this.planService.create(value).subscribe(res => {
      this.planService.successAlert('Plan Created successfully', notifications.success);
      this.modalRef.close();
      f.reset();
      this.waiting = false;
      this.reloadComponent();
    }, error => {
      this.planService.errorAlert(error, notifications.error);
      this.waiting = false;
    });
  }
 }

 updatePlan(f: NgForm) {
  if (f.valid) {
    this.waiting = true;
    this.planService.edit(this.item).subscribe((res) => {
      this.waiting = false;
      this.modalRef.close();
      this.planService.successAlert('Plan updated successfully', notifications.success);
    }, error => {
      this.planService.errorAlert(error, notifications.success);
      this.waiting = false;
    });
  }
}

remove(item, modal) {
       this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', centered: true })
            .result.then((result) => {
              this.waiting = true;
              this.planService.remove(item).subscribe(res => {
                this.waiting = false;
                this.reloadComponent();
                this.modalRef.close();
                this.planService.successAlert('Plan successfully', notifications.success);
              });
            }, (reason) => {
            });
}


}
