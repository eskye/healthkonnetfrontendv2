import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicationDiseaseComponent } from './medication-disease.component';

describe('MedicationDiseaseComponent', () => {
  let component: MedicationDiseaseComponent;
  let fixture: ComponentFixture<MedicationDiseaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationDiseaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationDiseaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
