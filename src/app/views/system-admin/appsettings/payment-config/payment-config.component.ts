import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { PaymentService } from 'src/app/shared/services/payment.service';
import { notifications } from 'src/app/shared/constant';

@Component({
  selector: 'app-payment-config',
  templateUrl: './payment-config.component.html',
  styleUrls: ['./payment-config.component.scss']
})
export class PaymentConfigComponent extends BaseComponent implements OnInit {

  isSaving = false;
  constructor(private paymentService: PaymentService) {
    super(null, null, null, null, paymentService);
  }

  ngOnInit() {
  }

  saveConfig(event) {
    this.isSaving = true;
   this.paymentService.savePaymentConfig(event).subscribe(res => {
    this.isSaving = false;
    this.paymentService.successAlert('Payment Gateway Configuration Setup Successfully', notifications.success);
   }, error => {
    this.isSaving = false;
     this.paymentService.errorAlert('An error occured while updating your bank account details', notifications.error);
   });
  }
}
