import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppsettingsRoutingModule } from './appsettings-routing.module';
import { BlogcategoryComponent } from './blogcategory/blogcategory.component';
import { AppsettingsComponent } from './appsettings.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { CreateComponent } from './email-template/create/create.component';
import { ListComponent } from './email-template/list/list.component';
import { DetailComponent } from './email-template/detail/detail.component';
import { TinymceModule } from 'angular2-tinymce';
import { PlanSetupComponent } from './plan-setup/plan-setup.component';
import { MedicationDiseaseComponent } from './medication-disease/medication-disease.component';
import { PaymentConfigComponent } from './payment-config/payment-config.component';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    TinymceModule.withConfig({}),
    ReactiveFormsModule,
    NgbModule,
    SharedPipesModule,
    AppsettingsRoutingModule
  ],
  declarations: [
    BlogcategoryComponent,
    AppsettingsComponent,
    EmailTemplateComponent,
    CreateComponent,
    ListComponent,
    DetailComponent,
    PlanSetupComponent,
    MedicationDiseaseComponent,
    PaymentConfigComponent
  ]
})
export class AppsettingsModule { }
