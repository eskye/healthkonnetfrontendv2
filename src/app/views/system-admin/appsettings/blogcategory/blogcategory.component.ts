import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { PostCategoryService } from 'src/app/shared/services/post-category.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { routes, notifications } from 'src/app/shared/constant';
import { isNullOrUndefined } from 'util';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-blogcategory',
  templateUrl: './blogcategory.component.html',
  styleUrls: ['./blogcategory.component.scss']
})
export class BlogcategoryComponent extends BaseComponent implements OnInit {

  cat: any = {};
  edit: boolean;
  private modalRef: NgbModalRef;
  constructor(private modalService: NgbModal, private postCategoryService: PostCategoryService) {
    super(null, null, null, null, postCategoryService);
  }

  ngOnInit() {
    this.init();
  }
  init() {
    this.url = `${routes.POST.CATEGORY}/count`;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPagination();
  }

  open(content, item = null) {
    this.edit = false;
    if (!isNullOrUndefined(item)) { this.edit = true; this.item = item; } else { this.item =  {}; }
  this.modalRef  = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'});
  this.modalRef.result.then((result) => {
   // console.log(result);
  }, (reason) => {
   // console.log('Err!', reason);
  });
}

createCategory(f: NgForm) {
  if (f.valid) {
    this.waiting = true;
    const value = f.value;
    this.postCategoryService.create(value).subscribe(res => {
      this.postCategoryService.successAlert('Category Created successfully', notifications.success);
      this.modalRef.close();
      f.reset();
      this.waiting = false;
      this.reloadComponent();
    }, error => {
      this.postCategoryService.errorAlert(error, notifications.error);
      this.waiting = false;
    });
  }
 }

 updateCategory(f: NgForm) {
  if (f.valid) {
    this.waiting = true;
    this.postCategoryService.edit(this.item).subscribe((res) => {
      this.waiting = false;
      this.modalRef.close();
      this.postCategoryService.successAlert('Post category updated successfully', notifications.success);
    }, error => {
      this.postCategoryService.errorAlert(error, notifications.success);
      this.waiting = false;
    });
  }
}

remove(item, modal) {
       this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', centered: true })
            .result.then((result) => {
              this.waiting = true;
              this.postCategoryService.remove(item).subscribe(res => {
                this.waiting = false;
                this.reloadComponent();
                this.modalRef.close();
                this.postCategoryService.successAlert('Category deleted successfully', notifications.success);
              });
              // this.init();
            }, (reason) => {
            });
}


}
