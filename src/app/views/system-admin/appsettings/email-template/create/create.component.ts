import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { EmailTemplateService } from 'src/app/shared/services/emailtemplate.service';
import { notifications } from 'src/app/shared/constant';
import { NgForm } from '@angular/forms';
declare var tinymce: any;
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  id: any;
  item: any = {};
 loading: boolean;
 placeholders: any;
  emailTypes: any;
  constructor(private activatedRoute: ActivatedRoute, private emailTemplateService: EmailTemplateService) {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
  }

 ngOnInit() {
   this.getEmailTemplate();
   this.getEmailTemplateTypes();
 }

 getEmailTemplateTypes() {
  this.emailTemplateService.getEmailTypes().subscribe(res => {
    this.emailTypes = res;
  }, error => {
    console.log(error);
  });
 }
  getEmailTemplate() {
    this.loading = true;
    this.emailTemplateService.detail(this.id).subscribe(res => {
      this.item = res.data;
      this.getEmailTemplatePlaceholders();
      this.loading = false;
    }, error => {
      this.loading = false;
      this.emailTemplateService.errorAlert(error, notifications.error);
    });
  }

  getEmailTemplatePlaceholders() {
    this.loading = true;
    this.emailTemplateService.getPlaceholders(this.item.emailType).subscribe(res => {
      this.placeholders = res.placeholders;
      this.loading = false;
    }, error => {
      this.loading = false;
      this.emailTemplateService.errorAlert(error, notifications.error);
    });
  }

  updateTemplate(f: NgForm) {
    if (f.valid) {
      this.loading = true;
     // console.log(packs);
      this.emailTemplateService.updateTemplate(this.item).subscribe((res) => {
       this.loading = false;
        this.emailTemplateService.successAlert('Email Template updated successfully', notifications.success);
      }, error => {
        this.loading = false;
        this.emailTemplateService.errorAlert(error, notifications.error);
      });
    }
  }
}
