import { Component, OnInit } from '@angular/core';
import { IPackage } from 'src/app/shared/common/model/ISetting';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { notifications } from 'src/app/shared/constant';
import { EmailTemplateService } from 'src/app/shared/services/emailtemplate.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  id: any;
   item: any;
  loading: boolean;
   constructor(private activatedRoute: ActivatedRoute, private emailTemplateService: EmailTemplateService) { 
     this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
       this.id = params.get('id');
     });
   }
  ngOnInit() {
    this.getEmailTemplate();
  }

  getEmailTemplate() {
    this.loading = true;
    this.emailTemplateService.detail(this.id).subscribe(res => {
      this.item = res.data;
      this.loading = false;
    }, error => {
      this.loading = false;
      this.emailTemplateService.errorAlert(error, notifications.error);
    });
  }

}
