import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostCategoryService } from 'src/app/shared/services/post-category.service';
import { routes } from 'src/app/shared/constant';
import { EmailTemplateService } from 'src/app/shared/services/emailtemplate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {
  emailTypes: any;

  constructor(private modalService: NgbModal, private emailTemplateService: EmailTemplateService, router: Router) {
    super(null, router, null, null, emailTemplateService);
  }

  ngOnInit() {
    this.init();
    this.getEmailTemplateTypes();
  }

  init() {
    this.url = `${routes.EMAILTEMPLATE.COUNT}`;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

  getEmailTemplateTypes() {
   this.emailTemplateService.getEmailTypes().subscribe(res => {
     this.emailTypes = res;
   }, error => {
     console.log(error);
   });
  }

  emailTypeChanged(id) {
    this.filter.emailType = id;
    this.setupPaginations();
  }
}
