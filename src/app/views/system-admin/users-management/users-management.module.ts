import { environment } from './../../../../environments/environment.prod';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersManagementRoutingModule } from './users-management-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersCreateComponent } from './users-create/users-create.component';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { CreatehealthcareComponent } from './createhealthcare/createhealthcare.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgmCoreModule } from '@agm/core';
import { MapService } from 'src/app/shared/services/map.service';
import { UsersLogComponent } from './users-log/users-log.component';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxDatatableModule,
    NgbModule,
    AgmCoreModule.forRoot({
      // To be added to environment variable when about to deploy
      apiKey: environment.Apikey,
      libraries: ['places']
    }),
    SharedPipesModule,
    UsersManagementRoutingModule
  ],
  declarations: [UsersListComponent,
     UsersCreateComponent,
     UsersDetailComponent,
     CreatehealthcareComponent,
     UsersLogComponent
    ],
  providers: [
    MapService
  ]
})
export class UsersManagementModule { }
