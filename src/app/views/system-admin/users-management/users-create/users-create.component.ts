import { Utils } from 'src/app/shared/utils';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { NgForm } from '@angular/forms';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { notifications } from 'src/app/shared/constant';


@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.scss']
})
export class UsersCreateComponent extends BaseComponent implements OnInit {
  fileExtensionError: boolean;
  isImage: boolean;
  fileExtensionMessage: any;
  constructor(private hmoService: HmoService, private fileManagerService: FileManagerService,
    private modalService: NgbModal) {
    super(null, null, null, null, hmoService);
  }

  ngOnInit() {
    this.getCountry();
  }

  async onFileChange(event) {
    const file = event.target.files[0];
    if (Utils.fileValidator(file, true)) {
        this.isImage = true;
      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        // reader.onload = () => {
        //   this.profilePic = reader.result;
        // };
      }
      if (event.target.files.length > 0) {
        const formData = new FormData();
        formData.append('File', file);
        // tslint:disable-next-line:no-shadowed-variable
        this.fileManagerService.uploadProfileImage(formData).subscribe((event: HttpEvent<any>) => {
          // console.log(event);
          switch (event.type) {
            case HttpEventType.Sent:
              break;
            case HttpEventType.Response:
            this.item.hmoUrl = (<any>event).body.data.result;
            this.waiting = false;
            // this.fileManagerService.successAlert((<any>event).body.data.filename);
            // this.msg = (<any>event).body.data.error;
            break;
            case 1: {
              this.waiting = true;
              // if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
              //   this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);

              }
              break;
          }
        }, error => {
          this.waiting = false;
          this.fileManagerService.
          errorAlert('An error occurred while uploading logo to the server, try again', 'Error Uploading File To Server');
        });
      }
    } else {
      this.fileExtensionMessage = 'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
    }

  }

  submit(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      let phoneNumberRule = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      // if(f.value.hmoPhone.match(phoneNumberRule)){
      //   this.hmoService.successAlert('Works', notifications.success);
        // this.hmoService.createHmo(f.value).subscribe(res => {
        //   this.waiting = false;
        //    this.hmoService.successAlert('Account Created Successfully', notifications.success);
        //    f.reset();
        // }, error => {
        //   this.waiting = false;
        //   this.hmoService.errorAlert(error, notifications.error);
        // });
      // } else {
      //   this.hmoService.errorAlert('error', notifications.error);
      // }      
        this.hmoService.createHmo(f.value).subscribe(res => {
          this.waiting = false;
           this.hmoService.successAlert('Account Created Successfully', notifications.success);
           f.reset();
        }, error => {
          this.waiting = false;
          this.hmoService.errorAlert(error, notifications.error);
        });
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
    .result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log('Err!', reason);
    });
  }
}
