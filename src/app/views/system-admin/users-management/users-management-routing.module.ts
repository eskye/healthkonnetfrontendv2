import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersCreateComponent } from './users-create/users-create.component';
import { CreatehealthcareComponent } from './createhealthcare/createhealthcare.component';
import { UsersLogComponent } from './users-log/users-log.component';

const routes: Routes = [
  {
    path: 'lists',
    component: UsersListComponent
  },
  {
    path: 'log',
    component: UsersLogComponent

  },
  {
    path: 'create',
    component: UsersCreateComponent

  },
  {
    path: 'hospital',
    component: CreatehealthcareComponent

  },
  /* {
    path: 'enrollees',
    component: EnrolleesComponent

  } */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersManagementRoutingModule { }
