import { Router } from '@angular/router';
import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Utils } from 'src/app/shared/utils';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, map } from 'rxjs/operators';
import { HealthcareService } from 'src/app/shared/services/healthcare.service';
import { MapService } from 'src/app/shared/services/map.service';
import { EmitService } from 'src/app/shared/services/emit.service';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { notifications } from 'src/app/shared/constant';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-createhealthcare',
  templateUrl: './createhealthcare.component.html',
  styleUrls: ['./createhealthcare.component.scss']
})
export class CreatehealthcareComponent extends BaseComponent implements OnInit, AfterViewInit {

  fileExtensionError: boolean;
  isImage: boolean;
  fileExtensionMessage: any;
  healthcares$: Observable<any[]>;
  people3Loading = false;
  textinput$ = new Subject<string>();
  @ViewChild('mapSearch', {read: ElementRef}) private mapSearch: ElementRef ;
  locationResponse: any;
  selectedItem: any;
  filename: any;
  mapAddressError: string;
  constructor(router: Router, private providerService: ProviderService, 
    private healthcareService: HealthcareService,
    private fileManagerService: FileManagerService,
    private mapService: MapService,
    private emitService: EmitService,
    private modalService: NgbModal) {
    super(null, router, null, null, providerService);
  }

  ngOnInit() {
    this.getCountry();
    this.loadHealthCare();
  }

  ngAfterViewInit() {
    this.locationResponse = this.mapService.autocompleteLocation(this.mapSearch);
    this.emitService.mapResponse$.subscribe(data => {
      this.locationResponse = data;
    });
  }

  async onFileChange(event) {
    const file = event.target.files[0]; 
    this.filename = file.name;
    if (Utils.fileValidator(file, true)) {
        this.isImage = true;
      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        // reader.onload = () => {
        //   this.profilePic = reader.result;
        // };
      }
      if (event.target.files.length > 0) {
        const formData = new FormData();
        formData.append('File', file);
        // tslint:disable-next-line:no-shadowed-variable
        this.fileManagerService.uploadProfileImage(formData).subscribe((event: HttpEvent<any>) => {
          // console.log(event);
          switch (event.type) {
            case HttpEventType.Sent:
              break;
            case HttpEventType.Response:
            this.item.providerUrl = (<any>event).body.data.result;
            this.waiting = false;
            // this.fileManagerService.successAlert((<any>event).body.data.filename);
            // this.msg = (<any>event).body.data.error;
            break;
            case 1: {
              this.waiting = true;
              // if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
              //   this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);

              }
              break;
          }
        }, error => {
          this.waiting = false;
          this.fileManagerService.
          errorAlert('An error occurred while uploading logo to the server, try again', 'Error Uploading File To Server');
        });
      }
    } else {
      this.fileExtensionMessage = 'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
    }

  }

  submit(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      if (isNullOrUndefined(this.locationResponse)) {
           this.mapAddressError = 'The nearest map address field is required, search and select the nearest map address to your hospital';
          return false;
      } else {
        f.value.latitude = this.locationResponse.latitude;
        f.value.longitude = this.locationResponse.longitude;
        f.value.providerAddress = this.locationResponse.location;
        this.providerService.createProvider(f.value).subscribe(res => {
          this.waiting = false;
          this.providerService.successAlert('Account Created Successfully', notifications.success);
          this.goToNav('/systemadmin/users/lists');
        }, error => {
          this.waiting = false;
          this.providerService.errorAlert(error, notifications.error);
        });
      }
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
    .result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log('Err!', reason);
    });
  }

  onSelect(item: any) {
    alert(item);
    this.selectedItem = item;
  }

  private loadHealthCare() {
    this.healthcares$ = concat(
      of([]), // default items
      this.textinput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.people3Loading = true)),
        switchMap(term => term.length < 3 ? of([]) :
          this.healthcareService.searchHealthCare(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.people3Loading = false))
          )
        )
      )
    );
  }
}

