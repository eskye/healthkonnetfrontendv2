import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { routes } from 'src/app/shared/constant';
import { HmoService } from 'src/app/shared/services/hmo.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-log.component.html',
  styleUrls: ['./users-log.component.scss']
})
export class UsersLogComponent extends BaseComponent implements OnInit {

  constructor(private hmoService: HmoService) {
    super(null, null, null, null, hmoService);
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.url = routes.USERLIST;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPagination();
  }
}
