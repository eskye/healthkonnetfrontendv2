import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwlModule } from 'ngx-owl-carousel';

import { SystemAdminRoutingModule } from './system-admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HealthCareComponent } from './health-care/health-care.component';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';

import { BankComponent } from './settings/bank/bank.component';
import { SetupPlanComponent } from './settings/setup-plan/setup-plan.component';
import { MedicalDiseaseSettingComponent } from './settings/medical-disease-setting/medical-disease-setting.component';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedComponentsModule,
    NgxEchartsModule,
    SharedPipesModule,
    NgxDatatableModule,
    NgbModule,
    SystemAdminRoutingModule,
    OwlModule
  ],
  declarations: [DashboardComponent, HealthCareComponent, BankComponent, SetupPlanComponent, MedicalDiseaseSettingComponent, AuditTrailComponent]
})
export class SystemAdminModule { }
