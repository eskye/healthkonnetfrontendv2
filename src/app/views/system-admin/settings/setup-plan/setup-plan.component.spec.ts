import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPlanComponent } from './setup-plan.component';

describe('SetupPlanComponent', () => {
  let component: SetupPlanComponent;
  let fixture: ComponentFixture<SetupPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
