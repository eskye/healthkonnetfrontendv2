import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalDiseaseSettingComponent } from './medical-disease-setting.component';

describe('MedicalDiseaseSettingComponent', () => {
  let component: MedicalDiseaseSettingComponent;
  let fixture: ComponentFixture<MedicalDiseaseSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalDiseaseSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalDiseaseSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
