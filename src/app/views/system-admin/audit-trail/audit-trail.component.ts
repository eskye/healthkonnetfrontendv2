import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { AuditTrailService } from 'src/app/shared/services/audit-trail.service';
import { routes } from 'src/app/shared/constant';

@Component({
  selector: 'app-audit-trail',
  templateUrl: './audit-trail.component.html',
  styleUrls: ['./audit-trail.component.scss']
})
export class AuditTrailComponent extends BaseComponent implements OnInit {
  actions: any;
  sections: any;

  constructor(private auditTrailService: AuditTrailService) {
    super(null, null, null, null, auditTrailService);
  }

  ngOnInit() {
    this.filter.browserName = '';
    this.filter.deviceOs = '';
    this.filter.auditActionId = 0;
    this.init();
    this.getAuditActions();
  }

  init() {
    this.url = routes.AUDITTRAIL.COUNT;
    this.query = {
      count: this.paginationConfig.count = 100,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

  getAuditActions() {
    this.auditTrailService.getActions().subscribe(res => {
      this.actions = res[0];
      // this.sections = res[1];
    });
  }
}
