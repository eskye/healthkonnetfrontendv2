import { EmitService } from './../../../shared/services/emit.service';
import { Component, OnInit } from '@angular/core';
import { EChartOption } from 'echarts';
import { echartStyles } from '../../../shared/echart-styles';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Utils } from 'src/app/shared/utils';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { routes } from 'src/app/shared/constant';
import { ICounter } from 'src/app/shared/common/model/ICounter';
import { ChartService } from 'src/app/shared/services/chart.service';
import { Router } from '@angular/router';
import { IProfile } from 'src/app/shared/common/model/IProfile';
import { isNullOrUndefined } from 'util';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
  })
  export class DashboardComponent extends BaseComponent implements OnInit {
   slideOptions = { items: 2, loop: true, dots: true, navigation: true, autoplay: true };
  chartLineOption1: EChartOption;
  chartLineOption2: EChartOption;
  chartLineOption3: EChartOption;
  chartLineOption4: EChartOption;
  chartPie1: any;
  chartPie2: any;
  chartBar1: any;
  salesChartBar: EChartOption;
  salesChartPie: EChartOption;
  doughnutChartLabels: string[] = [];
  doughnutChartData: number[] = [];
  doughnutChartType = 'pie';
  lineChartData: Array<any> = [];
  lineChartLabels: Array<any> = [];
 
  greetingimage: string;
  time: string;
  counter: ICounter;
  userData: IProfile;
  constructor(private modalService: NgbModal,
    private chartService: ChartService, private store: StoreService,
     private dashboardSerivce: DashboardService, router: Router) {
    super(null, router, null, null, chartService);
     
  }

  ngOnInit() {
    this.filter.days = 7;
    this.displayClock();
    this.getDashboardData();
   // this.getChartEnrolleeVisitData(query);
    // this.getEnrolleePackageData();
    // this.chart4();
  }

  private getDashboardData() {
    this.waiting = true;
    this.dashboardSerivce.getDashboard(routes.DASHBOARD.SUPERADMINDASHBOARD).subscribe(res =>{
     this.counter = res;
    }, error => {
     this.waiting = false;
     this.dashboardSerivce.errorAlert(error, 'Error Alert');
    });
  }

  get greeting(){ 
    return this.store.greet;
   }

  private displayClock() {
    // tslint:disable-next-line: max-line-length
    Utils.greet() === 'Good morning' ? this.greetingimage = 'morning' : Utils.greet() === 'Good afternoon' ? this.greetingimage = 'afternoon' : Utils.greet() === 'Good evening' ? this.greetingimage = 'evening' : null;
    setInterval(() => {
      this.time = Utils.clock();
    }, 1000);
  }


  async switchChartData() {
    const query = {
      q: 'days',
      param: this.filter.days
    };
    await this.getChartEnrolleeVisitData(query);
  }



  private async getChartEnrolleeVisitData(item) {
    this.waiting = true;
    await this.chartService
      .chartHmoEnrolleeVisitData(item)
      .then(data => {
        const labels: string[] = (<any>data.data).lastSevenDaysLabel;
        if (labels.length > 0) {
          this.formatDataVisit(labels);
        }
        this.lineChartData = (<any>data.data).data;

    this.chartLineOption3 = {
...echartStyles.lineNoAxis, ...{
          series: [{
            data: (<any>data.data).data,
            lineStyle: {
              color: 'rgba(5, 56, 118, .86)',
              width: 3,
              shadowColor: 'rgba(0, 0, 0, .2)',
              shadowOffsetX: -1,
              shadowOffsetY: 8,
              shadowBlur: 10
            },
            label: { show: true, color: '#212121' },
            type: 'line',
            smooth: true,
            itemStyle: {
              borderColor: 'rgba(7, 102, 216, 0.86)'
            }
          }]
        }
      };
      this.chartLineOption3.xAxis.data = this.lineChartLabels;
        this.waiting = false;
      })
      .catch(error => { 
        console.log(error);
      });
  }


  private formatDataVisit(data: string[]) {
    const pipe = new DatePipe('en-US');
    return data.forEach(element => {
      const formatedDate = pipe.transform(element, 'mediumDate');
      this.lineChartLabels.push(formatedDate);
    });
  }



  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }
}
