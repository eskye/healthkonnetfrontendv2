import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HealthCareComponent } from './health-care/health-care.component';
import { BankComponent } from './settings/bank/bank.component';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'users',
    loadChildren: './users-management/users-management.module#UsersManagementModule'
  },
  {
    path: 'health-care-list',
    component: HealthCareComponent
  },
  {
    path: 'appsetting',
    loadChildren: './appsettings/appsettings.module#AppsettingsModule'
  },
  {
    path: 'log',
    component: AuditTrailComponent
  },
  {
    path: 'settings/bank',
    component: BankComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemAdminRoutingModule { }
