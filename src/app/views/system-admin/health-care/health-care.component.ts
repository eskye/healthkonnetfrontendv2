import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Utils } from 'src/app/shared/utils';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HealthcareService } from 'src/app/shared/services/healthcare.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { routes } from 'src/app/shared/constant';

@Component({
  selector: 'app-health-care',
  templateUrl: './health-care.component.html',
  styleUrls: ['./health-care.component.scss']
})
export class HealthCareComponent extends BaseComponent implements OnInit {
  fileExtensionMessage: string;
  fileExtensionError: boolean;
  uploadedPercentage: number;
  filename: string;
  formData: FormData = new FormData();
  private modalRef: NgbModalRef;
  isUpload = false;
  constructor(private modalService: NgbModal, private healthCareService: HealthcareService) {
    super(null, null, null, null, healthCareService);
  }

  ngOnInit() {
    this.getCountry();
    this.init();
    this.filter.address = '';
  }

  init() {
    this.url = routes.HEALTHCARE.COUNT;
    this.query = {
      count: this.paginationConfig.count = 100,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

  open(content, action: boolean) {
      this.isUpload = action;
    this.modalRef = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
    this.modalRef.result.then((result) => {
      // console.log(result);
    }, (reason) => {
     // console.log('Err!', reason);
    });
  }

  async onFileChange(event) {
    const file = <File>event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, false, true)) {
      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        // reader.onload = () => {
        //   this.profilePic = reader.result;
        // };
      }
      if (event.target.files.length > 0) {
         this.formData.append('file', file);
      }
    } else {
      this.fileExtensionMessage = 'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.healthCareService.warningAlert(this.fileExtensionMessage);
    }

  }


  submit(flag: boolean) {
    this.waiting = true;
    if (flag) {
      this.UploadHealthCareBulkList();
    } else {
      this.AddnewHealthCare();
    }

  }


  private AddnewHealthCare() {
    this.healthCareService.createSingle(this.item).subscribe(res => {
      this.waiting = false;
      this.item = {};
      this.healthCareService.successAlert('Record created', 'Create');
      this.modalRef.close();
      this.init();
    }, error => {
      this.waiting = false;
      this.healthCareService.
        errorAlert('An error occurred, try again', 'Error!!!');
    });
  }

  private UploadHealthCareBulkList() {
    this.healthCareService.uploadBulkList(this.formData).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          break;
        case HttpEventType.Response:
          this.waiting = false;
          this.healthCareService.successAlert('Uploaded successfully', 'Uploaded');
          this.modalRef.close();
          this.init();
          break;
        case 1: {
          this.waiting = true;
          if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
            this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
          }
          break;
        }
      }
    }, error => {
      this.waiting = false;
      this.modalRef.close();
      this.healthCareService.
        errorAlert('An error occurred while uploading logo to the server, try again', 'Error Uploading File To Server');
    });
  }
}
