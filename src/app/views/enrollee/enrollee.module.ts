import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnrolleeRoutingModule } from './enrollee-routing.module';
 import { DashboardComponent } from './dashboard/dashboard.component';
import { CompleteProfilComponent } from './onboard/complete-profil/complete-profil.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { OthersProfileComponent } from './onboard/others-profile/others-profile.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { PaymentOnboardComponent } from './onboard/complete-profil/payment-onboard/payment-onboard.component';
import { ChangeProviderComponent } from './change-provider/change-provider.component'
import { HealthTipsComponent } from './health-tips/health-tips.component'
import { PackageComponent } from './package/package.component'
import { ProfileComponent } from './profile/profile.component';
import { EmergencyComponent } from './emergency/emergency.component';
import { EventsComponent } from './events/events.component';
import { DetailHealthTipsComponent } from './health-tips/detail-health-tips/detail-health-tips.component';
import { EventDetailComponent } from './media/event-detail/event-detail.component';
import { EventEnrolleeListComponent } from './media/event-enrollee-list/event-enrollee-list.component';
const components = [
  DashboardComponent,
  CompleteProfilComponent,
  OthersProfileComponent,
  PaymentOnboardComponent,
  ChangeProviderComponent,
  HealthTipsComponent,
  PackageComponent,
  ProfileComponent,
  EmergencyComponent,
  EventsComponent,
  DetailHealthTipsComponent,
  EventDetailComponent,
  EventEnrolleeListComponent
];
@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    SharedPipesModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    EnrolleeRoutingModule
  ],
  declarations: components
})
export class EnrolleeModule { }
