import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss']
})
export class PackageComponent extends BaseComponent implements OnInit {

  constructor(titleService: Title, private modalService: NgbModal, private enrolleeService: EnrolleeService) {
    super(null, null, null, titleService, enrolleeService);
  }

  ngOnInit() {
    this.titleRoute('Enrollee Package');
    this.getCurrentPackage();
  }

  open(content){
    this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    })
  }

  getCurrentPackage() {
    this.waiting = true;
    this.enrolleeService.getCurrentPackage().subscribe(
      res => {
        this.items = res.data;
        this.waiting = false;
      },
      error => {
        this.waiting = false;
        this.error.Message = error.error.message;
        this.error.isError = true;
        this.enrolleeService.errorAlert(error.error.message, 'Error');
      }
    );
  }
}
