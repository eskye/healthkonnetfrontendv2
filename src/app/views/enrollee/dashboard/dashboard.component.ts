import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EChartOption } from 'echarts';
import { echartStyles } from '../../../shared/echart-styles';
import { IProfile } from 'src/app/shared/common/model/IProfile';
import { Utils } from 'src/app/shared/utils';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { EmitService } from 'src/app/shared/services/emit.service';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { StoreService } from 'src/app/shared/services/store.service';
 
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends BaseComponent implements  OnInit {
  profile: IProfile; 
  chartLineOption1: EChartOption;
  chartLineOption2: EChartOption;
  chartLineOption3: EChartOption;
  salesChartBar: EChartOption;
  salesChartPie: EChartOption;
  time: string;
  hmoDetail: any;
  barcode: any;

  constructor(private ds: DataStoreService,_router: Router, _activatedRoute: ActivatedRoute, private store: StoreService,
     private emitService: EmitService, private enrolleeService: EnrolleeService) {
    super(null, _router, _activatedRoute, null, enrolleeService);
  }

  ngOnInit() {
    setInterval(() => {
      this.time = Utils.clock();
    }, 1000);
    this.getLoggedInUser();
     this.getEnrolleeDetails();
  }
  

  getLoggedInUser() {
    this.activatedRoute.data.subscribe(data => {
      this.profile = <IProfile>data['user'];
      this.redirectToAppRoute(this.profile);
      this.emitService.changeData(this.profile);
      this.emitService.changeMessage(this.profile.isActive);
    });
  }

 
  get greeting(){ 
    return this.store.greet;
   }

  getHmoDetail(id) {
    this.enrolleeService.getService(HmoService).getDetails(id).subscribe(
      res => {
        this.hmoDetail = res;
        this.waiting = false;
      },
      error => {
        this.enrolleeService.errorAlert(error, 'Notification');
      }
    );
  }

  getEnrolleeDetails() {
    this.waiting = true;
    this.enrolleeService.getEnrolleeByUserProfileId().subscribe(res => {
      this.getHmoDetail(res.hmoId);
    }, error => {
      this.enrolleeService.errorAlert(error, 'Notification');
    });
  }

  redirectToAppRoute(profile) {
    if (!profile.passwordChanged) {
        this.goToNav('/sessions/change-password');
    } else if (!profile.isEnrolleeDetails) {
      this.goToNav('enrollee/complete');
    } else if (!profile.isFacilityUpdated) {
      this.goToNav('enrollee/others');
    } else if (profile.isPersonal && !profile.isPlanUpdated) {
      this.goToNav('enrollee/final');
    }
  }

  generateBarcode() { 
    this.waiting = true;
    this.enrolleeService.generateEnrolleeBarcode().subscribe(
      res => { 
        this.barcode = res;
        this.waiting = false;
      },
      error => {
        this.waiting = false;
        this.enrolleeService.errorAlert(error, 'Error');
      }
    );
  }

}
