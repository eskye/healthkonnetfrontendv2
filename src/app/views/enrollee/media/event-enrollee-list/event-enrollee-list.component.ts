import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EventService } from 'src/app/shared/services/event.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { routes, notifications } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-event-enrollee-list',
  templateUrl: './event-enrollee-list.component.html',
  styleUrls: ['./event-enrollee-list.component.scss']
})
export class EventEnrolleeListComponent extends BaseComponent implements OnInit {

  constructor(private eventService: EventService,
    titleService: Title, router: Router) {
    super(null, router, null, titleService, eventService);
  }
  ngOnInit() {
    this.init();
  }

  init() { 
     this.waiting = true;
    this.url = routes.EVENT.COUNT;
  this.query = {
    count: this.paginationConfig.count,
    page: this.paginationConfig.page,
    orderByExpression: JSON.stringify({direction: 1, column: 1}),
    whereCondition: this.filter
  };
  this.genPagination().subscribe((res) => {
    // this.totalCount = res.data.total;
    this.eventService.getService(EmitService).checkTotalCount(res.data.total);
    this.items = res.data.items;
    // this.getSumTotal(this.items);
    this.waiting = false;
 }, _error => {
   this.waiting = false;
   this.eventService.errorAlert('An error occurred', notifications.error);
 });
  }

}
