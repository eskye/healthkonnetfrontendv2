import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { PostService } from 'src/app/shared/services/post.service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { routes, notifications } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-health-tips',
  templateUrl: './health-tips.component.html',
  styleUrls: ['./health-tips.component.scss']
})
export class HealthTipsComponent extends BaseComponent implements OnInit {

  constructor(private postService: PostService,
    titleService: Title, router: Router) {
    super(null, router, null, titleService, postService);
  }
  ngOnInit() {
    this.init();
  }

  init() {
    this.waiting = true;
    this.url = routes.POST.COUNT;
  this.query = {
    count: this.paginationConfig.count,
    page: this.paginationConfig.page,
    orderByExpression: JSON.stringify({direction: 1, column: 1}),
    whereCondition: this.filter
  };
  this.genPagination().subscribe((res) => {
    // this.totalCount = res.data.total;
    this.postService.getService(EmitService).checkTotalCount(res.data.total);
    this.items = res.data.items;
    // this.getSumTotal(this.items);
    this.waiting = false;
 }, _error => {
  this.waiting = false;
  this.postService.errorAlert('An error occurred', notifications.error);
});
  }

}
