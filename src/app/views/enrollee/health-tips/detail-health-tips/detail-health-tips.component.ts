import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detail-health-tips',
  templateUrl: './detail-health-tips.component.html',
  styleUrls: ['./detail-health-tips.component.scss']
})
export class DetailHealthTipsComponent extends BaseComponent implements OnInit {

  id: string;
  constructor(location: Location, activatedRoute: ActivatedRoute, router: Router) {
    super(location, router, activatedRoute, null, null);
  }

  ngOnInit() {
    this.id = this.getParamValue('id');
    // console.log(this.id);
  }

}
