import { ValidateInputService } from 'src/app/shared/services/validate-input.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-pin-verification',
  templateUrl: './pin-verification.component.html',
  styleUrls: ['./pin-verification.component.scss'],
  animations: [SharedAnimations]
})
export class PinVerificationComponent implements OnInit {
verificationForm: FormGroup;
isPassword = true;
isPin = false;
tip = 'Enter Confirmation Code';
  item: any;
  loading: boolean;
  successText: string;
  isSuccess: boolean;
  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.validate();
  }

  validate() {
    this.verificationForm = new FormGroup({
      pin: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), ValidateInputService.passwordValidator]),
      confirmpassword: new FormControl('', Validators.required),
    }, ValidateInputService.passwordMatchValidator.bind(this));
  }
 showPassword() {
   this.isPassword = false;
   this.isPin = true;
   this.tip = 'Create a strong password';
 }
 back() {
  this.isPassword = true;
  this.isPin = false;
  this.tip = 'Enter Confirmation Code';
 }

 submit() {
  if (this.verificationForm.valid) {
    this.loading = true;
     this.item = this.verificationForm.value; 
     this.accountService.activatePin(this.item).subscribe(res => {
       this.isSuccess = (<boolean>res);
       if (this.isSuccess) { this.successText = 'Account Verified'; } else {
         this.successText = 'Failed to confirm code, Please check and try again';
       }
       this.loading = false;
     }, error => {
       this.successText = error;
         this.isSuccess = false;
          this.loading = false;
        });
     return;
  }
  return this.successText = 'Password and Confirm Password is required';
}

}
