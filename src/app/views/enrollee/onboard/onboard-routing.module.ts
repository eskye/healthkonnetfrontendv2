import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainViewComponent } from './main-view/main-view.component';
import { PinVerificationComponent } from './pin-verification/pin-verification.component';

const routes: Routes = [
  {path: '', component: MainViewComponent, children: [
    {path: '', redirectTo: 'confirmation'},
    {path: 'confirmation', component: PinVerificationComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardRoutingModule { }
