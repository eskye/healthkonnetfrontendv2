import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Router, ActivatedRoute } from '@angular/router';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { PlanService } from 'src/app/shared/services/plan.service';
import { INameAndId } from 'src/app/shared/common/model/INameAndId';
import { EmitService } from 'src/app/shared/services/emit.service';
import { IProfile } from 'src/app/shared/common/model/IProfile';
import { HmoService } from 'src/app/shared/services/hmo.service';
@Component({
  selector: 'app-others-profile',
  templateUrl: './others-profile.component.html',
  styleUrls: ['./others-profile.component.scss']
})
export class OthersProfileComponent extends BaseComponent implements OnInit, AfterViewInit {
  healthcares$: Observable<any[]>;
  people3Loading: boolean;
  textinput$ = new Subject<string>();
  selectedItem: any;
  plans: any;
  loading: boolean;
  providers: INameAndId[];
  profile: IProfile;
  hmos: any;
  constructor(_router: Router, activatedRoute: ActivatedRoute, private enrolleeService: EnrolleeService,
     private providerService: ProviderService, private emitService: EmitService) {
    super(null, _router, activatedRoute, null, enrolleeService);
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.profile = <IProfile>data['user'];
    });
    this.getDetail();
  }

  ngAfterViewInit() {
    this.getPlans();
    this.getCountry();
    this.getHmos();
  }

  getDetail() {
   this.loading = true;
    this.enrolleeService.getEnrolleeByUserProfileId().subscribe(res => {
      this.item = res;
      this.loading = false;
    }, error => {
      this.loading = false;
      console.log(error);
    });
  }

  getPlans() {
    this.enrolleeService.getService(PlanService).get().subscribe(res =>{
       this.plans = res;
    });
  }

  getHmos() {
    this.enrolleeService.getService(HmoService).getHmo().subscribe(res =>{
       this.hmos = res;
    });
  }
  submit() {
    this.waiting = true;
    this.enrolleeService.createEnrollee(this.item).subscribe(res => {
      this.enrolleeService.successAlert('You have successfully completed account setup');
      this.goToNav('/enrollee/final');
    }, error => {
      this.waiting = false;
      this.enrolleeService.errorAlert(error);
    });
  }

  getProviderByState() {
    this.providerService.searchProvider(this.item.state).subscribe(res =>{
      this.providers = res;
    }, error => { console.log(error); });
  }

  private loadProvider() {
    this.healthcares$ = concat(
      of([]), // default items
      this.textinput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.people3Loading = true)),
        switchMap(term => term.length < 3 ? of([]) :
          this.providerService.searchProvider(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.people3Loading = false))
          )
        )
      )
    );
  }
}
