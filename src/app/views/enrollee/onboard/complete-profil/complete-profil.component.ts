import { Utils } from 'src/app/shared/utils';
import { Component, OnInit } from '@angular/core';
import { UserProfileService } from 'src/app/shared/services/user-profile.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Router } from '@angular/router';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';


@Component({
  selector: 'app-complete-profil',
  templateUrl: './complete-profil.component.html',
  styleUrls: ['./complete-profil.component.scss']
})
export class CompleteProfilComponent extends BaseComponent implements OnInit {
  isProfile = false;
  isPictureUpload = true;
  tip = 'Complete your personal information';
  filename: any;
  fileExtensionError: boolean;
  isImage: boolean;
  fileExtensionMessage: string;
  formData = new FormData();
  constructor(_router: Router, private userProfileService: UserProfileService, private fileManagerService: FileManagerService) {
    super(null, _router, null, null, userProfileService);
  }

  ngOnInit() {
    this.getCountry();
    this.getUserDetail();
  }

  getUserDetail() {
    this.userProfileService.getUserDetail().subscribe(res => {
      this.item = res;
      if (this.item.isEnrolleeDetails) {
        this.goToNav('/enrollee/dashboard');
      }
    }, error => {
      console.log(error);
    });
  }
  showProfilePicture() {
    this.isPictureUpload = false;
    this.isProfile = true;
    this.tip = 'Upload your recent passport photograph';
  }
  back() {
   this.isPictureUpload = true;
   this.isProfile = false;
   this.tip = 'Complete your personal information';
  }

  async onFileChange(event) {
    const file = event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, true)) {
      this.isImage = true;
      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          (<any>document.getElementById('Company_Logo_src')).src =
            reader.result;
        };
      }
      if (event.target.files.length > 0) {
        this.formData.append('File', file);
      }
    } else {
      this.fileExtensionMessage =
        'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
    }
  }
 submit() {
   this.MakeUploadRequest(this.formData);
 }
  private MakeUploadRequest(formData: FormData) {
    this.fileManagerService.uploadProfileImage(formData).subscribe(
      (event: HttpEvent<any>) => {
        // console.log(event);
        switch (event.type) {
          case HttpEventType.Sent:
            break;
          case HttpEventType.Response:
            this.isUploading = false;
            this.waiting = true;
            this.item.profilePic = (<any>event).body.data.result;
             this.userProfileService.updateUserDetail(this.item).subscribe(res => {
               this.waiting = false;
               this.goToNav('/enrollee/others');
               // this.userProfileService.successAlert('You have successfully setup your account');
             }, error => {
               this.waiting = false;
               this.userProfileService.errorAlert('Oops, An error occurred, try again');
             });
            // this.fileManagerService.successAlert((<any>event).body.data.filename);
            // this.msg = (<any>event).body.data.error;
            break;
          case 1:
            {
              this.isUploading = true;
              // if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
              //   this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
            }
            break;
        }
      }, error => {
        this.isUploading = false;
        this.fileManagerService.errorAlert(
          'An error occurred while uploading logo to the server, try again',
          'Error Uploading File To Server'
        );
      }
    );
  }
}
