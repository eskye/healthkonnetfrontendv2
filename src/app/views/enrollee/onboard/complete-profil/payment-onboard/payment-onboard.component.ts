import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { IProfile } from 'src/app/shared/common/model/IProfile';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
import { PaymentService } from 'src/app/shared/services/payment.service';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { INameAndId } from 'src/app/shared/common/model/INameAndId';

@Component({
  selector: 'app-payment-onboard',
  templateUrl: './payment-onboard.component.html',
  styleUrls: ['./payment-onboard.component.scss']
})
export class PaymentOnboardComponent extends BaseComponent implements OnInit {
  profile: IProfile;
  private modalRef: NgbModalRef;
  loading: boolean;
  hmos: any;
  plans: INameAndId[];

  constructor(_router: Router, activatedRoute: ActivatedRoute, private enrolleeService: EnrolleeService,
    private hmoService: HmoService, private modalService: NgbModal,
    @Inject(DOCUMENT) private document: any, private paymentService: PaymentService) {
   super(null, _router, activatedRoute, null, enrolleeService);
 }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.profile = <IProfile>data['user'];
    });
     this.redirectRoute();
    this.getDetail();
    this.getHmos();
  }

  private redirectRoute() {
    if (!this.profile.isPersonal) {
      this.goToNav('/enrollee/dashboard');
    }
  }

  getDetail() {
    this.loading = true;
     this.enrolleeService.getEnrolleeByUserProfileId().subscribe(res => {
       this.item = res;
       this.loading = false;
       this.getPackageByHmoId(this.item.hmoId);
     }, error => {
       this.loading = false;
       console.log(error);
     });
   }
   getHmos() {
    this.hmoService.getHmo().subscribe(res =>{
       this.hmos = res;
    });
  }
  getPackageByHmoId(id?) {
    this.hmoService.searchPackages(id).subscribe(res => { 
      this.plans = res;
    }, error => { console.log(error); });
  }

  open(content) {
    this.modalRef = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
    this.modalRef.result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log('Err!', reason);
    });
  }

  fundAccount() {
    this.waiting = true;
   // const data = {narration: this.subscriptionForm.value.narration, amount: this.subscriptionForm.value.amount};
    this.paymentService.fundWallet(this.item).subscribe((response) => {
      this.paymentService.successAlert('Redirecting to payment portal...');
      this.waiting  = false;
      this.document.location.href = response.data.authorization_url;
    }, error => {
      this.waiting = false;
      this.paymentService.errorAlert(error.error.message);

    });
  }

  submit() {
    this.waiting = true;
    this.enrolleeService.addEnrolleePlan(this.item).subscribe(res =>{
      this.enrolleeService.successAlert('Successfully setup your account');
      this.goToNav('/enrollee/dashboard');
    }, error =>{
      this.enrolleeService.errorAlert(error);
    })
  }

}
