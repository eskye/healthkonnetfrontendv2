import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardRoutingModule } from './onboard-routing.module';
import { MainViewComponent } from './main-view/main-view.component';
import { CompleteProfilComponent } from './complete-profil/complete-profil.component';
import { PinVerificationComponent } from './pin-verification/pin-verification.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { ValidateInputService } from 'src/app/shared/services/validate-input.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
const components = [
  MainViewComponent,
  PinVerificationComponent
];
@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    SharedComponentsModule,
     OnboardRoutingModule],
  declarations: components,
  providers: [ValidateInputService]
})

export class OnboardModule {}
