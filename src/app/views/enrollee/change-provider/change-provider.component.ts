import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent'; 
import { isNullOrUndefined } from 'util';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { AuthService } from 'src/app/shared/services/auth.service';

const hmo = [
  {
    Provider: 'ALK-Abello, Inc.',
    Enrollee_Name: 'Kelly Iglesiaz',
    Status: 'Approved',
    date: '2018-07-15 04:57:33',
    Action: 'Both'
  },
  {
    Provider: 'Apotex Corp.',
    Enrollee_Name: 'Honoria Heis',
    Status: 'Awaiting Approval',
    date: '2018-11-19 13:47:17',
    Action: 'View'
  },
  {
    Provider: 'Nelco Laboratories, Inc.',
    Enrollee_Name: 'Markos Rolfo',
    Status: 'Approved',
    date: '2018-12-03 06:40:02',
    Action: 'Confirm'
  },
  {
    Provider: 'Rebel Distributors Corp',
    Enrollee_Name: 'Henderson Davidovitz',
    Status: 'Awaiting Approval',
    date: '2018-11-16 08:54:36',
    Action: 'View'
  },
  {
    Provider: 'Ranbaxy Laboratories Inc.',
    Enrollee_Name: 'Steffi Capini',
    Status: 'Approved',
    date: '2019-02-27 20:09:28',
    Action: 'View'
  },
  {
    Provider: 'Jiangsu Province JianErKang, Ltd.',
    Enrollee_Name: 'Laurel McCusker',
    Status: 'Approved',
    date: '2018-10-21 11:02:46',
    Action: 'View'
  },
  {
    Provider: 'Clinical Solutions Wholesale',
    Enrollee_Name: 'Natividad Rigglesford',
    Status: 'Approved',
    date: '2018-11-04 13:27:55',
    Action: 'Confirm'
  },
  {
    Provider: 'Homeocare Laboratories',
    Enrollee_Name: 'Tedda Panas',
    Status: 'Awaiting Approval',
    date: '2018-08-14 12:04:50',
    Action: 'Confirm'
  }
];
const filtertype = ['All', 'Awaiting Approval', 'Approved'];

@Component({
  selector: 'app-change-provider',
  templateUrl: './change-provider.component.html',
  styleUrls: ['./change-provider.component.scss']
})
export class ChangeProviderComponent extends BaseComponent implements OnInit {
  HMOS = hmo;
  status = filtertype;
  // private _logo = '';
  isImage: boolean;
  fileExtensionError: boolean;
  fileExtensionMessage: string;
  filename: any;
  userRole: any;
  constructor(
    private modalService: NgbModal,
    private enrolleeService: EnrolleeService, private auth : AuthService) {
    super(null, null, null, null, enrolleeService);
    this.userRole = this.auth.store.getData('role');
  }

  ngOnInit() {
  }

changeProviderHandler(event) {
  if(!isNullOrUndefined(event)){
    this.waiting = true;
    this.enrolleeService.changeProvider(event).subscribe(res =>{
      this.enrolleeService.successAlert('Your request to change current health care provider has been submitted', 'Success Alert');
    }, error => {
      this.enrolleeService.errorAlert(error, 'Error Alert');
    });
  }
}
refresh(event) {
  this.reloadComponent();
}

  deleteInvoice(id, modal) {
    this.modalService
      .open(modal, { ariaLabelledBy: 'modal-basic-title', centered: true })
      .result.then(
        result => {
          this.enrolleeService.successAlert('Invoice Deleted!', 'Success!');
        },
        reason => {}
      );
  }
  open(content){
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', centered: true })
  }
}
