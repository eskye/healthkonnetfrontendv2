import { HealthTipsComponent } from './health-tips/health-tips.component';
import { UserProfileResolver } from './../../shared/resolvers/user-profile.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompleteProfilComponent } from './onboard/complete-profil/complete-profil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OthersProfileComponent } from './onboard/others-profile/others-profile.component';
import { PaymentOnboardComponent } from './onboard/complete-profil/payment-onboard/payment-onboard.component';
import { ChangeProviderComponent } from './change-provider/change-provider.component'
import { PackageComponent } from './package/package.component'
import { ProfileComponent } from './profile/profile.component';
import { EmergencyComponent } from './emergency/emergency.component';
import { EventEnrolleeListComponent } from './media/event-enrollee-list/event-enrollee-list.component';
import { EventDetailComponent } from './media/event-detail/event-detail.component';
import { DetailHealthTipsComponent } from './health-tips/detail-health-tips/detail-health-tips.component';


const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent, resolve: {user: UserProfileResolver}},
  {path: 'changeProvider', component: ChangeProviderComponent},
  {path: 'healthtips', component: HealthTipsComponent},
  {path: 'healthtips/:id/:slug', component: DetailHealthTipsComponent},
  {path: 'events', component: EventEnrolleeListComponent},
  {path: 'events/:id/:slug', component: EventDetailComponent},
  {path: 'package', component: PackageComponent},
  {path: 'emergency', component: EmergencyComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'complete', component: CompleteProfilComponent},
  {path: 'others', component: OthersProfileComponent, resolve: {user: UserProfileResolver} },
  {path: 'final', component: PaymentOnboardComponent, resolve: {user: UserProfileResolver} },
  {path: '', redirectTo: 'dashboard'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnrolleeRoutingModule { }
