import { ProcessedComponent } from './claim/processed/processed.component';
import { EnrolleeProviderComponent } from './enrollee-provider/enrollee-provider/enrollee-provider.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderRoutingModule } from './provider-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VerifyComponent } from './enrollee-provider/verify/verify.component';
import { ListComponent } from './enrollee-provider/list/list.component';
import { AuthorizationCodeComponent } from './enrollee-provider/authorization-code/authorization-code.component';
import { AddClaimComponent } from './enrollee-provider/add-claim/add-claim.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ClaimListComponent } from './claim/claim-list/claim-list.component';
import { AvatarModule } from 'ngx-avatar';
import { ClaimComponent } from './claim/claim.component';
import { BatchedClaimDetailComponent } from './claim/batched-claim-detail/batched-claim-detail.component';
import { ClaimBatchDetailComponent } from './claim/claim-batch-detail/claim-batch-detail.component';
import { AuthorizationCodeProviderComponent } from './claim/authorization-code-provider/authorization-code-provider.component';
import { EnrolleeDetailProviderComponent } from './enrollee-provider/enrollee-detail-provider/enrollee-detail-provider.component';

const components = [
  DashboardComponent,
  ListComponent,
  VerifyComponent,
  EnrolleeProviderComponent,
  AuthorizationCodeComponent,
  AddClaimComponent,
  ClaimListComponent,
  ClaimComponent,
  ProcessedComponent,
  BatchedClaimDetailComponent,
  ClaimBatchDetailComponent,
  AuthorizationCodeProviderComponent,
  EnrolleeDetailProviderComponent
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    SharedPipesModule,
    NgSelectModule,
    NgbModule,
    AvatarModule,
    NgxEchartsModule,
    ProviderRoutingModule
  ],
  declarations: components
})
export class ProviderModule { }
