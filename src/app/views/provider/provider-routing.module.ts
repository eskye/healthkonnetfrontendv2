import { AuthorizationCodeProviderComponent } from './claim/authorization-code-provider/authorization-code-provider.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EnrolleeProviderComponent } from './enrollee-provider/enrollee-provider/enrollee-provider.component';
import { ListComponent } from './enrollee-provider/list/list.component';
import { VerifyComponent } from './enrollee-provider/verify/verify.component';
import { AuthorizationCodeComponent } from './enrollee-provider/authorization-code/authorization-code.component';
import { AddClaimComponent } from './enrollee-provider/add-claim/add-claim.component';
import { ClaimListComponent } from './claim/claim-list/claim-list.component';
import { ClaimComponent } from './claim/claim.component';
import { ProcessedComponent } from './claim/processed/processed.component';
import { BatchedClaimDetailComponent } from './claim/batched-claim-detail/batched-claim-detail.component';
import { ClaimBatchDetailComponent } from './claim/claim-batch-detail/claim-batch-detail.component';
import { EnrolleeDetailProviderComponent } from './enrollee-provider/enrollee-detail-provider/enrollee-detail-provider.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'claims',
    component: ClaimComponent,
    children: [
      {path: 'batched', component: ClaimListComponent},
      {path: 'batched/:id/list', component: BatchedClaimDetailComponent},
      {path: 'batched/:id/detail', component: ClaimBatchDetailComponent},
      {path: 'processed', component: ProcessedComponent},
      {path: 'request-authorization', component: AuthorizationCodeProviderComponent}
    ]
  },
  {
    path: 'enrollee', component: EnrolleeProviderComponent,
    children: [
      {path: 'list', component: ListComponent},
      {path: ':id/detail', component: EnrolleeDetailProviderComponent},
      {path: 'verify', component: VerifyComponent},
      {path: 'authorizationcodes', component: AuthorizationCodeComponent},
      {path: 'add-claim', component: AddClaimComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRoutingModule { }
