import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { FormGroup, FormArray, NgForm } from '@angular/forms';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { VisitService } from 'src/app/shared/services/visit.service';
import { IDrug, IServiceClaim } from 'src/app/shared/common/model/IClaim';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { isNullOrUndefined } from 'util';
import { TariffService } from 'src/app/shared/services/tariff.service';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { INameAndId } from 'src/app/shared/common/model/INameAndId';

@Component({
  selector: 'app-add-claim',
  templateUrl: './add-claim.component.html',
  styleUrls: ['./add-claim.component.scss']
})
export class AddClaimComponent extends BaseComponent implements OnInit {
  response: any;
  providerData: any;
  userData: any;
  isClaim = false;
  isAttachment = false;
  isTariff = true;
  isService = true;
  title = 'Claim Request';
  diagnosis = {};
  drugsList$: Observable<any[]>;
  people3Loading = false;
  textinput$ = new Subject<string>();
  claimForm: FormGroup;
  claimItems: FormArray;
  amount = 0;
  totalAmount = 0;
  totalServiceAmount = 0;
  amountService = 0;
  isBatch = 0;
  claimId = 0;
  item: any = { diagnosisModel: {}, investigationModel: {}};
  public form: {
 drugs: IDrug[];
};
public form2: {
  services: IServiceClaim[];
};
  claimBatches: INameAndId[];
  next: boolean;
  token: any;
  ecode: any;
  fetchingDrug: boolean;

  constructor(route: ActivatedRoute, private enrolleeService: EnrolleeService, private claimService: ClaimService,
    private providerService: ProviderService, private tariffService: TariffService,
     private visitService: VisitService) {
    super(null, null, route, null, enrolleeService);

    this.form = {
      drugs: []
    };
    this.form2 = {
      services: []
    };
    this.addDrugs();
    this.addServices();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['token'];
      this.ecode = params['ecode'];
    });
    if (this.token && this.ecode) {
      this.item.authorizationCode = this.token;
      this.item.name = this.ecode;
    }
    this.getSClaimBatchs();
  }
  public addDrugs(): void {
      // CAUTION: When we output the form controls, we need to provide a unique name
      // for each input (so that it can be registered with the parent NgForm). For the
      // sake of this demo, we're going to use the current TIMPESTAMP (Date.now()) as a
      // hook into something unique about this model.
        this.form.drugs.push({
          unq: Date.now(), // <--- uniqueness hook.
          name: '',
          dosageDescription: '',
          quantityPrescribed: 0,
          cost: 0
        });
  }
  public addServices(): void {
      this.form2.services.push({
          unq: Date.now(),
          name: '',
          description: '',
          cost: 0
      });
}
  // I process the form-model.
public processForm( form: any ): void {
  this.item.medicationModels = this.form.drugs;
   // console.log( this.item);

  }
  public processServiceForm( form: any ): void {
    this.item.serviceRenderModels = this.form2.services;
    console.log( this.item);
    this.submitClaimFormFinal();
}


// I remove the pet at the given index.
public removeDrug( index: number) {
  const drug = this.form.drugs[index];
  this.totalAmount = this.amount - drug.cost;
  this.form.drugs.splice( index, 1 );
}

// I remove the pet at the given index.
public removeService( index: number) { 
  const service = this.form2.services[index];
  this.totalServiceAmount = this.amountService - service.cost;
  this.form2.services.splice( index, 1 );
}

  generateVisitCode() {
    this.error.isError = false;
    this.error.Message = '';
    this.visitService.generateVisitCode(this.item).subscribe(res => {
      this.waiting = false;
      this.response = res.data;
    }, error => {
      this.waiting = false;
      this.error.isError = true;
      this.error.Message = error;
      this.enrolleeService.errorAlert(error);
    });
  }

  computeTotalAmount() {
    this.amount = 0;
    this.form.drugs.forEach((drug, i) =>{
      this.amount += drug.cost;
    }); 
    this.totalAmount = this.amount;
  }
  calculatePrice(drug: any) {
    if (drug.quantityPrescribed === 0) {
      return false;
    } else {
      drug.cost = drug.price * drug.quantityPrescribed;
      this.computeTotalAmount();
    }

  }
  computeServiceTotalAmount() {
    this.amountService = 0;
    this.form2.services.forEach((service, i) =>{
      this.amountService += service.cost;
    });
    this.totalServiceAmount = this.amountService;
  }
back(tab: number) {
    switch (tab) {
      case 1:
        this.isAttachment = true;
        this.isClaim = false;
         this.title = 'Claim Request';
        break;
      case 2:
          this.isAttachment = false;
          this.isTariff = true;
          this.isClaim = true;
          this.isService = true;
          this.title = 'Diagnosis and Investigation';
         break;
      case 3:
          this.isAttachment = true;
          this.isTariff = false;
          this.isClaim = true;
          this.isService = true;
          this.title = 'Drug Service';
         break;
      default:
        break;
    }
  }
  show(tab: number) {
    switch (tab) {
      case 1:
          this.isAttachment = false;
          this.isTariff = true;
          this.isService = true;
          this.isClaim = true;
          this.title = 'Diagnosis and Investigation';
        break;
      case 2:
          this.isAttachment = true;
          this.isTariff = false;
          this.isClaim = true;
          this.isService = true;
          this.title = 'Drugs';
          break;
      case 3:
          this.isAttachment = true;
          this.isTariff = true;
          this.isClaim = true;
          this.isService = false;
          this.title = 'Services Render';
          this.getServicesTypes();
          break;
      default:
        break;
    }
  }

validateDrug(value, drug) {
  if (!isNullOrUndefined(drug.error)) {
     drug.error = '';
  }
  if (value.length > 0) {
    drug.fetchingDrug = true;
    this.providerService.getDrugPrice(value, this.response.hmoId).subscribe(res => {
      drug.price = res.price;
      drug.fetchingDrug = false;
      this.calculatePrice(drug);
    }, error => {
      drug.fetchingDrug = false;
      drug.error = error;
    });
  }
}
validateService(value, service) {
  if (!isNullOrUndefined(service.error)) {
     service.error = '';
  }
  if (value.length > 0) {
    service.fetchingDrug = true;
    this.providerService.getServicePrice(value, this.response.hmoId).subscribe(res => {
      service.cost = res.price;
      service.fetchingDrug = false;
       this.computeServiceTotalAmount();
    }, error => {
      service.fetchingDrug = false;
      service.error = error;
    });
  }
}

getServicesTypes() {
  this.tariffService.getServiceTypes(this.response.hmoId).subscribe(res => {
    this.items = res;
  }, error => {
    console.warn(error);
  });
}
getSClaimBatchs() {
  this.claimService.getClaimNameAndId().subscribe(res => {
    this.claimBatches = res;
  }, error => {
    console.warn(error);
  });
}

createBatch(f: NgForm) {
  if (f.valid) {
     const item = f.value;
     item.ultimateHmoId = this.response.hmoId;
     item.visitId = this.response.visitId;
     this.claimService.createClaimBatch(item).subscribe(res => {
       console.log(res);
       this.getSClaimBatchs();
     }, error => {
       console.log(error);
     });
  }
}

submitClaimForm(f: NgForm) {
  if (f.valid) {
    this.waiting = true;
     const item = f.value;
     item.ultimateHmoId = this.response.hmoId;
     item.visitId = this.response.visitId;
     item.enrolleeId = this.response.enrolleeId;
     this.claimService.createClaimForm(item).subscribe(res => {
       this.next = true;
       this.waiting = false;
       this.item.claimFormId = res.data.claimId;
    }, error => {
      this.waiting = false;
     this.claimService.errorAlert('Error: ' + error);
    });
  }
}
submitClaimFormFinal() {
       this.waiting = true;
     this.claimService.submitClaimFinal(this.item).subscribe(res => {
       console.log(res);
       this.waiting = false;
    }, error => {
      this.waiting = false;
     this.claimService.errorAlert('Error: ' + error);
    });
}
}
