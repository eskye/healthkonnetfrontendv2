import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';


@Component({
  selector: 'app-enrollee-detail-provider',
  templateUrl: './enrollee-detail-provider.component.html',
  styleUrls: ['./enrollee-detail-provider.component.scss']
})
export class EnrolleeDetailProviderComponent extends BaseComponent implements OnInit {

  id: string;
  constructor(location: Location, activatedRoute: ActivatedRoute, router: Router) {
    super(location, router, activatedRoute, null, null);
  }

  ngOnInit() {
    this.id = this.getParamValue('id');
   // console.log(this.id);
  }
}
