import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';
import { isUndefined } from 'util';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {
  userData: any;
  providerData: any;
  hmos: any;
  orgs: any;
  searchText: any;
  userRole: any;
  constructor(private modalService: NgbModal,
    private providerService: ProviderService,
   router: Router, location: Location) {
   super(location, router, null, null, providerService);
   // this.userData = this.providerService.getService(DataStoreService).getData('udata');
   this.userRole = this.providerService.getService(DataStoreService).getData('role');
 }
 ngOnInit() {
  this.fetchInit();
    this.init();
}

fetchInit() {
  this.providerService.fetchProviderDropDown().subscribe(res => {
    this.hmos = res[0];
    this.orgs = res[1];
  });
}
refresh(event) {
  this.reloadComponent();
}

count(event) {
  this.paginationConfig.count = event;
  this.init();
}
  init(e?) {
    if (isUndefined(e)) {
      this.filter = {};
   } else {
     this.filter = e;
   }
    this.waiting = true;
    this.url = routes.ENROLLEE.COUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.genPagination().subscribe((res) => {
      // this.totalCount = res.data.total;
      this.providerService.getService(EmitService).checkTotalCount(res.data.total);
      this.items = res.data.items;
      this.waiting = false;
   });
  }


}
