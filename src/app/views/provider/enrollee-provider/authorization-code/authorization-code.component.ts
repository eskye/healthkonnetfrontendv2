import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { Router } from '@angular/router';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { routes } from 'src/app/shared/constant';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Location } from '@angular/common';
@Component({
  selector: 'app-authorization-code-enrollee',
  templateUrl: './authorization-code.component.html',
  styleUrls: ['./authorization-code.component.scss']
})
export class AuthorizationCodeComponent extends BaseComponent implements OnInit {

  userData: any;
  providerData: any;
  hmos: any;
  orgs: any;
searchText: any;
  constructor(private modalService: NgbModal,
    private providerService: ProviderService,
   router: Router, location: Location) {
   super(location, router, null, null, providerService);
   this.userData = this.providerService.getService(DataStoreService).getData('udata');
   this.providerService.getDetails(this.userData.id).subscribe(res => {
     this.providerData = res;
    // console.log(this.providerData.providerId);
     this.init(this.providerData.providerId);

   }, error => {
   });
 }
 ngOnInit() {
  this.fetchInit();

}

fetchInit() {
  this.providerService.fetchProviderDropDown().subscribe(res =>{
    this.hmos = res[0];
    this.orgs = res[1];
  });
}

  init(id) {
    // console.log(this.filter);
    this.url = routes.ENROLLEE.COUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPagination(id);
  }

}
