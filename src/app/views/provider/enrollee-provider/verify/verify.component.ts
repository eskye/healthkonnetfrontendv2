import { ProviderService } from 'src/app/shared/services/provider.service';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { DataStoreService } from 'src/app/shared/services/data-store.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent extends BaseComponent implements OnInit {
  providerData: any;
  userData: any;
  response: any;
  constructor(private enrolleeService: EnrolleeService, private providerService: ProviderService) {
    super(null, null, null, null, enrolleeService);
  }

  ngOnInit() {
  }

  verify(item) {
    this.error.isError = false;
    this.error.Message = '';
    this.enrolleeService.verifyEnrollee(item).subscribe(res =>{
      this.waiting = false;
      this.response = res.data;
    }, error => {
      this.waiting = false;
      this.error.isError = true;
      this.error.Message = error;
      this.enrolleeService.errorAlert(error);
    });
  }

  verifyEnrollee() {
    this.waiting = true;
    this.userData = this.providerService.getService(DataStoreService).getData('udata');
   this.providerService.getDetails(this.userData.id).subscribe(res => {
     this.providerData = res;
     this.item.providerId = this.providerData.providerId;
     this.verify(this.item);
   });
  }
}
