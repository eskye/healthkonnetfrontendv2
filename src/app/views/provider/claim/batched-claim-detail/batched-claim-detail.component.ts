import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { routes } from 'src/app/shared/constant';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { ProviderService } from 'src/app/shared/services/provider.service';

@Component({
  selector: 'app-batched-claim-detail',
  templateUrl: './batched-claim-detail.component.html',
  styleUrls: ['./batched-claim-detail.component.scss']
})
export class BatchedClaimDetailComponent extends BaseComponent implements OnInit {
  batchCode: string;
  hmos: any;
  orgs: any;
  searchText: any;
  constructor(location: Location, router: Router, activatedRoute: ActivatedRoute, 
    private claimService: ClaimService, private providerService: ProviderService) {
    super(location, router, activatedRoute, null, claimService);
     this.batchCode = this.getParamValue('id');
  }

  ngOnInit() {
    this.fetchInit();
    this.filter.organizationId = 0;
    this.filter.hmoId = 0;
    this.filter.claimBatchCode = this.batchCode;
    this.init();
  }

  init() {
    this.url = routes.CLAIM.CLAIMFORMCOUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

  fetchInit() {
    this.providerService.fetchProviderDropDown().subscribe(res => {
      this.hmos = res[0];
      this.orgs = res[1];
    });
  }
  
}
