import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-claim-batch-detail',
  templateUrl: './claim-batch-detail.component.html',
  styleUrls: ['./claim-batch-detail.component.scss']
})
export class ClaimBatchDetailComponent extends BaseComponent implements OnInit {
  id: string;

  constructor(location: Location, activatedRoute: ActivatedRoute, router: Router) {
    super(location, router, activatedRoute, null, null);
  }

  ngOnInit() {
    this.id = this.getParamValue('id');
  }

}
