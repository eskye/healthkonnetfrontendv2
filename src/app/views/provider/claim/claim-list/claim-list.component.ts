import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { routes } from 'src/app/shared/constant';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-list',
  templateUrl: './claim-list.component.html',
  styleUrls: ['./claim-list.component.scss']
})
export class ClaimListComponent extends BaseComponent implements OnInit {

  constructor(router: Router, location: Location,  titleService: Title, private claimService: ClaimService) {
    super(location, router, null, titleService, claimService );
  }

  ngOnInit() {
    this.titleService.setTitle('Claiming processes');
        this.init();
  }

  init() {
    this.url = routes.CLAIM.BATCHEDCLAIMCOUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

}
