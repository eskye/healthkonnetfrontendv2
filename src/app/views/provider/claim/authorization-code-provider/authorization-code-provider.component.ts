import { AuthService } from './../../../../shared/services/auth.service';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { Observable, Subject, concat, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, map } from 'rxjs/operators';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Title } from '@angular/platform-browser';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';
import { isUndefined } from 'util';
@Component({
  selector: 'app-authorization-code-provider',
  templateUrl: './authorization-code-provider.component.html',
  styleUrls: ['./authorization-code-provider.component.scss']
})
export class AuthorizationCodeProviderComponent extends BaseComponent implements OnInit {
  enrollees$: Observable<any[]>;
  people3Loading = false;
  textinput$ = new Subject<string>();
  private modalRef: NgbModalRef;
  totalCount: any;
  userRole: any;
  modalClose: boolean;
  constructor( titleService: Title, private modalService: NgbModal, private auth : AuthService,
    private enrolleeService: EnrolleeService, private claimService: ClaimService) {
    super(null, null, null, titleService, enrolleeService);
    this.userRole = this.auth.store.getData('role');
   }

  ngOnInit() {
    this.titleService.setTitle('Request Claim Authorization Code');
   // this.loadEnrollee();
   this.init();
  }
  init(event?) {
    if (isUndefined(event)) {
      this.filter = {};
   } else {
     this.filter = event;
   }
    this.waiting = true;
    this.url = routes.CLAIM.GETREQUESTAUTHORIZATION;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.genPagination().subscribe((res) => {
      this.totalCount = res.data.total;
      this.claimService.getService(EmitService).checkTotalCount(res.data.total);
      this.items = res.data.items;
      this.waiting = false;
   });
  }

  refresh(event) {
    this.reloadComponent();
  }


submit(event) {
      this.waiting = true;
      this.claimService.requestAuthorizationCode(event).subscribe(res => {
        this.waiting = false;
        this.modalClose = false; 
        this.reloadComponent();
        this.claimService.successAlert('Authorization Request sent successfully', 'Success');
       // console.log(res);
      }, error => {
        this.waiting = false;
        this.claimService.errorAlert(error, 'Error');
      });
}
 
  private loadEnrollee() {
    this.enrollees$ = concat(
      of([]), // default items
      this.textinput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        tap(() => (this.people3Loading = true)),
        switchMap(term => term.length < 3 ? of([]) :
          this.enrolleeService.searchEnrollee(term).pipe(
            catchError(() => of([])), // empty list on error
            tap(() => (this.people3Loading = false))
          )
        )
      )
    );
  }
}
