import { EmitService } from './../../../shared/services/emit.service';
import { Component, OnInit, Inject } from '@angular/core';
import { EChartOption } from 'echarts';
import { echartStyles } from '../../../shared/echart-styles';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Utils } from 'src/app/shared/utils';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT, DatePipe } from '@angular/common';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { routes } from 'src/app/shared/constant';
import * as moment from 'moment';
import { ICounter } from 'src/app/shared/common/model/ICounter';
import { ChartService } from 'src/app/shared/services/chart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent extends BaseComponent implements OnInit {
  slideOptions = { items: 2, loop: true, dots: true, navigation: true, autoplay: true };
  chartLineOption1: EChartOption;
  chartLineOption2: EChartOption;
  chartLineOption3: EChartOption;
  chartLineOption4: EChartOption;
  salesChartBar: EChartOption;
  salesChartPie: EChartOption;
  doughnutChartLabels: string[] = [];
  doughnutChartData: number[] = [];
  doughnutChartType = 'pie';
  lineChartData: Array<any> = [];
  lineChartLabels: Array<any> = [];
  greeting: string;
  greetingimage: string;
  time: string;
  private modalRef: NgbModalRef;
  counter: ICounter;
  constructor(private ds: DataStoreService, private modalService: NgbModal,
    @Inject(DOCUMENT) private document: any,
    private chartService: ChartService, private emitService: EmitService,
     private dashboardSerivce: DashboardService, router: Router) {
    super(null, router, null, null, chartService);
  }

  ngOnInit() {
    this.filter.days = 7;
    this.greeting = `Hi ${this.ds.getData('udata').firstName}, ${Utils.greet()}`;
    this.displayClock();
    this.getDashboardData();
    const query = {
      q: 'days',
      param: this.filter.days
    };
    this.getChartEnrolleeVisitData(query);
    this.getEnrolleePackageData();
    this.chart4();
  }

  private getDashboardData() {
    this.waiting = true;
    this.dashboardSerivce.getDashboard(routes.DASHBOARD.HMODASHBOARD).subscribe(res =>{
     this.counter = res;
    }, error => {
     this.waiting = false;
     this.dashboardSerivce.errorAlert(error, 'Error Alert');
    });
  }

  private displayClock() {
    // tslint:disable-next-line: max-line-length
    Utils.greet() === 'Good morning' ? this.greetingimage = 'morning' : Utils.greet() === 'Good afternoon' ? this.greetingimage = 'afternoon' : Utils.greet() === 'Good evening' ? this.greetingimage = 'evening' : null;
    setInterval(() => {
      this.time = moment().format('h:mm:ss a');
    }, 1000);
  }

  private chart4(){

 this.chartLineOption4 = {

  color: ['#373c54'],
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    }
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  xAxis: [
    {
      type: 'category',
      data: ['Male', 'Female'],
      axisTick: {
        alignWithLabel: true
      }
    }
  ],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    name: 'Counters',
    type: 'bar',
    barWidth: '60%',
    data: [10, 52]
  }]
 }
  }

  async switchChartData() {
    const query = {
      q: 'days',
      param: this.filter.days
    };
    await this.getChartEnrolleeVisitData(query);
  }


  private async getEnrolleePackageData() {
     this.waiting = true;
    await this.chartService
      .chartHmoEnrolleeData()
      .then(data => {
        this.salesChartPie = {
          color: ['#162e63', '#162e63', '#162e63', '#8877bd', '#9181bd', '#6957af'],
          tooltip: {
            show: true,
            backgroundColor: 'rgba(0, 0, 0, .8)'
          },
          xAxis: [{
            axisLine: {
              show: false
            },
            splitLine: {
              show: false
            }
          }
          ],
          yAxis: [{
            axisLine: {
              show: false
            },
            splitLine: {
              show: false
            }
          }
          ],
          series: [{
            name: 'Package ',
            type: 'pie',
            radius: '75%',
            center: ['50%', '50%'],
            data: data.data,
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
          ]
        };
      })
      .catch(error => { 
        console.log(error);
      });
  }
  private async getChartData() {
    this.waiting = true;
    await this.chartService
      .chartHmoEnrolleeData()
      .then(data => {
        const labels: string[] = (<any>data.data).packageLabel;
        if (labels.length > 0) {
          this.formatData(labels);
        }
        this.doughnutChartData = (<any>data.data).noOfEnrollees;
        this.waiting = false;
      })
      .catch(error => {
        this.error.Message = error.error.message;
        this.error.isError = true;
        console.log(error);
      });
  }

  private async getChartEnrolleeVisitData(item) {
    this.waiting = true;
    await this.chartService
      .chartHmoEnrolleeVisitData(item)
      .then(data => {
        const labels: string[] = (<any>data.data).lastSevenDaysLabel;
        if (labels.length > 0) {
          this.formatDataVisit(labels);
        }
        this.lineChartData = (<any>data.data).data;

    this.chartLineOption3 = {
...echartStyles.lineNoAxis, ...{
          series: [{
            data: (<any>data.data).data,
            lineStyle: {
              color: 'rgba(5, 56, 118, .86)',
              width: 3,
              shadowColor: 'rgba(0, 0, 0, .2)',
              shadowOffsetX: -1,
              shadowOffsetY: 8,
              shadowBlur: 10
            },
            label: { show: true, color: '#212121' },
            type: 'line',
            smooth: true,
            itemStyle: {
              borderColor: 'rgba(7, 102, 216, 0.86)'
            }
          }]
        }
      };
      this.chartLineOption3.xAxis.data = this.lineChartLabels;
        this.waiting = false;
      })
      .catch(error => { 
        console.log(error);
      });
  }

  private formatData(data: string[]) {
    return data.forEach(element => {
      this.doughnutChartLabels.push(element);
    });
  }

  private formatDataVisit(data: string[]) {
    const pipe = new DatePipe('en-US');
    return data.forEach(element => {
      const formatedDate = pipe.transform(element, 'mediumDate');
      this.lineChartLabels.push(formatedDate);
    });
  }


  private chartData() {
    this.chartLineOption1 = {
      ...echartStyles.lineFullWidth, ...{
        series: [{
          data: [30, 40, 20, 50, 40, 80, 90],
          ...echartStyles.smoothLine,
          markArea: {
            label: {
              show: true
            }
          },
          areaStyle: {
            color: 'rgba(102, 51, 153, .2)',
            origin: 'start'
          },
          lineStyle: {
            color: '#663399',
          },
          itemStyle: {
            color: '#663399'
          }
        }]
      }
    };
    this.chartLineOption2 = {
      ...echartStyles.lineFullWidth, ...{
        series: [{
          data: [30, 10, 40, 10, 40, 20, 90],
          ...echartStyles.smoothLine,
          markArea: {
            label: {
              show: true
            }
          },
          areaStyle: {
            color: 'rgba(255, 193, 7, 0.2)',
            origin: 'start'
          },
          lineStyle: {
            color: '#FFC107'
          },
          itemStyle: {
            color: '#FFC107'
          }
        }]
      }
    };
    this.chartLineOption2.xAxis.data = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    this.chartLineOption3 = {
      ...echartStyles.lineNoAxis, ...{
        series: [{
          data: [40, 80, 20, 90, 30, 80, 40, 90, 20, 80, 30, 45, 50, 110, 90, 145, 120, 135, 120, 140],
          lineStyle: {
            color: 'rgba(102, 51, 153, 0.86)',
            width: 3,
            ...echartStyles.lineShadow
          },
          label: { show: true, color: '#212121' },
          type: 'line',
          smooth: true,
          itemStyle: {
            borderColor: 'rgba(102, 51, 153, 1)'
          }
        }]
      }
    };
     this.chartLineOption3.xAxis.data = ['1', '2', '3', 'Thu', 'Fri', 'Sat', 'Sun'];
    this.salesChartBar = {
      legend: {
        borderRadius: 0,
        orient: 'horizontal',
        x: 'right',
        data: ['Online', 'Offline']
      },
      grid: {
        left: '8px',
        right: '8px',
        bottom: '0',
        containLabel: true
      },
      tooltip: {
        show: true,
        backgroundColor: 'rgba(0, 0, 0, .8)'
      },
      xAxis: [{
        type: 'category',
        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
        axisTick: {
          alignWithLabel: true
        },
        splitLine: {
          show: false
        },
        axisLine: {
          show: true
        }
      }],
      yAxis: [{
        type: 'value',
        axisLabel: {
          formatter: '${value}'
        },
        min: 0,
        max: 100000,
        interval: 25000,
        axisLine: {
          show: false
        },
        splitLine: {
          show: true,
          interval: 'auto'
        }
      }
      ],
      series: [{
        name: 'Online',
        data: [35000, 69000, 22500, 60000, 50000, 50000, 30000, 80000, 70000, 60000, 20000, 30005],
        label: { show: false, color: '#0168c1' },
        type: 'bar',
        barGap: 0,
        color: '#bcbbdd',
        smooth: true,
      },
      {
        name: 'Offline',
        data: [45000, 82000, 35000, 93000, 71000, 89000, 49000, 91000, 80200, 86000, 35000, 40050],
        label: { show: false, color: '#639' },
        type: 'bar',
        color: '#7569b3',
        smooth: true
      }
      ]
    };
    this.salesChartPie = {
      color: ['#62549c', '#7566b5', '#7d6cbb', '#8877bd', '#9181bd', '#6957af'],
      tooltip: {
        show: true,
        backgroundColor: 'rgba(0, 0, 0, .8)'
      },
      xAxis: [{
        axisLine: {
          show: false
        },
        splitLine: {
          show: false
        }
      }
      ],
      yAxis: [{
        axisLine: {
          show: false
        },
        splitLine: {
          show: false
        }
      }
      ],
      series: [{
        name: 'Sales by Country',
        type: 'pie',
        radius: '75%',
        center: ['50%', '50%'],
        data: [
          { value: 535, name: 'USA' },
          { value: 310, name: 'Brazil' },
          { value: 234, name: 'France' },
          { value: 155, name: 'Germany' },
          { value: 130, name: 'UK' },
          { value: 348, name: 'India' }
        ],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: 'rgba(0, 0, 0, 0.5)'
          }
        }
      }
      ]
    };
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }
}
