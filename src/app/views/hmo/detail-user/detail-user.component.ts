import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { Router } from '@angular/router';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { isUndefined } from 'util';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';
import {Location} from '@angular/common';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent extends BaseComponent implements OnInit {

  providerData: any;
  hmos: any;
  orgs: any;
  searchText: any;
  userRole: any;
  constructor(
    private providerService: ProviderService, private auth : AuthService,
   router: Router, location: Location) {
    super(location, router, null, null, providerService);
    this.userRole = this.auth.store.getData('role');
  }

  ngOnInit() {
  }

}
