import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { notifications } from 'src/app/shared/constant';
import { IPackage } from 'src/app/shared/common/model/ISetting';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {

  private modalRef: NgbModalRef;
  constructor(
    private modalService: NgbModal, private hmoService: HmoService, router: Router) {
      super(null, router, null, null, hmoService);
  }
  ngOnInit() {
    this.getPackages();
  }

  getPackages() {
    this.waiting = true;
    this.hmoService.getPackages().subscribe((res) => {
      this.items = res;
      this.waiting = false;
    }, error => {
      this.waiting = false;
      this.hmoService.errorAlert(error, notifications.error);
    });
  }

  updatePack(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      const packs: IPackage = f.value;
      packs.packageId = this.item.packageId;
     // console.log(packs);
      this.hmoService.updatePackage(packs).subscribe((res) => {
       this.waiting = false;
       this.closeModal();
        this.hmoService.successAlert('Package updated successfully', notifications.success);
      }, error => {
        this.waiting = false;
        this.closeModal();
        this.hmoService.errorAlert(error, notifications.error);
      });
    }
  }

  open(content, item: IPackage) {
    this.item = item;
    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title', size: 'lg'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }

  closeModal() {
    this.modalRef.close();
  }
}
