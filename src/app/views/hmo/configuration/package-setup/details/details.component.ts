import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { IPackage } from 'src/app/shared/common/model/ISetting';
import { notifications } from 'src/app/shared/constant';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent  implements OnInit {
 id: any;
 package: IPackage;
 loading: boolean;
  constructor(private activatedRoute: ActivatedRoute, private hmoService: HmoService) { 
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
    });
  }

  ngOnInit() {
    this.getPackage();
  }

  getPackage() {
    this.loading = true;
    this.hmoService.detailPackage(this.id).subscribe(res => {
      this.package = res;
      this.loading = false;
    }, error => {
      this.loading = false;
      this.hmoService.errorAlert(error, notifications.error);
    });
  }

}
