import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HealthcareService } from 'src/app/shared/services/healthcare.service';
import { TariffService } from 'src/app/shared/services/tariff.service';
import { routes } from 'src/app/shared/constant';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tariff-category',
  templateUrl: './tariff-category.component.html',
  styleUrls: ['./tariff-category.component.scss']
})
export class TariffCategoryComponent extends BaseComponent implements OnInit {

  private modalRef: NgbModalRef;
  isEdit = false;
  constructor(private modalService: NgbModal,
    private tariffService: TariffService, router: Router) {
    super(null, router, null, null, tariffService);
  }


  ngOnInit() {
    this.init();
  }

  init() {
    this.url = `${routes.TARIFF.CATEGORY}/count`;
    this.query = {
      count: this.paginationConfig.count = 100,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPagination();
  }


  open(content, action: boolean) {
    this.isEdit = action;
  this.modalRef = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  this.modalRef.result.then((result) => {
    // console.log(result);
  }, (reason) => {
   // console.log('Err!', reason);
  });
}

submit(f: NgForm) {
  if (f.valid) {
      this.AddTariffcategory();
  } else {
    this.tariffService.warningAlert('The category field is required',  'Warning');
  }

}


private AddTariffcategory() {
  this.waiting = true;
  this.tariffService.createTariffCategory(this.item).subscribe(res => {
    this.waiting = false;
    this.item = {};
    this.tariffService.successAlert('Record created', 'Create');
    this.init();
  }, error => {
    this.waiting = false;
    this.tariffService.
      errorAlert(error, 'Error!!!');
  });
}
}
