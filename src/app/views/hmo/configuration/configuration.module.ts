import { ConfigurationComponent } from './configuration.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigurationRoutingModule } from './configuration-routing.module';
import { TariffComponent } from './tariff/tariff.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { TariffCategoryComponent } from './tariff-category/tariff-category.component';
import { HealthcareSelectionComponent } from './healthcare-selection/healthcare-selection.component';
import { CreateComponent } from './package-setup/create/create.component';
import { ListComponent } from './package-setup/list/list.component';
import { DetailsComponent } from './package-setup/details/details.component';
import { QuillModule } from 'ngx-quill';
 
const components = [
  ConfigurationComponent,
  TariffComponent,
  TariffCategoryComponent,
  HealthcareSelectionComponent,
  CreateComponent,
  ListComponent,
  DetailsComponent,
];

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbModule,
    QuillModule,
    SharedPipesModule,
    ConfigurationRoutingModule
  ],
  declarations: components
})
export class ConfigurationModule { }
