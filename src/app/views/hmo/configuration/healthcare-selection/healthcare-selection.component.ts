import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HealthcareService } from 'src/app/shared/services/healthcare.service';
import { routes, notifications } from 'src/app/shared/constant';
import { ProviderService } from 'src/app/shared/services/provider.service';
import { SelectHospitalService } from 'src/app/shared/services/select-hospital.service';
import { TariffService } from 'src/app/shared/services/tariff.service';
import { INameAndId } from 'src/app/shared/common/model/INameAndId';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-healthcare-selection',
  templateUrl: './healthcare-selection.component.html',
  styleUrls: ['./healthcare-selection.component.scss']
})
export class HealthcareSelectionComponent extends BaseComponent implements OnInit {
private modalRef: NgbModalRef;
  list: any;
  categories: INameAndId[];
  isAssign: boolean;
  itemDetail: any;
  constructor(private modalService: NgbModal,
     private providerService: ProviderService, private selectHospitalService: SelectHospitalService) {
    super(null, null, null, null, selectHospitalService);
  }

  ngOnInit() {
    this.init();
  }

  init() {
      this.url = `${routes.SELECTHOSPITAL.COUNT}`;
      this.query = {
        count: this.paginationConfig.count,
        page: this.paginationConfig.page,
        orderByExpression: JSON.stringify({direction: 1, column: 1}),
        whereCondition: this.filter
      };
      this.setupPaginations();
  }

  open(content, action = false, id = 0) {
    this.isAssign = action;
    this.item.selectedHospitalId = id;
    if (!this.isAssign) { this.getProviders(); } else { this.getCurrentTariff(id); this.getTariffCategories();  }
  this.modalRef  = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title'});
  this.modalRef.result.then((result) => {
   // console.log(result);
  }, (reason) => {
   // console.log('Err!', reason);
  });
}

getProviders() {
  this.providerService.getProviderNameAndId().subscribe(res  =>{
    this.checkItems = (<any>res).data;
  });
}

submit() {
  this.waiting = true;
  const selectedItems = this.getCheckedItemList();
  const body: Array<any> = [];
  selectedItems.forEach(item => {
   if (item.selected) {
     body.push({providerId: item.id, hmoId: 1});
   }
  });
  this.selectHospitalService.selectHospital(body).subscribe(res => {
    this.waiting = false;
    this.modalRef.close();
    this.reloadComponent();
    this.selectHospitalService.successAlert('Successfully Added', notifications.success);
  }, error => {
    this.waiting = false;
   this.selectHospitalService.errorAlert(error, notifications.error);
  });
}
assign(f: NgForm) {
   this.waiting = true;
   this.selectHospitalService.assignTariff(this.item).subscribe(res => {
     this.waiting = false;
     this.modalRef.close();
     this.reloadComponent();
    this.selectHospitalService.successAlert('Tariff assigned to hospital successfully', notifications.success);
   }, error => {
    this.waiting = false;
    this.selectHospitalService.errorAlert(error, notifications.error);
   });
}
remove(item, modal) {
  this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', centered: true })
            .result.then((result) => {
              this.waiting = true;
              this.selectHospitalService.removeHospital(item).subscribe(res => {
                this.waiting = false;
                this.reloadComponent();
                this.selectHospitalService.successAlert('You have successfully removed hospital from your coverage', notifications.success);
              });
              // this.init();
            }, (reason) => {
            });
}

private getTariffCategories() {
  this.selectHospitalService.
  getService(TariffService)
  .getNameAndId().subscribe(res => {
    this.categories = res;
  });
}

private getCurrentTariff(id) {
  this.selectHospitalService.getTariffCurrent(id).subscribe(res => {
    this.item = res.data;
  }, error => {
    this.error.isError = true;
    this.error.Message = error;
    // this.selectHospitalService.errorAlert(error, notifications.error);
  });
}

}
