
import { ConfigurationComponent } from './configuration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TariffComponent } from './tariff/tariff.component';
import { TariffCategoryComponent } from './tariff-category/tariff-category.component';
import { HealthcareSelectionComponent } from './healthcare-selection/healthcare-selection.component';
import { CreateComponent } from './package-setup/create/create.component';
import { ListComponent } from './package-setup/list/list.component';
import { DetailsComponent } from './package-setup/details/details.component';

const childRoute: Routes = [
  {path: 'tariff-category/:id/tariff', component: TariffComponent},
  {path: 'tariff-category', component: TariffCategoryComponent},
  {path: 'healthcare-selection', component: HealthcareSelectionComponent},
  {path: 'create', component: CreateComponent},
  {path: 'view', component: ListComponent},
  {path: 'view/:id', component: DetailsComponent},
];

const routes: Routes = [
  {
    path: '',
    component: ConfigurationComponent,
    children: [
      ...childRoute
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
