import { BASEURL, routes, notifications } from 'src/app/shared/constant';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { NgbModalRef, NgbModal, NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { TariffService } from 'src/app/shared/services/tariff.service';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Utils } from 'src/app/shared/utils';
import { isNullOrUndefined } from 'util';
import { INameAndId } from 'src/app/shared/common/model/INameAndId';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tariff',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.scss']
})
export class TariffComponent extends BaseComponent implements OnInit {
  private modalRef: NgbModalRef;
  isEdit = false;
  formData: FormData = new FormData();
  filename: string;
  fileExtensionError: boolean;
  fileExtensionMessage: string;
  catId: string;
  isDrug = false;
  isUpload = false;
  categories: INameAndId[];
  constructor(private modalService: NgbModal,
    private tariffService: TariffService, router: Router, activatedRoute: ActivatedRoute) {
    super(null, router, activatedRoute, null, tariffService);
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.catId = params.get('id');
    });
    this.filter.uid = this.catId;
    this.filter.category = '';
    this.init();
  }

  init() {
    this.url = `${routes.TARIFF.TARIFFCOUNT}`;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

  getDrugTariff() {
    this.filter.uid = this.catId;
      this.url = `${routes.TARIFF.DRUGCOUNT}`;
      this.query = {
        count: this.paginationConfig.count,
        page: this.paginationConfig.page,
        orderByExpression: JSON.stringify({direction: 1, column: 1}),
        whereCondition: this.filter
      };
      this.setupPaginations();
  }

   beforeChange($event: NgbTabChangeEvent) {
    if ($event.nextId === 'tab-drug') {
      this.paginationConfig.page = 1;
       this.getDrugTariff();
    } else if ($event.nextId === 'tab-service') {
      this.paginationConfig.page = 1;
      this.init();
    }
  }

  open(content, action: boolean, item = null) {
    this.item.tariffCategoryId = 0;
    this.isDrug = action;
    this.item = !isNullOrUndefined(item) ? item : {};
    this.isEdit = !isNullOrUndefined(item) ? true : false;
    this.getTariffCategories();
  this.modalRef = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  this.modalRef.result.then((result) => {
    // console.log(result);
  }, (reason) => {
   // console.log('Err!', reason);
  });
}


remove(modal, action: boolean, item) {
  this.isDrug = action;
  this.modalService.open(modal, { ariaLabelledBy: 'modal-basic-title', centered: true })
            .result.then((result) => {
               if (this.isDrug) {
                 this.deleteDrugTariff(item);
               } else {
                 this.deleteTariff(item);
               }
            }, (reason) => {
            });
}

download() {
  this.openInNewTab(`${routes.TARIFF.DOWNLOADTARIFFTEMPLATE}`,
   'popUpWindow',
  'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
   }

   downloadDrug() {
     this.openInNewTab(`${routes.TARIFF.DOWNLOADDRUGTARIFFTEMPLATE}`,
     'popUpWindow',
     'height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
   }

   async onFileChange(event) {
    const file = <File>event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, false, true)) {
      this.fileExtensionError = true;
      if (event.target.files.length > 0) {
         this.formData.append('file', file);
         this.formData.append('id', this.catId);
      }
    } else {
      this.fileExtensionMessage = 'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.tariffService.warningAlert(this.fileExtensionMessage);
    }

  }
  submit() {
    this.waiting = true;
    if (this.isDrug) {
         this.UploadTariffDrugList();
    } else {
      this.UploadTariffBulkList();
    }

  }
  create(f: NgForm) {
    this.waiting = true;
    if (this.isDrug) {
         this.createDrugTariff(f);
    } else {
      this.createTariff(f);
    }

  }
  update(f: NgForm) {
    this.waiting = true;
    if (this.isDrug) {
         this.updateDrugTariff(f);
    } else {
      this.updateTariff(f);
    }

  }
   private UploadTariffBulkList() {
    this.tariffService.uploadTarrifList(this.formData).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          break;
        case HttpEventType.Response:
          this.waiting = false;
          this.init();
          this.tariffService.successAlert('Uploaded successfully', 'Uploaded');
          this.clear();
          this.modalRef.close();
          // this.init();
          break;
        case 1: {
          this.waiting = true;
          /* if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
            this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
          } */
          break;
        }
      }
    }, error => {
      this.waiting = false;
      this.clear();
      this.modalRef.close();
      this.tariffService.
        errorAlert(error, 'Error Uploading File To Server');
    });
  }

  private UploadTariffDrugList() {
    this.tariffService.uploadDrugTarrifList(this.formData).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          break;
        case HttpEventType.Response:
          this.waiting = false;
          this.getDrugTariff();
          this.tariffService.successAlert('Uploaded successfully', 'Uploaded');
          this.clear();
          this.modalRef.close();
          // this.init();
          break;
        case 1: {
          this.waiting = true;
          /* if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
            this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
          } */
          break;
        }
      }
    }, error => {
      this.waiting = false;
      this.clear();
      this.modalRef.close();
      this.tariffService.
        errorAlert(error, 'Error Uploading File To Server');
    });
  }

  private getTariffCategories() {
    this.tariffService.getNameAndId().subscribe(res => {
      this.categories = res;
    });
  }
  private updateDrugTariff(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      const body = f.value;
      body.id = this.item.id;
     // console.log(packs);
      this.tariffService.updateDrugTariff(body).subscribe((res) => {
       this.waiting = false;
       this.getDrugTariff();
       this.closeModal();
        this.tariffService.successAlert('Record updated successfully', notifications.success);
      }, error => {
        this.waiting = false;
        this.closeModal();
        this.tariffService.errorAlert(error, notifications.error);
      });
    }
  }
  private updateTariff(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      const body = f.value;
      body.id = this.item.id;
     // console.log(packs);
      this.tariffService.updateTariffList(body).subscribe((res) => {
        this.init();
       this.waiting = false;
       this.closeModal();
        this.tariffService.successAlert('Record updated successfully', notifications.success);
      }, error => {
        this.waiting = false;
        this.closeModal();
        this.tariffService.errorAlert(error, notifications.error);
      });
    }
  }

  private createDrugTariff(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      const body = f.value;
      this.tariffService.createDrugTariff(body).subscribe((res) => {
        this.getDrugTariff();
       this.waiting = false;
       this.closeModal();
        this.tariffService.successAlert('Record updated successfully', notifications.success);
      }, error => {
        this.waiting = false;
        this.closeModal();
        this.tariffService.errorAlert(error, notifications.error);
      });
    }
  }
  private createTariff(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
      const body = f.value;
      this.tariffService.createTariff(body).subscribe((res) => {
        this.init();
       this.waiting = false;
       this.closeModal();
        this.tariffService.successAlert('Record updated successfully', notifications.success);
      }, error => {
        this.waiting = false;
        this.closeModal();
        this.tariffService.errorAlert(error, notifications.error);
      });
    }
  }

  private deleteTariff(item) {
    this.waiting = true;
    this.tariffService.removeTariff(item).subscribe(res => {
      this.waiting = false;
     this.init();
      this.tariffService.successAlert('Deleted successfully', notifications.success);
    });
  }

  private deleteDrugTariff(item) {
    this.waiting = true;
    this.tariffService.removeDrugTariff(item).subscribe(res => {
      this.waiting = false;
      this.getDrugTariff();
      this.tariffService.successAlert('Deleted successfully', notifications.success);
    });
  }
  clear() {
    this.formData.delete('file');
    this.formData.delete('id');
    this.filename = '';

  }
  closeModal() {
    this.modalRef.close();
  }
}
