import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { PaymentService } from 'src/app/shared/services/payment.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { isUndefined } from 'util';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-payment-report',
  templateUrl: './payment-report.component.html',
  styleUrls: ['./payment-report.component.scss']
})
export class PaymentReportComponent extends BaseComponent implements OnInit {
  totalCount: number;
  userRole: any;
  totalPaid: number;

  constructor(private paymentService: PaymentService, private auth: AuthService) {
    super(null, null, null, null, paymentService);
    this.userRole = this.auth.store.getData('role');
  }

  ngOnInit() {
    this.init();
  }

  init(event?) {
    if (isUndefined(event)) {
      this.filter = {};
   } else {
     this.filter = event;
   }
    this.waiting = true;
    this.url = routes.PAYMENT.PAYMENTLIST;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.genPagination().subscribe((res) => {
      this.totalCount = res.data.total;
      this.paymentService.getService(EmitService).checkTotalCount(res.data.total);
      this.items = res.data.items;
      this.getSumTotal(this.items);
      this.waiting = false;
   });
  }

  refresh(event) {
    this.reloadComponent();
  }
  getSumTotal(items) {
    this.totalPaid = this.getSum('amount', items);
  
  }
}
