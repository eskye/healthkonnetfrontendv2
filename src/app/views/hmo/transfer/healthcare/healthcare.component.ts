import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { CompanyService } from "src/app/shared/services/company.service";
import { FileManagerService } from "src/app/shared/services/file-manager.service";
const transactions = [
  {
    ref_code: "43269-860",
    desc: "felis sed lacus morbi",
    merchant: "SJ Creations, Inc.",
    amount: "€7,51",
    date: "2018-07-13 16:38:11"
  },
  {
    ref_code: "68151-2985",
    desc: "in sagittis",
    merchant: "Carilion Materials Management",
    amount: "¥5.02",
    date: "2019-04-11 20:16:06"
  },
  {
    ref_code: "76014-001",
    desc: "erat quisque erat eros",
    merchant: "Eclat Pharmaceuticals, LLC",
    amount: "£6.52",
    date: "2018-11-13 20:51:03"
  },
  {
    ref_code: "64735-050",
    desc: "congue eget",
    merchant: "Air Liquide Healthcare America Corporation",
    amount: "£7.57",
    date: "2018-06-13 11:27:44"
  },
  {
    ref_code: "0220-9054",
    desc: "est phasellus sit",
    merchant: "Laboratoires Boiron",
    amount: "€3,42",
    date: "2019-02-14 05:22:50"
  },
  {
    ref_code: "51655-391",
    desc: "mauris lacinia",
    merchant: "Northwind Pharmaceuticals, LLC",
    amount: "¥5.91",
    date: "2019-04-24 14:58:46"
  },
  {
    ref_code: "0573-1915",
    desc: "nulla tellus",
    merchant: "Pfizer Consumer Healthcare",
    amount: "€2,27",
    date: "2019-04-12 20:07:22"
  },
  {
    ref_code: "0372-0043",
    desc: "ultrices posuere",
    merchant: "SCOT-TUSSIN Pharmacal Co., Inc.",
    amount: "€6,63",
    date: "2019-02-09 15:14:20"
  }
];
@Component({
  selector: "app-healthcare",
  templateUrl: "./healthcare.component.html",
  styleUrls: ["./healthcare.component.scss"]
})
export class HealthcareComponent implements OnInit {
  transactions = transactions;
  private modalRef: NgbModalRef;
  constructor(
    private modalService: NgbModal,
    private companyService: CompanyService,
    private fileManagerService: FileManagerService
  ) {}

  ngOnInit() {}
  open(content) {
    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }
}
