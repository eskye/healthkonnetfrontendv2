import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { CompanyService } from "src/app/shared/services/company.service";
import { FileManagerService } from "src/app/shared/services/file-manager.service";

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit {
  private modalRef: NgbModalRef;
  constructor(
    private modalService: NgbModal,
    private companyService: CompanyService,
    private fileManagerService: FileManagerService
  ) {}

  ngOnInit() {}
  open(content) {
    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }
  opensm(content) {
    this.modalRef = this.modalService.open(content, {
      size: 'sm'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }
}
