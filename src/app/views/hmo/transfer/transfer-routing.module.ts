import { TransferComponent } from './transfer.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HealthcareComponent } from './healthcare/healthcare.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { PaymentComponent } from './payment/payment.component';

const childRoute: Routes = [
  {path: 'healthcare-provider', component: HealthcareComponent},
  {path: 'withdraw', component: WithdrawComponent},
  {path: 'payment', component: PaymentComponent},
];

const routes: Routes = [
  {
    path: '',
    component: TransferComponent,
    children: [
      ...childRoute
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class TransferRoutingModule { }
