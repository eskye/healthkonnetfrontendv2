import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-payment-report-detail',
  templateUrl: './payment-report-detail.component.html',
  styleUrls: ['./payment-report-detail.component.scss']
})
export class PaymentReportDetailComponent extends BaseComponent implements OnInit {

  id: string;
  constructor(location: Location, activatedRoute: ActivatedRoute, router: Router) {
    super(location, router, activatedRoute, null, null);
  }

  ngOnInit() {
    this.id = this.getParamValue('id');
    console.log(this.id);
  }

}
