import { TransferComponent } from './transfer.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransferRoutingModule } from './transfer-routing.module'
import { HealthcareComponent } from './healthcare/healthcare.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { PaymentComponent } from './payment/payment.component';

const components = [
  TransferComponent,
  WithdrawComponent,
  HealthcareComponent,
  PaymentComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgbModule,
    SharedPipesModule,
    TransferRoutingModule
  ],
  declarations: components
})
export class TransferModule { }
