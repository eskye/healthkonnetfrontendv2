import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HmoRoutingModule } from './hmo-routing.module';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { AvatarModule } from 'ngx-avatar';

import { OwlModule } from 'ngx-owl-carousel';

import { DashboardComponent } from './dashboard/dashboard.component';
import { OrgComponent } from './organization/org.component';
import { ReqComponent } from './req/req.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { TreatmentRequestComponent } from './treatment/request/request.component';
import { LogsComponent } from './claims/logs/logs.component';
import { DetailComponent } from './claims/detail/detail.component';
import { NgxEditorModule } from 'ngx-editor';
import { QuillModule } from 'ngx-quill';
 import { ListComponent } from './organization/list/list.component';
import { MainViewComponent } from './organization/main-view/main-view.component';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { ManageUserDetailComponent } from './manage-user-detail/manage-user-detail.component';
import { PaymentReportComponent } from './transfer/payment-report/payment-report.component';
import { DetailUserComponent } from './detail-user/detail-user.component';
import { PaymentReportDetailComponent } from './transfer/payment-report-detail/payment-report-detail.component';

const components = [
  DashboardComponent,
  OrgComponent,
  ReqComponent,
  TreatmentRequestComponent,
  LogsComponent,
  DetailComponent,
  ListComponent,
  MainViewComponent,
  ManageUserComponent,
  ManageUserDetailComponent,
  PaymentReportComponent,
  DetailUserComponent,
  PaymentReportDetailComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxEchartsModule,
    // Specify AvatarModule as an import
    AvatarModule,
    NgxEditorModule,
    QuillModule,
    NgbModule,
    SharedPipesModule,
    HmoRoutingModule,
    OwlModule
  ],
  declarations: components
})
export class HmoModule {}
