import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { AnnouncementsComponent } from './announcements/announcements.component';
import { BenefitsComponent } from './benefits.component';

import { CreatehealthtipsComponent } from './createhealthtips/createhealthtips.component';
import { ListhealthtipsComponent } from './listhealthtips/listhealthtips.component'; 
import { HealthTipDetailComponent } from './health-tip-detail/health-tip-detail.component';


const routes: Routes = [
  {path: '', component: BenefitsComponent,
  children: [
    {path: 'tips/create', component: CreatehealthtipsComponent},
    {path: 'tips/list', component: ListhealthtipsComponent},
    {path: 'tips/:id/:slug', component: HealthTipDetailComponent},
    {path: 'announcement', component: AnnouncementsComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BenefitsRoutingModule { }
