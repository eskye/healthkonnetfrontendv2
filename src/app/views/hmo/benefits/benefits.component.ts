import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from 'src/app/shared/services/company.service';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.scss']
})
export class BenefitsComponent extends BaseComponent implements OnInit {

  constructor() {
  	super(null, null, null, null)
  }
  ngOnInit() {
  }

}
