import { Component, OnInit } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from 'src/app/shared/services/company.service';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { PostService } from 'src/app/shared/services/post.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Title } from '@angular/platform-browser';
import { routes } from 'src/app/shared/constant';
import { Router } from '@angular/router';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-listhealthtips',
  templateUrl: './listhealthtips.component.html',
  styleUrls: ['./listhealthtips.component.scss']
})
export class ListhealthtipsComponent extends BaseComponent implements OnInit {

  private modalRef: NgbModalRef;
  constructor(
    private modalService: NgbModal, private postService: PostService,
    titleService: Title, router: Router) {
    super(null, router, null, titleService, postService);
  }
  ngOnInit() {
    this.init();
  }

  init() {
    this.url = routes.POST.COUNT;
  this.query = {
    count: this.paginationConfig.count,
    page: this.paginationConfig.page,
    orderByExpression: JSON.stringify({direction: 1, column: 1}),
    whereCondition: this.filter
  };
  this.genPagination().subscribe((res) => {
    // this.totalCount = res.data.total;
    this.postService.getService(EmitService).checkTotalCount(res.data.total);
    this.items = res.data.items;
    // this.getSumTotal(this.items);
    this.waiting = false;
 });
  }
  open(content) {
    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }
  openLg(content) {
    this.modalRef = this.modalService.open(content, {
      size: 'lg'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }
}