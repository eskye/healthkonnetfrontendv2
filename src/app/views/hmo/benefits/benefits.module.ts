import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BenefitsComponent } from './benefits.component'; 
import { CustomFormsModule } from 'ng2-validation';
import { TagInputModule } from 'ngx-chips';

import { BenefitsRoutingModule } from './benefits-routing.module';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { AnnouncementsComponent } from './announcements/announcements.component';
import { CreatehealthtipsComponent } from './createhealthtips/createhealthtips.component';
import { ListhealthtipsComponent } from './listhealthtips/listhealthtips.component';
import { HealthTipDetailComponent } from './health-tip-detail/health-tip-detail.component'; 

@NgModule({
  imports: [
    CommonModule,
    CustomFormsModule,
    SharedComponentsModule,
    FormsModule,
    QuillModule,
    NgbModule,
    SharedPipesModule,
    BenefitsRoutingModule,
    TagInputModule,
    ReactiveFormsModule
  ],

  declarations: [BenefitsComponent,
     AnnouncementsComponent,
     CreatehealthtipsComponent,
     ListhealthtipsComponent,
     HealthTipDetailComponent
    ]

})
export class BenefitsModule { }
