import { IPackage } from './../../../../shared/common/model/ISetting';
import { Component, OnInit } from '@angular/core';
 
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { NgForm } from '@angular/forms';
import { notifications } from 'src/app/shared/constant';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.scss']
})
export class AnnouncementsComponent extends BaseComponent implements OnInit {
  htmlContent: any;
  constructor(private hmoService: HmoService) {
    super(null, null, null, null, hmoService);
  }
 
  ngOnInit() {
  }

  createPack(f: NgForm) {
    if (f.valid) {
       this.waiting = true;
      const packs: IPackage = f.value;
    // console.log(packs);
      this.hmoService.createPackage(packs).subscribe((res) => {
       // this.items.push(res.data);
       this.waiting = false;
        this.reloadComponent();
        f.reset();
        this.hmoService.successAlert('Package Created', notifications.success);
      }, error => {
         this.waiting = false;
         this.hmoService.errorAlert(error, notifications.error);
      });
    }
  }


}
