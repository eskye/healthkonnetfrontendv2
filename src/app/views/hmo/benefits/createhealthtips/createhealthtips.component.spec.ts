import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatehealthtipsComponent } from './createhealthtips.component';

describe('CreatehealthtipsComponent', () => {
  let component: CreatehealthtipsComponent;
  let fixture: ComponentFixture<CreatehealthtipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatehealthtipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatehealthtipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
