import { IPackage } from './../../../../shared/common/model/ISetting';
import { Component, OnInit, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
 
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { NgForm, FormControl } from '@angular/forms';
import { notifications, routes } from 'src/app/shared/constant';
import { PostService } from 'src/app/shared/services/post.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { Utils } from 'src/app/shared/utils';

@Component({
  selector: 'app-createhealthtips',
  templateUrl: './createhealthtips.component.html',
  styleUrls: ['./createhealthtips.component.scss']
})
export class CreatehealthtipsComponent extends BaseComponent implements OnInit {

  htmlContent: any;
  filename: any;
  fileExtensionError: boolean;
  isImage: boolean;
  public imagePath;
  imgURL: any;
  public message: string;
  preview(files) {
  }
  items: [];
  fileExtensionMessage: string;
  formData = new FormData();
  tagsCtrl1 = new FormControl(this.items);

  constructor(private hmoService: HmoService, private postService: PostService,
     private fileManagerService: FileManagerService, private el: ElementRef, private renderer: Renderer2) {
    super(null, null, null, null, hmoService);
  }

  ngOnInit() {
    this.getNameAndId(routes.POST.CATEGORY + '/getNameAndIds');
  }


  createPack(f: NgForm) {
    if (f.valid) {
       this.waiting = true;
      const packs: IPackage = f.value;
      this.hmoService.createPackage(packs).subscribe((res) => {
       this.waiting = false;
        this.reloadComponent();
        f.reset();
        this.hmoService.successAlert('Package Created', notifications.success);
      }, error => {
         this.waiting = false;
         this.hmoService.errorAlert(error, notifications.error);
      });
    }
  }

  createPost(f: NgForm) {
    if (f.valid) {
      const value = f.value;
      if (value.isSavedAsDraft) {
        value.isPublished = false;
      } else {
        value.isPublished = true;
      }
      value.id = this.item.id;
      // console.log(value);
      this.waiting = true;
      this.postService.createPost(value).subscribe(res => {
        this.postService.successAlert('Your post have been published successfully', 'Published');
        this.waiting = false;
      }, error => {
        this.waiting = false;
        this.postService.errorAlert(error);
      });
    }
  }
  openFileBrowser(){
    (<any>document.getElementById('inputGroupFileBrowse2')).click();
  }
  async onFileChange(event) {
    const file = event.target.files[0];
    this.filename = file.name;
    const reader = new FileReader();
    if (event.target.files.length === 0) {
      return;
    }
 
    const mimeType = file.type;
    if (mimeType.match(/image\/*/) == null) {
      this.fileExtensionMessage =
        'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
      return;
    }
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
 
    if (Utils.fileValidator(file, true)) {
      this.isImage = true;
      this.fileExtensionError = true;
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          (<any>document.getElementById('imageAppend')).src =
            reader.result;
        };
      }
      if (event.target.files.length > 0) {
        this.formData.append('File', file);
      }
    } else {
      this.fileExtensionMessage =
        'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
    }
  }

  submitTips() {
    this.MakeUploadRequest(this.formData);
  }

  private MakeUploadRequest(formData: FormData) {
    this.fileManagerService.uploadImage(formData).subscribe(
      (event: HttpEvent<any>) => {
        // console.log(event);
        switch (event.type) {
          case HttpEventType.Sent:
            break;
          case HttpEventType.Response:
            this.isUploading = false;
            this.waiting = true;
            this.item.thumbnail = (<any>event).body.data.result;
            this.postService.createPost(this.item).subscribe(res => {
              this.postService.successAlert('Your post have been published successfully', 'Published');
              this.waiting = false;
            }, error => {
              this.waiting = false;
              this.postService.errorAlert(error);
            });
            // this.fileManagerService.successAlert((<any>event).body.data.filename);
            // this.msg = (<any>event).body.data.error;
            break;
          case 1:
            {
              this.isUploading = true;
              // if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
              //   this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
            }
            break;
        }
      }, error => {
        this.isUploading = false;
        this.fileManagerService.errorAlert(
          'An error occurred while uploading logo to the server, try again',
          'Error Uploading File To Server'
        );
      }
    );
  }
  public onSelect(item) {
    console.log('tag selected: value is ' + item);
  }

}
