import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentRequestComponent } from './request.component';

describe('TreatmentRequestComponent', () => {
  let component: TreatmentRequestComponent;
  let fixture: ComponentFixture<TreatmentRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
