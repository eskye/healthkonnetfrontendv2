import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';

const filtertype = ['All', 'Awaiting Approval', 'Approved', 'Declined'];
const treatment = [
  {
    image:
      'https://robohash.org/liberovoluptatibusquis.bmp?size=50x50&set=set1',
    name: 'Eli Lilly and Company',
    message:
      'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.',
    status: 'pending',
    date: '2018-12-28 11:49:49'
  },
  {
    image: 'https://robohash.org/saepecumsed.jpg?size=50x50&set=set1',
    name: 'Western Family Foods, Inc.',
    message: 'Aliquam non mauris. Morbi non lectus.',
    status: 'pending',
    date: '2018-12-29 03:25:41'
  },
  {
    image: 'https://robohash.org/voluptatemanimiipsam.jpg?size=50x50&set=set1',
    name: 'Hyland',
    message: 'Duis at velit eu est congue elementum.',
    status: 'declined',
    date: '2018-07-19 11:06:54'
  },
  {
    image: 'https://robohash.org/istequitotam.png?size=50x50&set=set1',
    name: 'Lake Erie Medical DBA Quality Care Products LLC',
    message: 'Praesent lectus.',
    status: 'declined',
    date: '2019-05-06 11:30:33'
  },
  {
    image: 'https://robohash.org/odioexplicabout.jpg?size=50x50&set=set1',
    name: 'Conopco Inc. d/b/a Unilever',
    message: 'Etiam justo. Etiam pretium iaculis justo.',
    status: 'approved',
    date: '2019-04-28 17:40:38'
  },
  {
    image: 'https://robohash.org/involuptatumsit.bmp?size=50x50&set=set1',
    name: 'Neutrogena Corporation',
    message: 'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.',
    status: 'pending',
    date: '2019-03-26 19:58:44'
  },
  {
    image: 'https://robohash.org/exdistinctiout.png?size=50x50&set=set1',
    name: 'Aidance Skincare & Topical Solutions, LLC',
    message:
      'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primi',
    status: 'approved',
    date: '2019-02-27 03:44:09'
  }
];
@Component({
  selector: 'app-treatment',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class TreatmentRequestComponent extends BaseComponent implements OnInit {
  status = filtertype;
  Treatment = treatment;
  constructor() {
    super(null, null, null, null);
  }

  ngOnInit() {}
}
