import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-manage-user-detail',
  templateUrl: './manage-user-detail.component.html',
  styleUrls: ['./manage-user-detail.component.scss']
})
export class ManageUserDetailComponent extends BaseComponent implements OnInit {
  id: string;
  constructor(location: Location, activatedRoute: ActivatedRoute, router: Router) {
    super(location, router, activatedRoute, null, null);
  }

  ngOnInit() {
    this.id = this.getParamValue('id');
   // console.log(this.id);
  }

}
