import { OrgComponent } from './organization/org.component';
import { ReqComponent } from './req/req.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TreatmentRequestComponent } from './treatment/request/request.component';
import { DetailComponent } from './claims/detail/detail.component';
import { MainViewComponent } from './organization/main-view/main-view.component';
import { ListComponent } from './organization/list/list.component';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { ManageUserDetailComponent } from './manage-user-detail/manage-user-detail.component';
import { PaymentReportComponent } from './transfer/payment-report/payment-report.component';
import { DetailUserComponent } from './detail-user/detail-user.component';
import { PaymentReportDetailComponent } from './transfer/payment-report-detail/payment-report-detail.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'clients',
    component: MainViewComponent,
    children: [
      {path: '', component: OrgComponent},
      {path: ':id/staffs', component: ListComponent}
    ]
  },
  {
    path: 'user',
    component: MainViewComponent,
    children: [
      {path: 'enrollee', component: ManageUserComponent},
      {path: 'enrollee/:id/detail', component: ManageUserDetailComponent},
      {path: 'enrollee/details', component: DetailUserComponent},
      // {path: ':id/staffs', component: ListComponent}
    ]
  },
  {
    path: 'transactions/index',
    component: PaymentReportComponent
  },
  {
    path: 'transactions/:id/detail',
    component: PaymentReportDetailComponent
  },
  {
    path: 'provider-requests',
    component: ReqComponent
  },
  {
    path: 'treatment/request',
    component: TreatmentRequestComponent
  },
  {
    path: 'claims',
    loadChildren: './claim-hmo/claim-hmo.module#ClaimHmoModule'
  },
  {
    path: 'claims/:id/detail',
    component: DetailComponent
  },
  {
    path: 'benefits',
    loadChildren: './benefits/benefits.module#BenefitsModule'
  },
  {
    path: 'configuration',
    loadChildren: './configuration/configuration.module#ConfigurationModule'
  },
  {
    path: 'transfer',
    loadChildren: './transfer/transfer.module#TransferModule'
  },
  {
    path: 'settings',
    loadChildren: './settings/setting.module#SettingModule'
  },
  {
    path: 'media',
    loadChildren: './Media/media.module#MediaModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HmoRoutingModule {}
