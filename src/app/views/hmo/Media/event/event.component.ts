import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
@Component({
  selector: "app-event",
  templateUrl: "./event.component.html",
  styleUrls: ["./event.component.scss"]
})

export class EventComponent extends BaseComponent implements OnInit {
  constructor(private modalService: NgbModal) {
    super(null, null, null, null)
  }

  ngOnInit() {}

  open(content) {
    this.modalService.open(content, {
      ariaLabelledBy: "modal-basic-title",
      centered: true
    });
  }
}
