import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventFormComponent } from './event-form/event-form.component';
import { MediaComponent } from './media.component';
import { EventComponent } from './event/event.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';


const routes: Routes = [
  {path: '', component: MediaComponent,
  children: [
    {path: 'event/create', component: EventFormComponent},
    {path: 'event/view', component: EventComponent}, 
    {path: 'event/:id/:slug', component: EventDetailComponent},
    {path: 'event/list', component: EventListComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MediaRoutingModule { }
