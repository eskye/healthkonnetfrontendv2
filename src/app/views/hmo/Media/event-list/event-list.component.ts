import { EmitService } from './../../../../shared/services/emit.service';
import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/shared/services/event.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { routes } from 'src/app/shared/constant';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent extends BaseComponent implements OnInit {

  constructor(private eventService: EventService,
    titleService: Title, router: Router) {
    super(null, router, null, titleService, eventService);
  }
  ngOnInit() {
    this.init();
  }

  init() {
    this.url = routes.EVENT.COUNT;
  this.query = {
    count: this.paginationConfig.count,
    page: this.paginationConfig.page,
    orderByExpression: JSON.stringify({direction: 1, column: 1}),
    whereCondition: this.filter
  };
  this.genPagination().subscribe((res) => {
    // this.totalCount = res.data.total;
    this.eventService.getService(EmitService).checkTotalCount(res.data.total);
    this.items = res.data.items;
    // this.getSumTotal(this.items);
    this.waiting = false;
 });
  }

}
