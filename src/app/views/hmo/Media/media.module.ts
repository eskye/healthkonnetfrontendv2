import { MediaComponent } from './media.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module'; 
import { EventComponent } from './event/event.component';
import { EventFormComponent } from './event-form/event-form.component';
import { MediaRoutingModule } from './media-routing.module';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
 
@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    QuillModule,
    NgbModule,
    SharedPipesModule,
    MediaRoutingModule
  ],

  declarations: [
     EventComponent,
     EventFormComponent,
     MediaComponent,
     EventListComponent,
     EventDetailComponent
    ]


})
export class MediaModule { }
