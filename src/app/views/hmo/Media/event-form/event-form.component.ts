import { Component, OnInit } from '@angular/core'; 
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { routes } from 'src/app/shared/constant';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { Utils } from 'src/app/shared/utils';
import { Title } from '@angular/platform-browser';
import { EventService } from 'src/app/shared/services/event.service';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent extends BaseComponent implements OnInit {

  htmlContent: any;
  filename: any;
  fileExtensionError: boolean;
  isImage: boolean;
  fileExtensionMessage: string;
  formData = new FormData();
  constructor(titleService: Title, private hmoService: HmoService, private eventService: EventService,
     private fileManagerService: FileManagerService) {
    super(null, null, null, titleService, eventService);
  }

  ngOnInit() {
     this.titleRoute('Create an event | Health Konnet');
    this.getNameAndId(routes.POST.CATEGORY + '/getNameAndIds');
  }

  async onFileChange(event) {
    const file = event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, true)) {
      this.isImage = true;
      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          (<any>document.getElementById('Company_Logo_src')).src =
            reader.result;
        };
      }
      if (event.target.files.length > 0) {
        this.formData.append('File', file);
      }
    } else {
      this.fileExtensionMessage =
        'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
    }
  }

  submitEvent() {
    this.waiting = true;
    this.MakeUploadRequest(this.formData);
  }

  private MakeUploadRequest(formData: FormData) {
    this.fileManagerService.uploadToCloud(formData, routes.FILEMANAGER.UPLOADEVENTIMAGE).subscribe(
      (event: HttpEvent<any>) => {
        // console.log(event);
        switch (event.type) {
          case HttpEventType.Sent:
            break;
          case HttpEventType.Response:
            this.isUploading = false;
            this.item.eventFlyer = (<any>event).body.data.filename;
            this.eventService.create(this.item).subscribe(res => {
              this.eventService.successAlert('Your event has been published successfully', 'Published');
              this.waiting = false;
            }, error => {
              this.waiting = false;
              this.eventService.errorAlert(error);
            });
            // this.fileManagerService.successAlert((<any>event).body.data.filename);
            // this.msg = (<any>event).body.data.error;
            break;
          case 1:
            {
              this.isUploading = true;
              // if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
              //   this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
            }
            break;
        }
      }, error => {
        this.isUploading = false;
        this.fileManagerService.errorAlert(error,
          'Error Uploading File To Server'
        );
      }
    );
  }

  toggleCheck(e) {
    console.log(e);
    this.item.isSentToEnrollees = e.target.checked;
  }

}
