import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingRoutingModule } from './setting-routing.module';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import { PackageComponent } from './package/package.component';
import { ProfileComponent } from './profile/profile.component';
import { BankComponent } from './bank/bank.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';

const components = [
  SettingsViewComponent,
  BankComponent,
  PackageComponent,
  ProfileComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SharedPipesModule,
    SettingRoutingModule
  ],
  declarations: components
})
export class SettingModule { }
