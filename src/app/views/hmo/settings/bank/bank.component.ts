import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { PaymentService } from 'src/app/shared/services/payment.service';
import { notifications } from 'src/app/shared/constant';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent extends BaseComponent implements OnInit {

  isSaving = false;
  constructor(private paymentService: PaymentService) {
    super(null, null, null, null, paymentService);
  }

  ngOnInit() {
  }

  saveBankDetails(event) {
    this.isSaving = true;
   this.paymentService.CreateBankDetails(event).subscribe(res => {
    this.isSaving = false;
    this.paymentService.successAlert('Account details updated successfully', notifications.success);
   }, error => {
    this.isSaving = false;
     this.paymentService.errorAlert('An error occured while updating your bank account details', notifications.error);
   });
  }
}
