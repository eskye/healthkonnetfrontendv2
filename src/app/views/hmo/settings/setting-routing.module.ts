import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import { BankComponent } from './bank/bank.component';
import { ProfileComponent } from './profile/profile.component';
const childRoute: Routes = [
  {
    path: 'bank',
    component: BankComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },

];

const routes: Routes = [
  {
    path: '',
    component: SettingsViewComponent,
    children: [
      ...childRoute
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
