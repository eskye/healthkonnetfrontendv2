import { EmitService } from './../../../shared/services/emit.service';
import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { EChartOption } from 'echarts';
import { echartStyles } from '../../../shared/echart-styles';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Utils } from 'src/app/shared/utils';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT, DatePipe } from '@angular/common';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { routes } from 'src/app/shared/constant';
import * as moment from 'moment';
import { ICounter } from 'src/app/shared/common/model/ICounter';
import { ChartService } from 'src/app/shared/services/chart.service';
import { Router } from '@angular/router';
import { IProfile } from 'src/app/shared/common/model/IProfile';
import { isNullOrUndefined } from 'util';
import { Store } from '@ngrx/store';
import { AppState } from '../../sessions/state/user.reducers';
import { StoreService } from 'src/app/shared/services/store.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent extends BaseComponent implements OnInit {
  slideOptions = { items: 2, loop: true, dots: true, navigation: true, autoplay: true };
  chartLineOption1: EChartOption;
  chartLineOption2: EChartOption;
  chartLineOption3: EChartOption;
  chartLineOption4: EChartOption;
  salesChartBar: EChartOption;
  salesChartPie: EChartOption;
  doughnutChartLabels: string[] = [];
  doughnutChartData: number[] = [];
  doughnutChartType = 'pie';
  lineChartData: Array<any> = [];
  lineChartLabels: Array<any> = [];
  greetingimage: string;
  time: string;
  counter: ICounter;
  userData: IProfile;
  isEmpty: boolean;
  constructor(private modalService: NgbModal,
    private chartService: ChartService, private ds: DataStoreService, private store: StoreService,
     private dashboardSerivce: DashboardService, router: Router) {
    super(null, router, null, null, chartService);
  }

  ngOnInit() {
    this.filter.days = 7;
    this.displayClock();
    this.getDashboardData();
    const query = {
      q: 'days',
      param: this.filter.days
    };
    this.getChartEnrolleeVisitData(query);
    this.getEnrolleePackageData();
    this.chart4(); 
  }

  /* get username(){ 
    return this.store.user;
  }
    */
 
  get greeting(){ 
   return this.store.greet;
  }

  private getDashboardData() {
    this.waiting = true;
    this.dashboardSerivce.getDashboard(routes.DASHBOARD.HMODASHBOARD).subscribe(res =>{
     this.counter = res;
    }, error => {
     this.waiting = false;
     this.dashboardSerivce.errorAlert(error, 'Error Alert');
    });
  }

  private displayClock() {
    // tslint:disable-next-line: max-line-length
    Utils.greet() === 'Good morning' ? this.greetingimage = 'morning' : Utils.greet() === 'Good afternoon' ? this.greetingimage = 'afternoon' : Utils.greet() === 'Good evening' ? this.greetingimage = 'evening' : null;
    setInterval(() => {
      this.time =Utils.clock();
    }, 1000);
  }

  private chart4(){

 this.chartLineOption4 = {

  color: ['#373c54'],
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    }
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  xAxis: [
    {
      type: 'category',
      data: ['Male', 'Female'],
      axisTick: {
        alignWithLabel: true
      }
    }
  ],
  yAxis: [{
    type: 'value'
  }],
  series: [{
    name: 'Counters',
    type: 'bar',
    barWidth: '60%',
    data: [10, 52]
  }]
 }
  }

  async switchChartData() {
    const query = {
      q: 'days',
      param: this.filter.days
    };
    await this.getChartEnrolleeVisitData(query);
  }


  private async getEnrolleePackageData() {
     this.waiting = true;
    await this.chartService
      .chartHmoEnrolleeData()
      .then(data => {
        this.salesChartPie = {
          color: ['#162e63', '#162e63', '#162e63', '#8877bd', '#9181bd', '#6957af'],
          tooltip: {
            show: true,
            backgroundColor: 'rgba(0, 0, 0, .8)'
          },
          xAxis: [{
            axisLine: {
              show: false
            },
            splitLine: {
              show: false
            }
          }
          ],
          yAxis: [{
            axisLine: {
              show: false
            },
            splitLine: {
              show: false
            }
          }
          ],
          series: [{
            name: 'Package ',
            type: 'pie',
            radius: '75%',
            center: ['50%', '50%'],
            data: data.data,
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
              }
            }
          }
          ]
        };
      })
      .catch(error => { 
        console.log(error);
      });
  }

  private async getChartEnrolleeVisitData(item) {
    this.waiting = true;
    await this.chartService
      .chartHmoEnrolleeVisitData(item)
      .then(data => {
        if(data.data.data.length === 0){
          this.isEmpty = true;
        }else{

       
        const labels: string[] = (<any>data.data).lastSevenDaysLabel;
        if (labels.length > 0) {
          this.formatDataVisit(labels);
        }
        this.lineChartData = (<any>data.data).data;

    this.chartLineOption3 = {
...echartStyles.lineNoAxis, ...{
          series: [{
            data: (<any>data.data).data,
            lineStyle: {
              color: 'rgba(5, 56, 118, .86)',
              width: 3,
              shadowColor: 'rgba(0, 0, 0, .2)',
              shadowOffsetX: -1,
              shadowOffsetY: 8,
              shadowBlur: 10
            },
            label: { show: true, color: '#212121' },
            type: 'line',
            smooth: true,
            itemStyle: {
              borderColor: 'rgba(7, 102, 216, 0.86)'
            }
          }]
        }
      };
      this.chartLineOption3.xAxis.data = this.lineChartLabels;
    }
        this.waiting = false;
      })
      .catch(error => { 
        console.log(error);
      });
  }

  private formatData(data: string[]) {
    return data.forEach(element => {
      this.doughnutChartLabels.push(element);
    });
  }

  private formatDataVisit(data: string[]) {
    const pipe = new DatePipe('en-US');
    return data.forEach(element => {
      const formatedDate = pipe.transform(element, 'mediumDate');
      this.lineChartLabels.push(formatedDate);
    });
  }



  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }
}
