import { HttpEvent, HttpEventType } from '@angular/common/http';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { isUndefined, isNullOrUndefined } from 'util';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { Utils } from 'src/app/shared/utils';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ClientService } from 'src/app/shared/services/client.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {
  userRole: any;
  totalCount: any;
  packages: any;
  fileExtensionError: boolean;
  fileExtensionMessage: string;
  private modalRef: NgbModalRef;
  filename: any;
  formData: any = new FormData();
  key: string;
  constructor(activatedRoute: ActivatedRoute,
    private employeeService: EmployeeService,
     private modalService: NgbModal, private authService: AuthService) {
    super(null, null, activatedRoute, null, employeeService);
    this.userRole = this.authService.store.getData('role');
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.key = params.get('id');
    });
    this.getClientDetail();
    this.init();
  }

  refresh(event) {
    this.reloadComponent();
  }
  init(event?) {
    if (isUndefined(event)) {
       this.filter = {};
       this.filter.key = this.key;
    } else {
      this.filter = event;
    }
    this.waiting = true;
    this.url = routes.STAFF.COUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.genPagination().subscribe((res) => {
      this.totalCount = res.data.total;
      this.employeeService.getService(EmitService).checkTotalCount(res.data.total);
      this.items = res.data.items;
      this.waiting = false;
   });
  }
  getClientDetail() {
    this.employeeService.getService(ClientService).getDetailByKey(this.key).subscribe(res => {
      this.item = res.data;
    }, error => {

    });
  }

  getPackages() {
    this.employeeService.getService(HmoService).getPackagesNameAndId().subscribe(res => {
      this.packages = res;
    }, error => {

    });
  }

  open(content, item = {}, orgId = 0) {
    this.getPackages();
    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }


  async onUploadFileChange(event) {
   
    const file = <File>event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, false, true)) {
      this.fileExtensionError = true;
      if (event.target.files.length > 0) {
         this.formData.append('file', file);
         this.formData.append('id', this.item.id);
         this.formData.append('OrgId', (<number>this.item.orgId));
      }
    } else {
      this.fileExtensionMessage = 'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.employeeService.warningAlert(this.fileExtensionMessage);
    }
  }

  uploadEnrollee() {
    this.waiting = true;
    this.employeeService.uploadBulkList(this.formData).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          break;
        case HttpEventType.Response:
          this.waiting = false;
          this.employeeService.successAlert('Uploaded successfully', 'Uploaded');
           this.reloadComponent();
          break;
        case 1: {
          this.waiting = true;
          /* if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
            this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
          } */
          break;
        }
      }
    }, error => {
      this.waiting = false;
       this.reloadComponent();
      this.employeeService.errorAlert(error, 'Error Uploading File To Server');
    });
  }

}
