import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Utils } from 'src/app/shared/utils';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { FileManagerService } from 'src/app/shared/services/file-manager.service';
import { CompanyService } from 'src/app/shared/services/company.service';
import { routes } from 'src/app/shared/constant';
import { isNullOrUndefined } from 'util';
import { NgForm } from '@angular/forms';
import { UploadStaffComponent } from 'src/app/shared/components/staff/upload-staff/upload-staff.component';
import { HmoService } from 'src/app/shared/services/hmo.service';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-organization',
  templateUrl: './org.component.html',
  styleUrls: ['./org.component.css']
})
export class OrgComponent extends BaseComponent implements OnInit {
  searchParameter: string;
  sectors = ['NHIS', 'Private', 'State'];
  isImage: boolean;
  fileExtensionError: boolean;
  fileExtensionMessage: string;
  private modalRef: NgbModalRef;
  filename: any;
  packages: any;
  formData: any = new FormData();
  constructor(router: Router,
    private modalService: NgbModal,
    private companyService: CompanyService,
    private fileManagerService: FileManagerService
  ) {
    super(null, router, null, null, fileManagerService);
  }

  ngOnInit() {
    this.item.dueDate = new Date();
    this.getCountry();
    this.init();
  }

  init() {
    this.url = routes.ORGANIZATION.COUNT;
    this.query = {
      count: this.paginationConfig.count = 100,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({ direction: 1, column: 1 }),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }

  

  open(content, item = {}, orgId = 0) {
    this.item.orgId = orgId;
    if (!isNullOrUndefined(item)) {
      this.item = item;
    }

    this.modalRef = this.modalService.open(content, {
      ariaLabelledBy: 'modal-basic-title'
    });
    this.modalRef.result.then(
      result => {
        console.log(result);
      },
      reason => {
        console.log('Err!', reason);
      }
    );
  }

  deleteInvoice(id, modal) {
    this.modalService
      .open(modal, { ariaLabelledBy: 'modal-basic-title', centered: true })
      .result.then(
        result => {
          this.fileManagerService.successAlert('Invoice Deleted!', 'Success!');
        },
        reason => {}
      );
  }
  
  async onFileChange(event) {
    const file = event.target.files[0];
    this.filename = file.name;
    if (Utils.fileValidator(file, true)) {
      this.isImage = true;
      this.fileExtensionError = true;
      const reader = new FileReader();
      if (event.target.files && event.target.files.length > 0) {
        reader.readAsDataURL(file);
        reader.onload = () => {
          (<any>document.getElementById('Company_Logo_src')).src =
            reader.result;
        };
      }
      if (event.target.files.length > 0) {
        const formData = new FormData();
        formData.append('File', file);
        this.MakeUploadRequest(formData);
      }
    } else {
      this.fileExtensionMessage =
        'File format not valid, only file with png,jpg is allowed';
      this.fileExtensionError = false;
      this.fileManagerService.warningAlert(this.fileExtensionMessage);
    }
  }

  submit() {
    this.waiting = true;
    this.companyService.createCompany(this.item).subscribe(
      res => {
        this.CloseAction();
      },
      error => {
        this.waiting = false;
        console.log(error);
        this.companyService.errorAlert(error, 'Error!!!');
      }
    );
  }
  addSetting(f: NgForm) {
    if (f.valid) {
      this.waiting = true;
    /*   this.item.dueDate = new Date(
        this.item.dueDate.year,
        this.item.dueDate.month - 1,
        this.item.dueDate.day
      ); */
      this.companyService.addSetting(this.item).subscribe(
        res => {
          this.waiting = false;
          this.modalRef.close();
        },
        error => {
          this.waiting = false;
          console.log(error);
          this.companyService.errorAlert(error, 'Error!!!');
        }
      );
    }
  }

  private CloseAction() {
    this.waiting = false;
    this.item = {};
    this.filename = '';
    this.companyService.successAlert('Record created', 'Create');
    this.modalRef.close();
    this.reloadComponent();
  }

  private MakeUploadRequest(formData: FormData) {
    this.fileManagerService.uploadProfileImage(formData).subscribe(
      (event: HttpEvent<any>) => {
        // console.log(event);
        switch (event.type) {
          case HttpEventType.Sent:
            break;
          case HttpEventType.Response:
            this.item.logoUrl = (<any>event).body.data.result;

            this.isUploading = false;
            // this.fileManagerService.successAlert((<any>event).body.data.filename);
            // this.msg = (<any>event).body.data.error;
            break;
          case 1:
            {
              this.isUploading = true;
              // if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
              //   this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
            }
            break;
        }
      },
      error => {
        this.isUploading = false;
        this.fileManagerService.errorAlert(
          'An error occurred while uploading logo to the server, try again',
          'Error Uploading File To Server'
        );
      }
    );
  }
 
}
