import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AvatarModule } from 'ngx-avatar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClaimHmoRoutingModule } from './claim-hmo-routing.module';
import { ClaimHmoComponent } from './claim-hmo.component';
import { AddClaimHmoComponent } from './add-claim-hmo/add-claim-hmo.component';
import { ListClaimsHmoComponent } from './list-claims-hmo/list-claims-hmo.component';
import { AuthorizationCodeHmoComponent } from './authorization-code-hmo/authorization-code-hmo.component';
import { ListAuthorizationCodeHmoComponent } from './list-authorization-code-hmo/list-authorization-code-hmo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { DetailClaimHmoComponent } from './detail-claim-hmo/detail-claim-hmo.component';
import { VerifyEnrolleeHmoComponent } from './verify-enrollee-hmo/verify-enrollee-hmo.component';

const components = [
  ClaimHmoComponent,
  AddClaimHmoComponent,
  ListClaimsHmoComponent,
  AuthorizationCodeHmoComponent,
   ListAuthorizationCodeHmoComponent,
   DetailClaimHmoComponent,
   VerifyEnrolleeHmoComponent
  ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    SharedPipesModule,
    NgSelectModule,
    NgbModule,
    AvatarModule,
    ClaimHmoRoutingModule
  ],
  declarations: components
})
export class ClaimHmoModule { }
