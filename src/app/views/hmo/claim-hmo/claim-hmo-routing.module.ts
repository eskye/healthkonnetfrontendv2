import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClaimHmoComponent } from './claim-hmo.component';
import { AddClaimHmoComponent } from './add-claim-hmo/add-claim-hmo.component';
import { ListClaimsHmoComponent } from './list-claims-hmo/list-claims-hmo.component';
import { ListAuthorizationCodeHmoComponent } from './list-authorization-code-hmo/list-authorization-code-hmo.component';
import { AuthorizationCodeHmoComponent } from './authorization-code-hmo/authorization-code-hmo.component';
import { DetailClaimHmoComponent } from './detail-claim-hmo/detail-claim-hmo.component';

const routes: Routes = [
  {path: '', component: ClaimHmoComponent,
  children: [
    {path: 'add-claim', component: AddClaimHmoComponent},
    {path: 'list', component: ListClaimsHmoComponent},
    {path: ':id/detail', component: DetailClaimHmoComponent},
    {path: 'authorization-codes', component: ListAuthorizationCodeHmoComponent},
    {path: 'new-authorization-code-request', component: AuthorizationCodeHmoComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClaimHmoRoutingModule { }
