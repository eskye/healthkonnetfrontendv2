import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Router, ActivatedRoute } from '@angular/router';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { routes } from 'src/app/shared/constant';
import { Location } from '@angular/common';

@Component({
  selector: 'app-list-claims-hmo',
  templateUrl: './list-claims-hmo.component.html',
  styleUrls: ['./list-claims-hmo.component.scss']
})
export class ListClaimsHmoComponent extends BaseComponent implements OnInit {
  batchCode: string;
   searchText: any;
  constructor(location: Location, router: Router, activatedRoute: ActivatedRoute,
    private claimService: ClaimService) {
    super(location, router, activatedRoute, null, claimService);
  }

  ngOnInit() {
    this.getStatuses(routes.CLAIM.CLAIMSTATUS);
    this.getNameAndId(routes.SELECTHOSPITAL.SELECTEDHOSPITAL);
    this.filter.isSent = true;
    this.init();
  }

  init() {
    this.url = routes.CLAIM.CLAIMFORMCOUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.setupPaginations();
  }
}

