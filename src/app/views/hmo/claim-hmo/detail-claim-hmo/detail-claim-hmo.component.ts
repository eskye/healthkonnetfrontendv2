import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { ClaimService } from 'src/app/shared/services/Claim.Service';

@Component({
  selector: 'app-detail-claim-hmo',
  templateUrl: './detail-claim-hmo.component.html',
  styleUrls: ['./detail-claim-hmo.component.scss']
})
export class DetailClaimHmoComponent extends BaseComponent implements OnInit {

  id: string;

  constructor(location: Location, activatedRoute: ActivatedRoute, router: Router, 
    private claimService: ClaimService) {
    super(location, router, activatedRoute, null, claimService);
  }

  ngOnInit() {
    this.id = this.getParamValue('id');
  }

  approveClaim($event) {
    this.waiting = true;
    this.claimService.approveClaim(this.id).subscribe(res => {
      this.waiting = false;
      this.reloadComponent();
      this.claimService.successAlert('Claim has been approved successfully', 'Success');
      this.goToNav('/hmo/claims/list');
    }, error => {
      this.waiting = false;
      this.claimService.errorAlert(error, 'Error');
    });
    }

}
