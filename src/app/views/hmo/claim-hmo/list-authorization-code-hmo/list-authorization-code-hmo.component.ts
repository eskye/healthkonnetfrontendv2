import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { Title } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/shared/services/auth.service';
import { EnrolleeService } from 'src/app/shared/services/enrollee.service';
import { ClaimService } from 'src/app/shared/services/Claim.Service';
import { isUndefined } from 'util';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-list-authorization-code-hmo',
  templateUrl: './list-authorization-code-hmo.component.html',
  styleUrls: ['./list-authorization-code-hmo.component.scss']
})
export class ListAuthorizationCodeHmoComponent extends BaseComponent implements OnInit {
  totalCount: any;
  userRole: any;
  modalClose: boolean;
  constructor( titleService: Title,  private auth : AuthService, private claimService: ClaimService) {
    super(null, null, null, titleService, claimService);
    this.userRole = this.auth.store.getData('role');
   }

  ngOnInit() {
    this.titleService.setTitle('Request Claim Authorization Code');
   // this.loadEnrollee();
   this.init();
  }
  init(event?) {
    if (isUndefined(event)) {
      this.filter = {};
   } else {
     this.filter = event;
   }
    this.waiting = true;
    this.url = routes.CLAIM.GETREQUESTAUTHORIZATION;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.genPagination().subscribe((res) => {
      this.totalCount = res.data.total;
      this.claimService.getService(EmitService).checkTotalCount(res.data.total);
      this.items = res.data.items;
      this.waiting = false;
   });
  }

  refresh(event) {
    this.reloadComponent();
  }


submit(event) {
      this.waiting = true;
      this.claimService.requestAuthorizationCode(event).subscribe(res => {
        this.waiting = false;
        this.modalClose = false;
        this.reloadComponent();
        this.claimService.successAlert('Authorization Request sent successfully', 'Success');
       // console.log(res);
      }, error => {
        this.waiting = false;
        this.claimService.errorAlert(error, 'Error');
      });
}

approveAuthorizationCode(item) {
  this.waiting = true;
  this.claimService.approveAuthorizationCode(item).subscribe(res => {
    this.waiting = false;
    this.reloadComponent();
    this.claimService.successAlert('Authorization Code Generated and Approved', 'Success');
   // console.log(res);
  }, error => {
    this.waiting = false;
    this.claimService.errorAlert(error, 'Error');
  });
  }
}
