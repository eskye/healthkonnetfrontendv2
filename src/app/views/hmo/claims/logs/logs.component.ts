import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';

const log = [
  {
    visit_code: '59-9228414',
    name: 'Trudie Coleson',
    status: 'declined',
    date: '2018-08-30 03:18:56'
  },
  {
    visit_code: '51-9511125',
    name: 'Elmo Hercules',
    status: 'declined',
    date: '2018-10-19 21:05:38'
  },
  {
    visit_code: '00-1433456',
    name: 'Benson Arias',
    status: 'declined',
    date: '2018-11-25 14:26:43'
  },
  {
    visit_code: '58-0116464',
    name: 'Truman Remmers',
    status: 'approved',
    date: '2019-06-01 00:30:06'
  },
  {
    visit_code: '52-7271073',
    name: 'Daron Ritchings',
    status: 'pending',
    date: '2018-08-15 13:36:59'
  }
];

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent extends BaseComponent implements OnInit {
  logs = log; actions: any;
  sections: any;

  constructor() {
    super(null, null, null, null);
  }

  ngOnInit() {}
}
