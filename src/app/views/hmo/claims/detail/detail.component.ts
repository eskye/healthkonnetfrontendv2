import { Component, OnInit, ViewChild } from '@angular/core';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
const detail = [
  {
    claim_type: 'dedicated',
    description: 'rutrum',
    quantity: 88,
    price: '$2.55',
    date: '2018-06-28 13:16:48'
  },
  {
    claim_type: 'complexity',
    description: 'erat nulla',
    quantity: 72,
    price: '$5.02',
    date: '2018-12-18 03:41:55'
  },
  {
    claim_type: 'productivity',
    description: 'nam',
    quantity: 81,
    price: '$0.32',
    date: '2018-12-12 23:41:58'
  },
  {
    claim_type: 'local area network',
    description: 'lacus morbi sem',
    quantity: 48,
    price: '$2.89',
    date: '2018-09-25 10:04:51'
  },
  {
    claim_type: 'actuating',
    description: 'elementum',
    quantity: 7,
    price: '$3.90',
    date: '2018-07-31 13:32:17'
  },
  {
    claim_type: 'dedicated',
    description: 'elit sodales scelerisque',
    quantity: 61,
    price: '$2.33',
    date: '2018-11-11 11:26:32'
  },
  {
    claim_type: 'process improvement',
    description: 'eget',
    quantity: 72,
    price: '$5.03',
    date: '2018-11-08 07:40:07'
  },
  {
    claim_type: 'knowledge base',
    description: 'ut massa quis',
    quantity: 42,
    price: '$2.34',
    date: '2018-07-07 05:59:23'
  }
];
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  details = detail;

  @ViewChild(PerfectScrollbarDirective) psContainer: PerfectScrollbarDirective;
  constructor() {}

  ngOnInit() {}


  sendMessage(e) {

  }
}
