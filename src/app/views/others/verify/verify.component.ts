import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { sroles } from 'src/app/shared/constant';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
query: string;
amount: any;
  private userData:  any | boolean;
  private isAnonymous: boolean;
  isLoading = false;
  constructor(private route: ActivatedRoute, private router: Router, private auth: AuthService) {
    this.userData = this.auth.store.getData('udata');
    if (this.userData === false) {
      this.auth.getUserDetail().then(userdata => {
        this.userData = userdata;
        this.isAnonymous = this.userData.isAnonymous;
      }, err => {console.error(err); });
    }
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.query = params['status'];
      this.amount = params['amount'];
      // if (this.query === 'success' && this.userData.isAnonymous && this.auth.role === 'Enrollee') {
      //   this.router.navigate(['/enrollee/package']);
      // } else if (this.query === 'success' && !this.isAnonymous && this.auth.role === 'Organization') {
      //   this.router.navigate(['/organization/payments']);
      // } else {
      //   this.router.navigate(['/verify']);
      // }
    });
  }

  goToApplication() {
    if (this.query === 'success' && this.userData.isAnonymous && this.auth.role === sroles.Enrollee) {
      this.router.navigate(['/enrollee/package']);
    } else if (this.query === 'success' && !this.isAnonymous && this.auth.role === sroles.Organization) {
      this.router.navigate(['/client/dashboard']);
    } else {
      this.router.navigate(['/error']);
    }
  }

}
