import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AccountService } from 'src/app/shared/services/account.service';
import { Observable, of } from 'rxjs';
import {Action} from '@ngrx/store';
import * as userAction from '../state/user.actions';
import { mergeMap, catchError, map } from 'rxjs/operators';

@Injectable()

export class UserEffect {
    constructor(private actions$: Actions, private accountService: AccountService) { }
    @Effect()
    getLoggedInUser$: Observable<Action> = this.actions$.pipe(
        ofType<userAction.GetCurrentUser>(
            userAction.UserActionTypes.GET_CURRENTLOGGEDIN_USER
        ),
       mergeMap((actions: userAction.GetCurrentUser) =>
          this.accountService.getCurrentLoggedInUser().pipe(
              map(
                  (user) =>
                  new userAction.LoadUserSuccess(user)
              ), catchError(err => of(new userAction.LoadUserFail(err.message)))
          )
       )
    );
}