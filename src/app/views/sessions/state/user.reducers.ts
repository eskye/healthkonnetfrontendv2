import * as userActions from './user.actions';
import * as fromRoot from './../../../state/app-state';
export interface IUserState {
    user: {};
    payLoad: {};
    loading: boolean;
    loaded: boolean;
    error: string;
}
export interface AppState extends fromRoot.AppState {
   user: IUserState;
}
export const initalState: IUserState = {
    user: {},
    payLoad: {},
    loading: false,
    loaded: false,
    error: ''
};
export function userReducer(state = initalState, action: userActions.Action): IUserState {
    switch (action.type) {
        case userActions.UserActionTypes.GET_CURRENTLOGGEDIN_USER: {
            return {
                ...state,
                loading: true
                };
        }
        case userActions.UserActionTypes.LOAD_USER_SUCCESS: {
            return {
                ...state,
                payLoad: (<any>action.payLoad).data,
                loading: false,
                loaded: true
                };
        }
        case userActions.UserActionTypes.LOAD_USER_FAIL:{
            return {
                ...state,
                loading: true,
                loaded: true,
            };
        }
        case userActions.UserActionTypes.REMOVE_USER_DATA: {
            return {
                ...state,
                payLoad: {},
                loading: false
                };
        }
        default: {
        return state;
        }

    }
}