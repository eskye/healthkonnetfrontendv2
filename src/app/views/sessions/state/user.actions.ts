 import {Action} from '@ngrx/store';

export enum UserActionTypes{
  GET_CURRENTLOGGEDIN_USER = '[User] Load Current User',
  LOAD_USER_SUCCESS = '[User] Load Success User',
  LOAD_USER_FAIL = '[User] Load Fail User',
  REMOVE_USER_DATA = '[User] Remove User Data'
}

export class GetCurrentUser implements Action {
  readonly type = UserActionTypes.GET_CURRENTLOGGEDIN_USER;
}

export class LoadUserSuccess implements Action {
 readonly type = UserActionTypes.LOAD_USER_SUCCESS;
 constructor(public payLoad: {}) { }
}

export class LoadUserFail implements Action {
readonly type = UserActionTypes.LOAD_USER_FAIL;
constructor(public payLoad: string) {}
}
export class RemoveUserData implements Action {
  readonly type = UserActionTypes.REMOVE_USER_DATA;
 }
export type Action = GetCurrentUser | LoadUserSuccess | LoadUserFail | RemoveUserData;
