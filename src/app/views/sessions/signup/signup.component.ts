import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { NgForm } from '@angular/forms';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  animations: [SharedAnimations]
})
export class SignupComponent implements OnInit {
item: any = {};
loading = false;
  response: string;
  isSuccess: boolean;
  constructor(private accountService: AccountService) { }

  ngOnInit() {
  }

  submit(f: NgForm) {
    if (f.valid) {
       this.loading = true;
       this.accountService.createAccounts(this.item).subscribe(res => {
         this.loading = false;
         this.isSuccess = true;
         this.response = 'Your account have been created successfully, Check your mail to complete sign up process';
       }, error => {
         this.isSuccess = false;
         this.response = error;
         this.loading = false;
         this.accountService.errorAlert(error);
       });
    }
  }

}
