import { UserEffect } from './state/user.effects';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionsRoutingModule } from './sessions-routing.module';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { ForgotComponent } from './forgot/forgot.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { StoreModule } from '@ngrx/store';
import { userReducer } from './state/user.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedComponentsModule,
    SessionsRoutingModule,
    StoreModule.forFeature('user', userReducer),
    EffectsModule.forFeature([UserEffect])
  ],
  declarations: [SignupComponent, SigninComponent, ForgotComponent, ChangePasswordComponent, ResetPasswordComponent]
})
export class SessionsModule { }
