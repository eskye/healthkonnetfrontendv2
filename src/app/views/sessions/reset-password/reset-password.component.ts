import { Component, OnInit } from '@angular/core';
import { RouterStateSnapshot, Router } from '@angular/router';
import { AccountService } from 'src/app/shared/services/account.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidateInputService } from 'src/app/shared/services/validate-input.service';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  animations: [SharedAnimations]
})
export class ResetPasswordComponent implements OnInit {
  token: string;
  userId: string;
  resetPassword: FormGroup;
  resetObj: any;
  successText: string;
  loading: boolean;
  isSuccess: boolean;

  constructor(private router: Router, private accountService: AccountService) { }

  ngOnInit() {
    const snapshot: RouterStateSnapshot = this.router.routerState.snapshot;
    this.token = snapshot.root.queryParamMap.get('token');  // <-- hope it help
    this.userId = snapshot.root.queryParamMap.get('userId');
    this.validate();
  }

  validate() {
    this.resetPassword = new FormGroup({
      password: new FormControl('', [Validators.required, ValidateInputService.passwordValidator]),
      confirmpassword: new FormControl('', Validators.required),
    }, ValidateInputService.passwordMatchValidator.bind(this));
  }

  resetUserPassword() {
    if (this.resetPassword.valid) {
      this.loading = true;
       this.resetObj = this.resetPassword.value;
       this.resetObj.code = this.token;
       this.resetObj.UserId = this.userId;
       this.accountService.resetPassword(this.resetObj).subscribe(res => {
         this.isSuccess = true;
         this.successText = `${(<any>res).message}. We are redirecting you to login...`;
         this.loading = false;
         setTimeout(() => {
          this.router.navigate(['/sessions/signin']);
        }, 1500);
       }, error => {
         this.successText = error;
          console.log(error);
           this.isSuccess = false;
            this.loading = false;
          });
       return;
    }
    return this.successText = 'Password and Confirm Password is required';
  }

}
