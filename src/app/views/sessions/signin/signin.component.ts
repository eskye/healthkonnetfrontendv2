import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import {
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import {
  Router,
  RouteConfigLoadStart,
  ResolveStart,
  RouteConfigLoadEnd,
  ResolveEnd
} from '@angular/router';
import { ValidateEmail } from 'src/app/shared/directives/email-validator.directive';
import { ValidatePassword } from 'src/app/shared/directives/password-validator.directive';
import { IUserLogin } from '../../../shared/common/model/IAccount';
import { DataStoreService } from '../../../shared/services/data-store.service';
import { AccountService } from '../../../shared/services/account.service';
import { SystemConstant as constants, routes } from '../../../shared/constant';
import { Store } from '@ngrx/store';
import * as userActions from '../../sessions/state/user.actions';
import { isNullOrUndefined } from 'util';
import { EmitService } from 'src/app/shared/services/emit.service';
import { AppState } from '../state/user.reducers';
 
 
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  animations: [SharedAnimations]
})
export class SigninComponent implements OnInit {
  loading: boolean;
  loadingText: string;
  signinForm: FormGroup;
  errorText: string;
  constructor(
    private ds: DataStoreService,
    private accountService: AccountService,
    private store: Store<AppState>,
    private emitService: EmitService,
    private router: Router
  ) {}

  ngOnInit() {
    this.triggerGuard();
    this.router.events.subscribe(event => {
      if (
        event instanceof RouteConfigLoadStart ||
        event instanceof ResolveStart
      ) {
        // this.loadingText = 'Loading Dashboard Module...';
        // this.loading = true;
      }
      if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
        this.loading = false;
      }
    });
    this.validateFields();
  }
  validateFields() {
    this.signinForm = new FormGroup({
      email: new FormControl('', [Validators.required, ValidateEmail('email')]),
      password: new FormControl('', [
        Validators.required,
        ValidatePassword('password')
      ])
    });
  }
  signin() {
    this.loading = true;
    this.loadingText = 'Sigining in...';
    this.errorText = '';
    if (this.signinForm.valid) {
      const login: IUserLogin = this.signinForm.value;
      return this.makeRequest(login);
    }
    this.loading = false;
    return (this.errorText = 'Email and Password is required');

    // this.auth.signin(this.signinForm.value)
    //     .subscribe(res => {
    //         this.router.navigateByUrl('/dashboard/v1');
    //         this.loading = false;
    //     });
  }

  private makeRequest(login) {
    this.accountService.login(login).subscribe(
      token => {
        this.KeepData(token);
        // this.getLoggedInUser();
        this.storeUserData().then(
          data => {
           // console.log(data);
            this.emitService.changeData(data);
            this.ds.keepData('udata', data);
            // this.ds.deleteUserData('user');
            this.ds.saveUserData('user', JSON.stringify(data));
            // this.cookieService.set('udata', JSON.stringify(data));
            //  this.cookieService.set('token', token.accessToken);
          },
          err => {
            console.error(err);
          }
        );

        if (token.role === constants.LEVEL_ACCESS.h) {
          this.router.navigate(['/hmo/dashboard']);
        } else if (token.role === constants.LEVEL_ACCESS.p) {
          this.router.navigate(['/provider/dashboard']);
        } else if (token.role === constants.LEVEL_ACCESS.o) {
          return this.router.navigate(['/client/dashboard']);
        } else if (token.role === constants.LEVEL_ACCESS.e) {
          this.router.navigate(['/enrollee/dashboard']);
        } else if (token.role === constants.LEVEL_ACCESS.a) {
          this.router.navigate(['/systemadmin/dashboard']);
        } else if (
          this.accountService.AuthService.role === constants.LEVEL_ACCESS.s
        ) {
          // this.router.navigateByUrl(this.returnUrl);
          this.router.navigate(['/systemadmin/dashboard']);
        } else {
          this.router.navigateByUrl('/sessions/signin');
        }
      },
      error => {
        if (!isNullOrUndefined(error.message)) {
          if (error.message.toLowerCase().includes('unknown error')) {
            this.errorText = 'Unable to connect to the server, try again';
            this.loading = false;
          } else {
            this.loading = false;
            this.errorText = error.error.message;
          }
        } else {
          this.loading = false;
          this.errorText =
            'Cannot connect to the server, check your internet connection';
        }
      }
    );
  }

  /*  private getLoggedInUser() {
    this.store.dispatch(new userActions.GetCurrentUser());
  }  */
  private storeUserData() {
    return new Promise((resolve, reject) => {
      this.accountService
        .getlist(routes.LOGGEDIN)
        .subscribe(data => resolve(data.data), err => reject(err));
    });
  }

  triggerGuard() {
    // alert(this.dataService.getData('role'));
    if (!!this.ds.getData('token') && !!this.ds.getData('role')) {
      this.RedirectUserIfNotLoggedOut();
    }
  }

  private RedirectUserIfNotLoggedOut() {
    // debugger;
    if (this.accountService.AuthService.role === constants.LEVEL_ACCESS.h) {
      this.router.navigate(['/hmo/dashboard']);
      // this.router.navigateByUrl(this.returnUrl);
    } else if (
      this.accountService.AuthService.role === constants.LEVEL_ACCESS.p
    ) {
      this.router.navigate(['/provider/dashboard']);
      // this.router.navigateByUrl(this.returnUrl);
    } else if (
      this.accountService.AuthService.role === constants.LEVEL_ACCESS.o
    ) {
      this.router.navigate(['/client/dashboard']);
      // this.router.navigateByUrl(this.returnUrl);
    } else if (this.ds.getData('role') === 'Enrollee') {
      this.router.navigate(['/enrollee']);
      // this.router.navigateByUrl(this.returnUrl);
    } else if (
      this.accountService.AuthService.role === constants.LEVEL_ACCESS.a
    ) {
      this.router.navigate(['/systemadmin/dashboard']);
      // this.router.navigateByUrl(this.returnUrl);
    } else if (
      this.accountService.AuthService.role === constants.LEVEL_ACCESS.s
    ) {
      this.router.navigate(['/systemadmin/dashboard']);
      // this.router.navigateByUrl(this.returnUrl);
    } else {
      // this.router.navigateByUrl(this.returnUrl);
      this.router.navigate(['/']);
    }
  }

  protected KeepData(token) {
    this.ds.keepData('token', token.accessToken);
    this.ds.keepData('role', token.role);
    this.ds.keepData('key', token.key);
  }
}
