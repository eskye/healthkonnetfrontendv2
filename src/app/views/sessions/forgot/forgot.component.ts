import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss'],
  animations: [SharedAnimations]
})
export class ForgotComponent implements OnInit {
  forgotPassword: FormGroup;
  showIcon: boolean;
  errorText = '';
  successText = '';
  loading: boolean;
  isSuccess: boolean;
  constructor(private accountService: AccountService) { }


  ngOnInit() {
    this.reset();
  }

  reset() {
    this.forgotPassword = new FormGroup({
      email: new FormControl('', Validators.required),
    });
  }

  sendPassword() {
    if (this.forgotPassword.valid) {
         this.loading = true;
      const email = this.forgotPassword.value.email;
      this.accountService.sendPasswordMail(email).subscribe(data => {
        this.successText = 'Check your email for reset password instruction';
        this.isSuccess = true;
        this.loading = false;
      }, error => {
        this.successText = error;
         this.isSuccess = false;
          this.loading = false;
        });
    }
  }
}
