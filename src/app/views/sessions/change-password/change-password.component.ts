import { Component, OnInit } from '@angular/core';
import { SharedAnimations } from 'src/app/shared/animations/shared-animations';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidateInputService } from 'src/app/shared/services/validate-input.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { notifications } from 'src/app/shared/constant';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  animations: [SharedAnimations]
})
export class ChangePasswordComponent implements OnInit {
  changeForm: FormGroup;
  item: any;
  loading: boolean;
  errorText: any;

  constructor(private authService: AuthService, private router: Router, private toastService: ToastrService) { }

  ngOnInit() {
    this.validate();
  }

  validate() {
    this.changeForm = new FormGroup({
      currentpassword: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, ValidateInputService.passwordValidator]),
      confirmpassword: new FormControl('', Validators.required),
    }, ValidateInputService.passwordMatchValidator.bind(this));
  }

  changePassword() {
    if (this.changeForm.valid) {
      this.loading = true;
      this.item = this.changeForm.value;
      this.item.id = this.authService.Key;
      this.authService.passwordChanger(this.item).subscribe(() => {
        this.authService.store.removeAllPersistedData();
        this.loading = false;
        this.toastService.success('Password have been changed successfully', notifications.success, {timeOut: 4000});
        this.router.navigate(['/sessions/signin']);
      }, error => {
        this.loading = false;
       this.errorText = error;
      });
    }
  }
}
