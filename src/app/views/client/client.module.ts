import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientRoutingModule } from './client-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { NgbModule, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill';
import { NgxEchartsModule } from 'ngx-echarts';
import { UploadStaffComponent } from './upload-staff/upload-staff.component';
import { StaffComponent } from './staff/staff.component';
import { ListComponent } from './staff/list/list.component';
import { TransactionComponent } from './transaction/transaction.component';
import { TransactionDetailComponent } from './transaction/transaction-detail/transaction-detail.component';

const components = [
  DashboardComponent,
  UploadStaffComponent,
  StaffComponent,
  ListComponent,
  TransactionComponent,
  TransactionDetailComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedComponentsModule,
    SharedPipesModule,
    NgbModule,
    QuillModule,
    NgxEchartsModule,
    ClientRoutingModule
  ],
  declarations: components,
 
})
export class ClientModule { }
