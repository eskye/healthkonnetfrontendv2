import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UploadStaffComponent } from './upload-staff/upload-staff.component';
import { StaffComponent } from './staff/staff.component';
import { ListComponent } from './staff/list/list.component';
import { TransactionComponent } from './transaction/transaction.component';
import { TransactionDetailComponent } from './transaction/transaction-detail/transaction-detail.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'staff',
    component: StaffComponent,
    children: [
      {path: 'list', component: ListComponent},
      {path: 'create', component: UploadStaffComponent},
      {path: '', redirectTo: 'list'}
  ]
  },
  {
    path: 'transactions',
    component: TransactionComponent
  },
  {
    path: 'transactions/:id/detail',
    component: TransactionDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
