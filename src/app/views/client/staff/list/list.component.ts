import { Component, OnInit } from '@angular/core';
import { isUndefined } from 'util';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { routes } from 'src/app/shared/constant';
import { EmitService } from 'src/app/shared/services/emit.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {
  userRole: any;
  totalCount: any;
  constructor(private employeeService: EmployeeService, private authService: AuthService) {
    super(null, null, null, null, employeeService);
    this.userRole = this.authService.store.getData('role');
  }

  ngOnInit() {
    this.init();
  }

  refresh(event) {
    this.reloadComponent();
  }
  init(event?) {
    if (isUndefined(event)) {
       this.filter = {};
    } else {
      this.filter = event;
    }
    this.waiting = true;
    this.url = routes.STAFF.COUNT;
    this.query = {
      count: this.paginationConfig.count,
      page: this.paginationConfig.page,
      orderByExpression: JSON.stringify({direction: 1, column: 1}),
      whereCondition: this.filter
    };
    this.genPagination().subscribe((res) => {
      this.totalCount = res.data.total;
      this.employeeService.getService(EmitService).checkTotalCount(res.data.total);
      this.items = res.data.items;
      this.waiting = false;
   });
  }
}
