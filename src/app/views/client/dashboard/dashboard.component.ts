import { ChartService } from './../../../shared/services/chart.service';
import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { EChartOption } from 'echarts'; 
import { Utils } from 'src/app/shared/utils';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { DataStoreService } from 'src/app/shared/services/data-store.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
import { PaymentService } from 'src/app/shared/services/payment.service';
import { DashboardService } from 'src/app/shared/services/dashboard.service';
import { routes } from 'src/app/shared/constant';
import { ICounter } from 'src/app/shared/common/model/ICounter';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends BaseComponent implements OnInit, AfterViewInit {
  salesChartPie: EChartOption;
  greeting: string;
  greetingimage: string;
  time: string;
  counter: ICounter;
  private modalRef: NgbModalRef;
  isProcessing: boolean;
  constructor(private ds: DataStoreService, private modalService: NgbModal,
    @Inject(DOCUMENT) private document: any, private dashboardSerivce: DashboardService,
    private chartService: ChartService, private paymentService: PaymentService) {
    super(null, null, null, null, paymentService);
  }

  ngOnInit() {
    this.getDashboardData();
    this.getEnrolleePackageData();
  }

  private getDashboardData() {
    this.waiting = true;
    this.dashboardSerivce.getDashboard(routes.DASHBOARD.ORGANIZATIONDASHBOARD).subscribe(res =>{
     this.counter = res;
    }, error => {
     this.waiting = false;
     this.dashboardSerivce.errorAlert(error, 'Error Alert');
    });
  }

  private async getEnrolleePackageData() {
    this.waiting = true;
   await this.chartService
     .chartDashboardData()
     .then(data => {
       this.salesChartPie = {
         color: ['#2f47c2', '#162e63', '#2f47c2', '#162e63', '#162e63', '#2f47c2'],
         tooltip: {
           show: true,
           backgroundColor: 'rgba(0, 0, 0, .8)'
         },
         xAxis: [{
           axisLine: {
             show: false
           },
           splitLine: {
             show: false
           }
         }
         ],
         yAxis: [{
           axisLine: {
             show: false
           },
           splitLine: {
             show: false
           }
         }
         ],
         series: [{
           name: 'Health Care Provider',
           type: 'pie',
           radius: '75%',
           center: ['50%', '50%'],
           data: data.data,
           itemStyle: {
             emphasis: { 
               shadowBlur: 10,
               shadowOffsetX: 0,
               shadowColor: 'rgba(0, 0, 0, 0.5)'
             }
           }
         }
         ]
       };
     })
     .catch(error => {
       console.log(error);
     });
 }

  ngAfterViewInit() {
    console.log(this.ds.getData('udata'));
    this.greeting = `Hi ${this.ds.getData('udata').firstName}, ${Utils.greet()}`;
    Utils.greet() === 'Good morning' ? this.greetingimage = 'morning'
     : Utils.greet() === 'Good afternoon' ? this.greetingimage = 'afternoon'
// tslint:disable-next-line: no-unused-expression
     : Utils.greet() === 'Good evening' ? this.greetingimage = 'evening' : null;
    setInterval(() => {
      this.time = Utils.clock();
    }, 1000);
  }
  open(content) {
    this.modalRef = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
    this.modalRef.result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log('Err!', reason);
    });
  }

  fundAccount(f: NgForm) {
    if (f.valid) {
      this.isProcessing = true;
      this.item.isMobile = false;
      this.paymentService.fundWallet(this.item).subscribe((response) => {
        this.paymentService.successAlert('Redirecting to payment portal...');
        this.isProcessing = false;
        this.document.location.href = response.data.authorization_url;
      }, error => {
        this.isProcessing = false;
        this.paymentService.errorAlert(error.error.message);
      });
    } else {
      this.paymentService.errorAlert('Invalid request');
    }
  }

}
