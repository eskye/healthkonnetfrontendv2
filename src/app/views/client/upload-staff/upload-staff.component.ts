import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/shared/services/employee.service';
import { BaseComponent } from 'src/app/shared/common/baseClasses/BaseComponent';
import { HttpEvent, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-upload-staff',
  templateUrl: './upload-staff.component.html',
  styleUrls: ['./upload-staff.component.scss']
})
export class UploadStaffComponent extends BaseComponent implements OnInit {

  constructor(router: Router, private employeeService: EmployeeService ) {
    super(null, router, null, null, employeeService);
  }

  ngOnInit() {
  }

  submitFinally(formdata: FormData) {
    this.waiting = true;
    this.employeeService.uploadBulkList(formdata).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          break;
        case HttpEventType.Response:
          this.waiting = false;
          this.employeeService.successAlert('Uploaded successfully', 'Uploaded');
           this.reloadComponent();
          break;
        case 1: {
          this.waiting = true;
          /* if (Math.round(this.uploadedPercentage) !== Math.round(event['loaded'] / event['total'] * 100)) {
            this.uploadedPercentage = Math.round(event['loaded'] / event['total'] * 100);
          } */
          break;
        }
      }
    }, error => {
      this.waiting = false;
       this.reloadComponent();
      this.employeeService.errorAlert(error, 'Error Uploading File To Server');
    });
  }
}
