import { Component } from '@angular/core';
import { FirebaseMessagingService } from './shared/services/firebase-messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  message: any;
  constructor(private messagingService: FirebaseMessagingService) {
    this.messagingService.getPermission('user001');
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;
  }
  title = 'bootDash';
}
