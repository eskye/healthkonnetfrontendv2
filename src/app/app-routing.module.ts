import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './shared/components/layouts/auth-layout/auth-layout.component';
import { AuthGaurd } from './shared/services/auth.gaurd';
import { BlankLayoutComponent } from './shared/components/layouts/blank-layout/blank-layout.component';
// tslint:disable-next-line: max-line-length
import { AdminLayoutSidebarCompactComponent } from './shared/components/layouts/admin-layout-sidebar-compact/admin-layout-sidebar-compact.component';
// tslint:disable-next-line: max-line-length
import { AdminLayoutSidebarLargeComponent } from './shared/components/layouts/admin-layout-sidebar-large/admin-layout-sidebar-large.component';
import { RoleGuardService } from './shared/services/route-guards/role-guard.service';
import { sroles } from './shared/constant';
import { AuthGuardService } from './shared/services/route-guards/auth-guard.service';
import { VerifyComponent } from './views/others/verify/verify.component';
import { CompleteProfilComponent } from './views/enrollee/onboard/complete-profil/complete-profil.component';

const adminRoutes: Routes = [
    {
      path: 'dashboard',
      loadChildren: './views/dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'uikits',
      loadChildren: './views/ui-kits/ui-kits.module#UiKitsModule'
    },
    {
      path: 'forms',
      loadChildren: './views/forms/forms.module#AppFormsModule'
    },
    {
      path: 'invoice',
      loadChildren: './views/invoice/invoice.module#InvoiceModule'
    },
    {
      path: 'inbox',
      loadChildren: './views/inbox/inbox.module#InboxModule'
    },
    {
      path: 'calendar',
      loadChildren: './views/calendar/calendar.module#CalendarAppModule'
    },
    {
      path: 'chat',
      loadChildren: './views/chat/chat.module#ChatModule'
    },
    {
      path: 'tables',
      loadChildren: './views/data-tables/data-tables.module#DataTablesModule'
    },
    {
      path: 'pages',
      loadChildren: './views/pages/pages.module#PagesModule'
    },
    {
        path: 'icons',
        loadChildren: './views/icons/icons.module#IconsModule'
    }
  ];

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sessions/signin',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'sessions',
        loadChildren: './views/sessions/sessions.module#SessionsModule'
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'onboarding',
        loadChildren: './views/enrollee/onboard/onboard.module#OnboardModule'
      }
    ]
  },
  {
    path: '',
    component: BlankLayoutComponent,
    children: [
      {
        path: 'others',
        loadChildren: './views/others/others.module#OthersModule'
      }
    ]
  },
  {
    path: '',
    component: AdminLayoutSidebarCompactComponent,
    canActivate: [AuthGaurd],
    children: adminRoutes
  },
  {
    path: 'systemadmin',
    component: AdminLayoutSidebarCompactComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: sroles.Superadmin
    },
    children: [
      {
        path: '',
        loadChildren: './views/system-admin/system-admin.module#SystemAdminModule'
      }
  ]
  },
  {
    path: 'hmo',
    component: AdminLayoutSidebarCompactComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: sroles.HMO
    },
    children: [
      {
        path: '',
        loadChildren: './views/hmo/hmo.module#HmoModule'
      }
  ]
  },
  {
    path: 'client',
    component: AdminLayoutSidebarCompactComponent,
    canActivate: [RoleGuardService],
     data: {
      expectedRole: sroles.Organization
    },
    children: [
      {
        path: '',
        loadChildren: './views/client/client.module#ClientModule'
      }
  ]
  },
  {
    path: 'enrollee',
    component: AdminLayoutSidebarCompactComponent,
     canActivate: [RoleGuardService],
     data: {
      expectedRole: sroles.Enrollee
    },
    children: [
      {
        path: '',
        loadChildren: './views/enrollee/enrollee.module#EnrolleeModule'
      }
  ]
  },
  {
    path: 'provider',
    component: AdminLayoutSidebarCompactComponent,
    // canActivate: [RoleGuardService],
     data: {
      expectedRole: sroles.Provider
    },
    children: [
      {
        path: '',
        loadChildren: './views/provider/provider.module#ProviderModule'
      }
  ]
  },
  {path: 'verify',
    canActivate: [AuthGuardService],
    component: VerifyComponent
  },
  {
    path: '**',
    redirectTo: 'others/404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
